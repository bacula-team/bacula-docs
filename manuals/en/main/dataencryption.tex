
\chapter{Data Encryption}
\label{blb:DataEncryption}
\index[general]{Data Encryption}
\index[general]{Encryption!Data}
\index[general]{Data Encryption}

\bacula{} permits file data encryption and signing within the File Daemon (or
Client) prior to sending data to the Storage Daemon.  Upon restoration,
file signatures are validated and any mismatches are reported.  At no time
does the Director or the Storage Daemon have access to unencrypted file
contents.

\smallskip
It is very important to specify what this implementation does NOT do:
\begin{bitemize}
\item There is one important restore problem to be aware of, namely, it's
  possible for the director to restore new keys or a \bacula{} configuration
  file to the client, and thus force later backups to be made with a
  compromised key and/or with no encryption at all.  You can avoid this by
  not changing the location of the keys in your \bacula{} File daemon
  configuration file, and not changing your File daemon keys.  If you do
  change either one, you must ensure that no restore is done that restores
  the old configuration or the old keys.  In general, the worst effect of
  this will be that you can no longer connect the File daemon.

\item The implementation does not encrypt file metadata such as file path
  names, permissions, and ownership. Extended attributes are also currently
  not encrypted. However, Mac OSX resource forks are encrypted.
\end{bitemize}

\smallskip
Encryption and signing are implemented using \acs{RSA} private keys coupled with
self-signed X.509 public certificates. This is also sometimes known as \ac{PKI}.

\smallskip
Each File Daemon should be given its own unique private/public key pair.
In addition to this key pair, any number of ``Master Keys'' may be specified
-- these are key pairs that may be used to decrypt any backups should the
File Daemon key be lost.  Only the Master Key's public certificate should
be made available to the File Daemon.  Under no circumstances should the
Master Private Key be shared or stored on the Client machine.

\smallskip
The Master Keys should be backed up to a secure location, such as a \acs{CD}
placed in a in a fire-proof safe or bank safety deposit box. The Master
Keys should never be kept on the same machine as the Storage Daemon or
Director if you are worried about an unauthorized party compromising either
machine and accessing your encrypted backups.

\smallskip
While less critical than the Master Keys, File Daemon Keys are also a prime
candidate for off-site backups; burn the key pair to a \acs{CD} and send the \acs{CD}
home with the owner of the machine.

\smallskip
NOTE!!! If you lose your encryption keys, backups will be unrecoverable.
\textbf{ALWAYS} store a copy of your master keys in a secure, off-site location.

\smallskip
The basic algorithm used for each backup session (Job) is:
\begin{benumerate}
\item The File daemon generates a session key.
\item The \acs{FD} encrypts that session key via \acs{PKE} for all recipients (the file
daemon, any master keys).
\item The \acs{FD} uses that session key to perform symmetric encryption on the data.
\end{benumerate}


\section{Building \bacula{} with Encryption Support}
\index[general]{Building \bacula{} with Encryption Support}

To build \bacula{} with encryption support, you will need
the Open\acs{SSL} libraries and headers installed.  When configuring \bacula{}, use:

\begin{bVerbatim}
./configure --with-openssl ...
\end{bVerbatim}

\smallskip
Please note, the \bacula{} Enterprise binaries are all built with
encryption enabled.

\section{Encryption Technical Details}
\index[general]{Encryption Technical Details}
The implementation uses 128bit \acs{AES}-\acs{CBC}, with \acs{RSA} encrypted symmetric
session keys. The \acs{RSA} key is user supplied.
If you are running OpenSSL 0.9.8 or later, the signed file hash uses
\acs{SHA}-256 -- otherwise, \acs{SHA}-1 is used.

\smallskip
End-user configuration settings for the algorithms are not currently
exposed -- only the algorithms listed above are used. However, the
data written to Volume supports arbitrary symmetric, asymmetric, and
digest algorithms for future extensibility, and the back-end
implementation currently supports:

\begin{bitemize}
\item Symmetric Encryption:
  \begin{bitemize}
  \item 128, 192, and 256-bit AES-CBC
  \item Blowfish-CBC
  \end{bitemize}
\item Asymmetric Encryption (used to encrypt symmetric session keys):
  \begin{bitemize}
  \item RSA
  \end{bitemize}
\item Digest Algorithms:
  \begin{bitemize}
  \item MD5
  \item SHA1
  \item SHA256
  \item SHA512
  \end{bitemize}
\end{bitemize}

\smallskip
The various algorithms are exposed via an entirely re-usable,
OpenSSL-agnostic \acs{API} (ie, it is possible to drop in a new encryption
backend). The Volume format is \acs{DER}-encoded \acs{ASN.1}, modeled after the
Cryptographic Message Syntax from \acs{RFC} 3852. Unfortunately, using \acs{CMS}
directly was not possible, as at the time of coding a free software
streaming \acs{DER} decoder/encoder was not available.



\section{Concepts}
\index[general]{Encryption Implement Concepts}
Data encription is configured in \bacula{} only in the File Daemon.
The Job report that is produced at the end of each job has a
line indicating whether or not encryption was enabled:

\begin{bVerbatim}
VSS:         no
Encryption:  no/yes    <===
Accurate:    no
...
\end{bVerbatim}


\section{Generating Private/Public Encryption Keys}
\index[general]{Generating Private/Public Encryption Keypairs}

Generate a Master Key Pair (once) with:

\begin{bVerbatim}
openssl genrsa -out master.key 2048
openssl req -new -key master.key -x509 -out master.cert
\end{bVerbatim}

Save these files (\bfilename{master.key} and \bfilename{master.cert}) as you
will need them to create File Daemon keys or in case
you loose the File Daemon keys.

\smallskip

Generate  a File Daemon Key Pair for each \acs{FD}:
\begin{bVerbatim}
openssl genrsa -out fd-example.key 2048
openssl req -new -key fd-example.key -x509 -out fd-example.cert
cat fd-example.key fd-example.cert >fd-example.pem
\end{bVerbatim}

When configuring the File Daemon (see section \vref{blb:director:configure:filedaemon}), you will need
the keys you just generated here, so you will need to transmit
them to the machine where the \acs{FD} is installed.

\smallskip
Note, there seems to be a lot of confusion around the file extensions given
to these keys.  For example, a \bfilename{.pem} file can contain all the following:
private keys (\acs{RSA} and \acs{DSA}), public keys (\acs{RSA} and \acs{DSA}) and (X.509) certificates.
It is the default format for OpenSSL. It stores data Base64 encoded \acs{DER} format,
surrounded by \acs{ASCII} headers, so is suitable for text mode transfers between
systems. A \bfilename{.pem} file may contain any number of keys either public or
private. We use it in cases where there is both a public and a private
key.

\smallskip
Typically, above we have used the \bfilename{.cert} extension to refer to X.509
certificate encoding that contains only a single public key.


\section{Example Data Encryption Configuration}
\index[general]{Example!File Daemon Configuration File}
\index[general]{Example!Data Encryption Configuration File}
\index[general]{Example Data Encryption Configuration}

When configuring the \acs{FD}, use the keys generated above in a 
\acs{FD} configuration file that will look something like the
following:

\smallskip
\bfilename{bacula-fd.conf}
\begin{bVerbatim}
FileDaemon {
   Name = example-fd
   FDport = 9102                  # where we listen for the director
   WorkingDirectory = /opt/bacula/working
   Pid Directory = /var/run
   Maximum Concurrent Jobs = 20

   PKI Signatures = Yes            # Enable Data Signing
   PKI Encryption = Yes            # Enable Data Encryption
   PKI Keypair = "/opt/bacula/etc/fd-example.pem"    # Public and Private Keys
   PKI Master Key = "/opt/bacula/etc/master.cert"    # ONLY the Public Key
}
\end{bVerbatim}

You must restart your File Daemon after making this change
to the \bfilename{bacula-fd.conf} file. 

\smallskip
\paragraph{Note}: the PKIMasterKey directive is not mandatory, but if used
will allow you to decrypt the files if ever the \acs{FD} PKIKeypair is lost.
If you loose the \acs{FD}'s PKIKeypair, you will not be able to recover
your data unless you have used a PKIMasterKey.

\section{Decrypting with a Master Key}
\index[general]{Decrypting with a Master Key}
It is preferable to retain a secure, non-encrypted copy of the
client's own encryption keypair. However, should you lose the
client's keypair, recovery with the master keypair is possible.

\smallskip
First create a keypair with:

\begin{bVerbatim}
cat master.key master.cert >master.pem
\end{bVerbatim}

\smallskip
Then modify your File Daemons configuration file to use
the master keypair:

\begin{bVerbatim}
FileDaemon {
   Name = example-fd
   FDport = 9102                  # where we listen for the director
   WorkingDirectory = /opt/bacula/working
   Pid Directory = /var/run
   Maximum Concurrent Jobs = 20

   PKI Signatures = Yes            # Enable Data Signing
   PKI Encryption = Yes            # Enable Data Encryption
   PKI Keypair = "/opt/bacula/etc/master.pem"   # Master Public and Private Keys
}
\end{bVerbatim}

\smallskip
Restart your File Daemon and you should be able to recover your
lost files.
