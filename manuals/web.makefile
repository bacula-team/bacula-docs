MANUALENVFILE=manual.env
ENVFILE=$(shell [ -e ./$(MANUALENVFILE) ] && echo "OK")
ifeq ($(ENVFILE),OK)
  include $(MANUALENVFILE)
  REALMANUALENV=$(realpath ./$(MANUALENVFILE))
else
  $(error No environment file "$(MANUALENVFILE)" found. You can copy "$(MANUALENVFILE).dist" to "$(MANUALENVFILE)" and adjust some definitions.)
endif

AT=@


INPUTDIR=$(abspath ./$(DOC))
#
# /!\ Warning
# These directories are to be included as it is into the HTML files
# Do *NOT* make them absolute path.
JSDIR=../../js
CSSDIR=../../css
IMAGESDIR=../../images
#
# Web documentation generation
SIMULIMAGESDIR=../../images
VERBOSITY=0
HTML_DEPTH=3
HTML_TITLES=60 # use the 60 first words of the section heading to name the node
HTML_VERSION=5
HTML_DEBUG= # put -d here to get debug
LATEX_TO_HTML=$(LATEX2HTML) -no_auto_link -verbosity $(VERBOSITY) -info 0 -nonavigation -split $(HTML_DEPTH) -local_icons -t $(HTML_TITLE) -long_titles $(HTML_TITLES) -toc_stars -contents_in_nav -no-antialias -no_antialias_text -white -transparent $(DOC) -html_version $(HTML_VERSION) > tex.out

TEXINPUTS=.:$(LATEX_DIR)://:
export $(TEXINPUTS)
.SUFFIXES:     .tex .html
.PHONY:
.DONTCARE:

first_rule: all

all: web clean

verbose:
	$(AT)echo "Verbose"
	$(AT)echo "Tools DIR: " $(TOOLS_DIR)
	$(AT)echo "Input DIR: " $(INPUTDIR)
	$(AT)echo "HTML DIR: " $(HTML_DIR)
	$(AT)echo "js DIR: " $(JSDIR)
	$(AT)echo "css DIR: " $(CSSDIR)
	$(AT)echo "Images: " $(IMAGES)
	$(AT)echo "images DIR: " $(IMAGESDIR)
	$(AT)echo "LaTeX DIR: " $(LATEX_DIR)
	$(AT)echo "Covers DIR: " $(COVERSDIR)
	$(AT)echo "PDF covers DIR: " $(PDFCOVERSDIR)
	$(AT)echo "SVG covers DIR:" $(SVGCOVERSDIR)
	$(AT)echo "LaTeX to HTML binary: " $(LATEX2HTMLBIN)
	$(AT)echo "Simulation image DIR: " $(SIMULIMAGESDIR)
	$(AT)echo "Verbosity (latex2html): " $(VERBOSITY)
	$(AT)echo "LATEX_TO_HTML: $(LATEX_TO_HTML)"
	$(AT)echo "PERL5LIB: " $(PERL5LIB)
	$(AT)echo "TEXINPUTS: " $(TEXINPUTS)
	$(AT)echo "$(TOOLS_DIR)/translatedoc.pl -i $(INPUTDIR)/index.html -e -o $(HTML_DIR)/index.html -j $(JSDIR) -c $(CSSDIR) -p $(IMAGESDIR) -n $(DOC) -r $(INPUTDIR))"

pngimages:
	$(AT)echo "PNG images"
	$(AT)([ -e $(SVGIMAGES_DIR) ] && $(MAKE) -C $(SVGIMAGES_DIR) png)
	$(AT)mkdir -p $(SIMULIMAGESDIR)
	$(AT)(for F in $(PNGIMAGES_DIR)/*.png ; do ln -sf $$F ./`basename $$F`; cp -u $$F $(SIMULIMAGESDIR); done)
	$(AT)echo "PNG done."

jpgimages:
	$(AT)echo "JPG images"
	$(AT)mkdir -p $(SIMULIMAGESDIR)
	$(AT)(for F in $(JPGIMAGES_DIR)/*.jpg ; do ln -sf $$F ./`basename $$F`; f=`basename -s jpg $$F` ; fpnm=$$f'pnm' ; fpng=$$f'png' ; jpegtopnm $$F > $(SIMULIMAGESDIR)/$$fpnm ; pnmtopng $(SIMULIMAGESDIR)/$$fpnm > $(SIMULIMAGESDIR)/$$fpng ; rm $(SIMULIMAGESDIR)/$$fpnm ; done)
	$(AT)echo "JPG done."

clean:
	$(AT)echo -n "Cleaning..."
	$(AT)find . -type l -iname "*.eps" -exec rm {} \;
	$(AT)find . -type l -iname "*.png" -exec rm {} \;
	$(AT)find . -type l -iname "*.jpg" -exec rm {} \;
	$(AT)rm -f bgraphicpath.tex
	$(AT)echo "Done."

commonfiles:
	cp $(LATEX_DIR)/bgraphicpath-web.tex ./bgraphicpath.tex
	$(AT)echo "Done"

web: commonfiles pngimages jpgimages
	$(AT)echo "Making web"
	$(AT)rm -f next.eps next.png prev.eps prev.png up.eps up.png
	$(AT)rm -f $(DOC)/xp-*.png
	$(AT)rm -f $(DOC)/next.eps $(DOC)/next.png $(DOC)/prev.eps $(DOC)/prev.png $(DOC)/up.eps $(DOC)/up.png
	$(AT)$(LATEX_TO_HTML)
	$(AT)cp -f $(DOC)/$(MAINDOC) $(DOC)/index.html
	$(AT)cp ../$(DOC).css ./$(DOC)/
	$(AT)(cd $(DOC) ; $(TOOLS_DIR)/translatedoc.pl -i $(INPUTDIR)/index.html $(HTML_DEBUG) -e -o $(HTML_DIR)/index.html -j $(JSDIR) -c $(CSSDIR) -p $(IMAGESDIR) -n $(DOC) -r $(INPUTDIR))
	$(AT)(cd $(DOC) ; for L in $(INPUTDIR)/*html ; do $(TOOLS_DIR)/translatedoc.pl $(HTML_DEBUG) -i $(INPUTDIR)/`basename $$L` -o $(HTML_DIR)/`basename $$L` -j $(JSDIR) -c $(CSSDIR) -p $(IMAGESDIR) -n $(DOC) ; done)
	$(AT)mkdir -p $(JSDR) $(CSSDIR) $(IMAGESDIR)
	$(AT)cp $(DOC)/$(DOC).css $(CSSDIR)
	$(AT)echo "Done making web"
