%%
%%

\chapter{Installing and Configuring \mysql{}}
\label{blb:MySqlChapter}
\index[general]{\mysql{}!Installing and Configuring }
\index[general]{Installing and Configuring \mysql{} }

\section{Installing and Configuring \mysql{} -- Phase I}
\index[general]{Installing and Configuring \mysql{} -- Phase I }
\index[general]{Phase I!Installing and Configuring \mysql{} -- }

If you use the \btool{./configure {-}{-}with-mysql=mysql-directory} statement for
configuring \textbf{\bacula{}}, you will need \mysql{} version 4.1 or later installed
in the \bdirectoryname{mysql-directory}.  If you are using one of the new modes such as
\acs{ANSI}/\acs{ISO} compatibility, you may experience problems.

If \mysql{} is installed in the standard system location, you need only enter
\textbf{{-}{-}with-mysql} since the configure program will search all the
standard locations.  If you install \mysql{} in your home directory or some
other non-standard directory, you will need to provide the full path to it.

Installing and Configuring \mysql{} is not difficult but can be confusing the
first time. As a consequence, below, we list the steps that we used to install
it on our machines. Please note that our configuration leaves \mysql{} without
any user passwords. This may be an undesirable situation if you have other
users on your system.

The notes below describe how to build \mysql{} from the source tar files. If
you have a pre-installed \mysql{}, you can return to complete the installation
of \bacula{}, then come back to \bilink{Phase II}{blb:mysql_phase2} of the \mysql{} installation.  If you
wish to install \mysql{} from rpms, you will probably need to install
the following:

\begin{bVerbatim}
mysql-<version>.rpm
mysql-server-<version>.rpm
mysql-devel-<version>.rpm
\end{bVerbatim}

If you wish to install them from debs, you will probably need the
following:

\begin{bVerbatim}
mysql-server-<version>.deb
mysql-client-<version>.deb
libmysqlclient15-dev-<version>.deb
libmysqlclient15off-<version>.deb
\end{bVerbatim}

The names of the packages may vary from distribution to
distribution. It is important to have the \textbf{devel} or \textbf{dev} package loaded as
it contains the libraries and header files necessary to build
\bacula{}.  There may be additional packages that are required to
install the above, for example, \btool{zlib} and \btool{openssl}.

Once these packages are installed, you will be able to build \bacula{} (using
the files installed with the \bhighlight{mysql} package, then run \mysql{} using the
files installed with \bhighlight{mysql-server}. If you have installed \mysql{} by debs or rpms,
please skip Phase I below, and return to complete the installation of
\bacula{}, then come back to Phase II of the \mysql{} installation when indicated
to do so.

Beginning with \bacula{} version 1.31, the thread safe version of the
\mysql{} client library is used, and hence you should add the
      \textbf{{-}{-}enable-thread-safe-client} option to the \btool{./configure}
as shown below:

\begin{benumerate}
\item Download \mysql{} source code from
   \bref{http://www.mysql.com/downloads}{www.mysql.com/downloads}

\item Detar it with something like:
\begin{bVerbatim}
tar xvfz mysql-filename
\end{bVerbatim}

Note, the above command requires GNU \tar{}. If you do not  have GNU \tar{}, a
command such as:

\begin{bVerbatim}
zcat mysql-filename | tar xvf -
\end{bVerbatim}

will probably accomplish the same thing.

\item \begin{bVerbatim}
cd mysql-source-directory
\end{bVerbatim}

   where you replace \bdirectoryname{mysql-source-directory} with the  directory name where
   you put the \mysql{} source code.

 \item \begin{bVerbatim}
./configure --enable-thread-safe-client --prefix=mysql-directory
 \end{bVerbatim}

   where you replace \bdirectoryname{mysql-directory} with the directory  name where you
   want to install mysql. Normally for system  wide use this is \bdirectoryname{/usr/local/mysql}.
   In my case, I use  \bdirectoryname{\~{}kern/mysql}.

 \item \begin{bVerbatim}
make
 \end{bVerbatim}

   This takes a bit of time.

\item make install

   This will put all the necessary binaries, libraries and support  files into
   the \bdirectoryname{mysql-directory} that you specified above.

 \item \begin{bVerbatim}
./scripts/mysql\_install\_db
 \end{bVerbatim}

   This will create the necessary \mysql{} databases for controlling  user access.
Note, this script can also be found in the  \bdirectoryname{bin} directory in the
installation directory

\end{benumerate}

The \mysql{} client library \textbf{mysqlclient} requires the \btool{gzip} compression
library \bfilename{libz.a} or \bfilename{libz.so}. If you are using rpm packages, these
libraries are in the \textbf{libz-devel} package. On Debian systems, you will
need to load the \textbf{zlib1g-dev} package. If you are not using rpms or debs,
you will need to find the appropriate package for your system.

At this point, you should return to completing the installation of
\textbf{\bacula{}}. Later after \bacula{} is installed, come back to this chapter to
complete the installation. Please note, the installation files used in the
second phase of the \mysql{} installation are created during the \bacula{}
Installation.

\label{blb:mysql_phase2}
\section{Installing and Configuring \mysql{} -- Phase II}
\index[general]{Installing and Configuring \mysql{} -- Phase II }
\index[general]{Phase II!Installing and Configuring \mysql{} -- }

At this point, you should have built and installed \mysql{}, or already have a
running \mysql{}, and you should have configured, built and installed
\textbf{\bacula{}}. If not, please complete these items before proceeding.

Please note that the \btool{./configure} used to build \textbf{\bacula{}} will need to
include \textbf{{-}{-}with-mysql=mysql-directory}, where \bdirectoryname{mysql-directory} is the
directory name that you specified on the \btool{./configure} command for configuring
\mysql{}. This is needed so that \bacula{} can find the necessary include headers
and library files for interfacing to \mysql{}.

\textbf{\bacula{}} will install scripts for manipulating the database (create,
delete, make tables etc) into the main installation directory. These files
will be of the form \bfilename{*\_bacula\_*} (e.g. \bfilename{create\_bacula\_database}). These files
are also available in the \bdirectoryname{\bbracket{bacula-src}/src/cats} directory after
running \btool{./configure}. If you inspect \bfilename{create\_bacula\_database}, you will see
that it calls \bfilename{create\_mysql\_database}. The \bfilename{*\_bacula\_*} files are provided for
convenience. It doesn't matter what database you have chosen;
\bfilename{create\_bacula\_database} will always create your database.

Now you will create the \bacula{} \mysql{} database and the tables that \bacula{} uses.


\begin{benumerate}
\item Start \btool{mysql}. You might want to use the \btool{startmysql}  script
   provided in the \bacula{} release.

 \item \begin{bVerbatim}
cd <install-directory>
 \end{bVerbatim}
   This directory contains the \bacula{} catalog  interface routines.

 \item \begin{bVerbatim}
./grant_mysql_privileges
 \end{bVerbatim}
   This script creates unrestricted access rights for the user \textbf{bacula}.
   You may  want to modify it to suit your situation. Please
   note that none of the userids, including root, are password protected.
   If you need more security, please assign a password to the root user
   and to bacula. The program \btool{mysqladmin} can be used for this.

 \item \begin{bVerbatim}
./create_mysql_database
 \end{bVerbatim}
   This script creates the \mysql{} \textbf{bacula} database.  The databases you
   create as well as the access databases will be located in
   \bdirectoryname{\bbracket{install-dir}/var/} in a subdirectory with the name of the
   database, where \bdirectoryname{\bbracket{install-dir}} is the directory name that you
   specified on the \textbf{prefix} option.  This can be important to
   know if you want to make a special backup of the \bacula{} database or to
   check its size.

 \item \begin{bVerbatim}
./make\_mysql\_tables
 \end{bVerbatim}
   This script creates the \mysql{} tables used by \textbf{\bacula{}}.
\end{benumerate}

Each of the three scripts (\bfilename{grant\_mysql\_privileges}, \bfilename{create\_mysql\_database}
and \bfilename{make\_mysql\_tables}) allows the addition of a command line argument. This
can be useful for specifying the user and or password. For example, you might
need to add \textbf{-u root} to the command line to have sufficient privilege to
create the \bacula{} tables.

To take a closer look at the access privileges that you have setup with the
above, you can do:

\begin{bVerbatim}
<mysql-directory>/bin/mysql -u root mysql
select * from user;
\end{bVerbatim}

\section{Re-initializing the Catalog Database}
\index[general]{Database!Re-initializing the Catalog }
\index[general]{Re-initializing the Catalog Database }

After you have done some initial testing with \textbf{\bacula{}}, you will probably
want to re-initialize the catalog database and throw away all the test Jobs
that you ran. To do so, you can do the following:

\begin{bVerbatim}
cd <install-directory>
./drop_mysql_tables
./make_mysql_tables
\end{bVerbatim}

Please note that all information in the database will be lost and you will be
starting from scratch. If you have written on any Volumes, you must write an
end of file mark on the volume so that \bacula{} can reuse it. Do so with:

\begin{bVerbatim}
(stop Bacula or unmount the drive)
mt -f /dev/nst0 rewind
mt -f /dev/nst0 weof
\end{bVerbatim}

Where you should replace \bfilename{/dev/nst0} with the appropriate tape drive
device name for your machine.

\section{Linking \bacula{} with \mysql{}}
\index[general]{Linking \bacula{} with \mysql{} }
\index[general]{\mysql{}!Linking \bacula{} with }
\index[general]{Upgrading}

After configuring \bacula{} with

\btool{./configure {-}{-}enable-thread-safe-client --prefix=\bbracket{mysql-directory}}
where \bdirectoryname{\bbracket{mysql-directory}} is in my case \bdirectoryname{/home/kern/mysql}, you may
have to configure the loader so that it can find the \mysql{} shared libraries.
If you have previously followed this procedure and later add the
\textbf{{-}{-}enable-thread-safe-client} options, you will need to rerun the \btool{ldconfig}
program shown below. If you put \mysql{} in a standard place such as
\bdirectoryname{/usr/lib} or \bdirectoryname{/usr/local/lib} this will not be necessary, but in my
case it is. The description that follows is Linux specific. For other
operating systems, please consult your manuals on how to do the same thing:

First edit: \bfilename{/etc/ld.so.conf} and add a new line to the end of the file
with the name of the mysql-directory. In my case, it is:

\bdirectoryname{/home/kern/mysql/lib/mysql} then rebuild the loader's cache with:

\btool{/sbin/ldconfig}. If you upgrade to a new version of \textbf{\mysql{}}, the shared
library names will probably change, and you must re-run the \btool{/sbin/ldconfig}
command so that the runtime loader can find them.

Alternatively, your system my have a loader environment variable that can be
set. For example, on a Solaris system where I do not have root permission, I
use:
\begin{bVerbatim}
LD_LIBRARY_PATH=/home/kern/mysql/lib/mysql
\end{bVerbatim}
Finally, if you have encryption enabled in \mysql{}, you may need to add
\textbf{-lssl -lcrypto} to the link. In that case, you can either export the
appropriate LDFLAGS definition, or alternatively, you can include them
directly on the \btool{./configure} line as in:

\begin{bVerbatim}
LDFLAGS="-lssl -lcyrpto" ./configure <your-options>
\end{bVerbatim}

\section{Installing \mysql{} from RPMs}
\index[general]{\mysql{}!Installing from RPMs}
\index[general]{Installing \mysql{} from RPMs}
If you are installing \mysql{} from \acsp{RPM}, you will need to install
both the \mysql{} binaries and the client libraries.  The client
libraries are usually found in a devel package, so you must
install:

\begin{bVerbatim}
mysql
mysql-devel
\end{bVerbatim}

This will be the same with most other package managers too.

\section{Upgrading \mysql{}}
\index[general]{Upgrading \mysql{} }
\index[general]{Upgrading!\mysql{} }
\index[general]{Upgrading}
If you upgrade \mysql{}, you must reconfigure, rebuild, and re-install
\bacula{} otherwise you are likely to get bizarre failures.  If you
install from rpms and you upgrade \mysql{}, you must also rebuild \bacula{}.
You can do so by rebuilding from the source rpm. To do so, you may need
to modify the \bfilename{bacula.spec} file to account for the new \mysql{} version.


\section{\mysql{} Configuration Caution}
\index[general]{\mysql{} Caution}
Bacula requires that at least one of the \textbf{TIMESTAMP} fields in the
database schema to have a valid value of zero.  If you turn on the 
\textbf{SQL\_MODE} \textbf{STRICT\_ALL\_TABLES}, you must be sure that you
turn off the \textbf{NO\_ZERO\_DATA} and the \textbf{NO\_ZERO\_IN\_DATE} 
restrictions or Bacula will not function correctly.
