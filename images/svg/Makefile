#
# Makefile for converting svg files
# to something else: png, pdf, eps
#
# 25-nov-2020 - Philippe Chauvat / Bacula Systems
#		Porting what is done in covers to images to let them be including variables/parameters
# 22-oct-2020 - Philippe Chauvat / Bacula Systems
#		Improvements to handle Inkscape version
# 10-Oct-2012 - Philippe Chauvat / Bacula Systems
#
INKSCAPE=inkscape
VERSION=$(shell $(INKSCAPE) -V 2>/dev/null|cut -d ' ' -f2|cut -d'.' -f1)
VERBOSE=FALSE
ifeq ($(VERBOSE),TRUE)
	HIDE=
else
	HIDE=@
endif

#
# Traitement de la compilation à partir de la version 1 d'Inkscape
ifeq ($(VERSION),1)
	INKSCAPE_FLAGS=-o
	SVG_TO_PDF=--export-type=pdf -C --export-pdf-version=1.5 -T $(INKSCAPE_FLAGS)
	SVG_TO_EPS=--export-type=eps -C -T $(INKSCAPE_FLAGS)
	SVG_TO_PNG=--export-type=png -C -T --export-dpi=120 $(INKSCAPE_FLAGS)
#
# Version 0.x
else
	INKSCAPE_FLAGS=-z -T -C
	SVG_TO_PDF=$(INKSCAPE_FLAGS) -A
	SVG_TO_EPS=$(INKSCAPE_FLAGS) -E
	SVG_TO_PNG=$(INKSCAPE_FLAGS) -e
endif

PDFDIR=../pdf
PNGDIR=../png
EPSDIR=../eps

TPLSVGS=$(shell grep -l '__' *.svg)
SVGS=$(shell grep -L '__' *.svg)
TPLS=$(TPLSVGS:.svg=.tpl)
PDFS=$(addprefix $(PDFDIR)/,$(SVGS:.svg=.pdf)) $(addprefix $(PDFDIR)/,$(TPLS:.tpl=.pdf))
EPSS=$(addprefix $(EPSDIR)/,$(SVGS:.svg=.eps)) $(addprefix $(EPSDIR)/,$(TPLS:.tpl=.eps))
PNGS=$(addprefix $(PNGDIR)/,$(SVGS:.svg=.png)) $(addprefix $(PNGDIR)/,$(TPLS:.tpl=.png))

BDATE := $(shell date +%Y-%m-%d)

first_rule: all

all: check tpl pdf png

.SUFFIXES:
.PHONY:
.DONTCARE:

check:
	$(HIDE)echo "Template SVG files: $(TPLSVGS)"
	$(HIDE)echo "Regular SVG files: $(SVGS)"
	$(HIDE)echo "TPL files to be generated: $(TPLS)"
	$(HIDE)echo "PDFDIR: $(PDFDIR)"
	$(HIDE)echo "PDFs $(PDFS)"
	$(HIDE)echo "EPSDIR: $(EPSDIR)"
	$(HIDE)echo "EPSs: $(EPSS)"
	$(HIDE)echo "PNGDIR: $(PNGDIR)"
	$(HIDE)echo "PNGs: $(PNGS)"
#
# PDF images creation
pdf: $(TEMPLATES) $(PDFS)

tpl: $(TPLS)

$(TPLS): $(VERSION_TEX)

%.tpl: %.svg
	$(HIDE)echo "$< :   Handling version number and date"
	$(HIDE)sed "s/__BVERSION__/`awk '{print $$1}' $(VERSION_TEX)`/g" $< > $@
	$(HIDE)sed -i 's/__BEDITION__/$(BACULAVERSION)/g' $@
	$(HIDE)sed -i 's/__BDATE__/$(BDATE)/g' $@


$(PDFS): | $(PDFDIR)
$(PDFDIR):
	$(HIDE)echo "Creating PDF images directory..."
	$(HIDE)mkdir $(PDFDIR)
	$(HIDE)echo "Done"

$(PDFDIR)/%.pdf: %.tpl
	$(HIDE)echo "Converting $< to $@"
	$(HIDE)$(INKSCAPE) $(SVG_TO_PDF) $@ $< 1>$<.svg_error 2>$<.svg_error

$(PDFDIR)/%.pdf: %.svg
	$(HIDE)echo "$< :   Converting to PDF"
	$(HIDE)$(INKSCAPE) $(SVG_TO_PDF) $@ $<
#
# EPS images creation
eps: $(TEMPLATES) $(EPSS)
$(EPSS): | $(EPSDIR)
$(EPSDIR):
	$(HIDE)echo "Creating EPS images directory..."
	$(HIDE)mkdir $(EPSDIR)
	$(HIDE)echo "Done"

$(EPSDIR)/%.eps: %.tpl
	$(HIDE)echo "Converting $< to $@"
	$(HIDE)$(INKSCAPE) $(SVG_TO_EPS) $@ $< 1>$<.svg_error 2>$<.svg_error

$(EPSDIR)/%.eps: %.svg
	$(HIDE)$(INKSCAPE) $(INKSCAPE_FLAGS) $(SVG_TO_EPS) $(EPSDIR)/$@ $<

#
# PNG images creation
png: $(TEMPLATES) $(PNGS)
$(PNGS): | $(PNGDIR)
$(PNGDIR):
	$(HIDE)echo "Creating PNG images directory..."
	$(HIDE)mkdir $(PNGDIR)
	$(HIDE)echo "Done"

$(PNGDIR)/%.png: %.tpl
	$(HIDE)echo "Converting $< to $@"
	$(HIDE)$(INKSCAPE) $(SVG_TO_PNG) $@ $< 1>$<.svg_error 2>$<.svg_error

$(PNGDIR)/%.png: %.svg
	$(HIDE)$(INKSCAPE) $(INKSCAPE_FLAGS) $(SVG_TO_PNG) $(PNGDIR)/$@ $<

clean:
	$(HIDE)rm -f $(TPLS)
	$(HIDE)(if [ -e $(PDFDIR) ]; then cd $(PDFDIR) ; rm -f $(PDFS); fi)
	$(HIDE)(if [ -e $(EPSDIR) ]; then cd $(EPSDIR) ; rm -f $(EPSS); fi)
	$(HIDE)(if [ -e $(PNGDIR) ]; then cd $(PNGDIR) ; rm -f $(PNGS); fi)
	$(HIDE)rm -f *~ *.svg_error

distclean: clean
