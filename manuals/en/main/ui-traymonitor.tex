\section{The Tray Monitor}
\label{blb:TrayMonitorChapter}
\index[general]{User Interfaces!Tray Monitor}
\index[general]{Tray Monitor}

The Tray Monitor program is a simplified graphical user interface intended to
be used with \acsp{GUI} providing a tray area, similar to Windows' Task Bar
information icon tray area. % TODO: what is this thing called in English?

It provides the capability to monitor all the core components of a \bacula{}
infrastructure, namely Directors, Storage Daemons and File Daemons. The Tray
Monitor program can be configured to monitor an arbitrary number of those
daemons, and it can show an icon indicating current activity of the monitored
daemon.

The Tray Monitor also has a built-in graphical configuration interface
(but it can, as all \bacula{} components, be configured with a plain text file)
and allows to initiate backup jobs through its context menu.

In addition, the Tray Monitor program can interactively or non-interactively
start jobs.

The latter functionality is particularly valuable if used in conjunction with
the capability of File Daemons to serve as proxy when connecting to a Director
needing a connection to the client to run a job.

\subsection{Installation}
When building the \bacula{} software from source, the Tray Monitor program will
be built similarly and under the same conditions as \bat{}, the \acl{BAT}, is.
In particular, it needs a Qt build environment.
\medskip

When installing the client software from packages, the Tray Monitor may be
packaged individually or along with some other console programs -- this is
a packager's decision. \baculasystems{}' Enterprise Edition packages the Tray
Monitor together with the \acs{GUI} console (\bat{}) on Linux, and with the client
installer on Windows.

The actual steps to install depend on the platform and the packages used.
A Windows Installer will typically allow to select the Tray Monitor for
installation in some tree view of components to be installed.
\medskip

The Tray Monitor program can be started whenever a \acs{GUI} session is initiated,
but care should be taken to use a configuration file that belongs to the
user who runs the \acs{GUI} session. In particular, having a default configuration
file with credentials allowing unrestricted Director access usable by
regular end users is to be avoided.


\subsection{Configuration}
The Tray Monitor program, unlike most other parts of the \bacula{} software, can
be run without a configuration file in place. In this case, the configuration
panel will be opened, allowing the user to configure the program according to
her needs.

With the Tray Monitor it is quite likely that individual users will have their
own configuration, i.e. a system-wide configuration file is not the
common way to run the program. This is because the Tray Monitor, to do its
work, needs access to backend components of the \bacula{} infrastructure, and
individual users will only be provided with restricted access by the
\bacula{} administration for security reasons.

For example, on a system shared by regular users and system administrators,
the non-privileged users should not be able to see all jobs that can be run
but only the ones relating to their own data, while a system administrator
probably needs to monitor all the components running on the machines under his
responsibility.

\subsubsection{\acs{GUI} Configuration}

When started without a configuration file, or when the ``Configure\ldots{}''
context menu item of the tray icon is clicked, the configuration window of
the \btool{Tray Monitor} program is shown. It consists of a tabbed view of
configuration pages and some buttons, which provide the following functionality:

\begin{bdescription}
\item[Save] to save the current state into a text file (see paragraph above).
\item[Cancel] to abort the operation, leave everything as it was, and close the
configuration panel.
\item[Password] to toggle between showing passwords on the current tab page
  in clear or hidden. This is useful to keep passwords hidden against
  observers, but still be able to check potentially complex and long
  passwords when needed.
\item[(Add) Client] to add a new, empty client configuration tab.
\item[(Add) Storage] to add a new, empty storage configuration tab.
\item[(Add) Director] to add a new, empty director configuration tab.
\end{bdescription}

There will always be at least one tab to configure the identity of this Tray
Monitor instance itself. Configuration page tabs that represent Clients,
Storages, and Directors, will initially appear with a title of ``New \bbracket{Component}''
which will change to the name of the monitored component once that has been provided.

\subsubsection{The Monitor Configuration page}

This tab defines the identity of the Tray Monitor instance. It is always
available, cannot be removed, and there can only be one such tab. It contains
the following fields:

\begin{bdescription}
\item[Name] the name of this Tray Monitor instance. This name is
  used to authenticate when connecting to any other \bacula{} daemon.\par
  Note that, as usual with \bacula{}, the password used for authentication
  is configured individually for each counterpart.\par
  The name is a \textit{name-string} data type and allows
  only a limited set of characters (essentially, \acs{ASCII} characters and numerals
  plus basic interpunction) and is limited to a length of 127 characters.
\item[Refresh Interval] is used to set the number of seconds between refreshes
  of status displays. Integer numbers between 5 and 9999 can be entered or
  selected.
\item[Command Directory] allows to enter or select the path of a directory
  which will be polled for command files. See below for details on how to
  use this feature. % TODO: create link once the description exists
\item[Display Advanced Options] will enable a settings page to select
  more Job options when the ``Run\ldots{}'' context menu item of the
  \btool{Tray Monitor} program is selected. See below for more information. % TODO: link
\end{bdescription}

As usual for a \bacula{} component, unless the identity is configured the program
will not be very functional.

\subsubsection{Common elements}

A common layout is used throughout all the configuration pages for the
different \bacula{} Daemons. We will describe that first, before we look at
specific additional configuration items for Client and Director.

We have always two groups of settings and a waste bin icon. The waste bin is
used to delete the currently shown configuration from the Tray Monitor setup.

One group of settings is used to define the essential information to contact
and use the component, and one group to configure \acs{TLS}-related information. The
former contains some required fields, among them the connection and
authentication information, while the latter is optional.

We will not discuss the details of \acs{TLS} configuration here -- if \acs{TLS} is
required, the \bacula{} administrator will probably give detailed instructions
and has to provide at least the ``\acs{CA} Certificate File''. Simply put, leave the
``Enabled'' check box in the \acs{TLS} section turned off unless detailed and
site-specific configuration information is provided. If it is enabled, the
selections in this group are critical and need to match what is configured
for the respective daemon.
\medskip

The General section contains the following elements common to Client,
Storage, and Director configuration pages:
\begin{bdescription}
\item[Name] to configure the name that is used to represent this component
  in the Tray Monitor's user interface. This is a typical \bacula{} ``name-string''
  with restrictions in length and usable character set (see above in the
  ``Monitor Configuration'' description).
\item[Description] can hold a textual description of the component. It is
  not used for any functionality, but rather to allow users to annotate their
  configuration.\par
  The length of the description text is limited to 512 characters, and it can
  contain (nearly) any character as it's of the configuration data type
  ``string''.
\item[Password] is used to configure the shared secret that authenticates
  against the configured daemon. By default the text entered here is hidden, but
  it is possible to toggle with clear-text display using the ``Password'' button
  on the right.\par
  The password is of \bacula{}'s ``password'' data type and as such can contain
  up to 127 characters. \acs{ASCII} symbols, numerals, and some punctuation are
  allowed here.
\item[Address] is the address at which the configured daemon can be contacted.
  Usually, an \acs{IP}v4 address in dotted quad notation or a \acs{DNS} name is used.\par
  Note that domain names should be fully qualified -- using plain host names
  is strongly discouraged.\par
  Support for \acs{IP}v6 addresses depends on how the Tray Monitor program was
  built, but should be matching the \acs{IP}v6 support status of all other \bacula{}
  programs resulting from the same build run.
\item[Port] represents the port number at which the daemon that is configured
  to be monitored can be contacted. Integer numbers between 1 and 65535 are
  allowed here. The default value shown in a new configuration page is the
  default port number for the daemon type.\par
  This value should only be modified in two cases:
  \begin{benumerate}
  \item When the \bacula{} administrator explicitly indicates that a specific,
    non standard port number should be used.
  \item When the File Daemon is configured for client-initiated backup. In this
    case the address to use will usually be ``localhost'', the port 9102, and
    the ``Remote'' checkbox will be ticked.\par
    See below for more information on this mode of operation. % TODO: Link
  \end{benumerate}
\item[Timeout] is used to set the connection timeout so the Tray Monitor will
  not try infinitely long to connect to the counterpart.\par
  This is a typical \bacula{} ``time-string'', so the input format can be pretty
  flexible. However, for the purpose of network timeouts, plain integers
  representing numbers are suitable, and values between 5 and 300 (seconds)
  should be reasonable. The default unit is seconds.
\item[Monitor] is a checkbox which, when selected, tells the Tray Monitor
  program that the currently configured daemon should update the
  tray icon. When the daemon has some activity the tray icon will indicate
  this. Otherwise the tray icon will show an idle state.\par
  This setting is most useful on client systems where it can give an
  indication of current activity to the desktop user, so that disrupting
  \bacula{} operations by shutting down or disconnecting from the network can
  be avoided.
\end{bdescription}

\subsubsection{Client configuration}

In most cases, the \bacula{} File Daemon will be the one running on the local
machine. In addition, a Client can also serve as a proxy for the Tray Monitor
program to connect to a Director. This additional functionality is enabled with
a configuration item:

\begin{bdescription}
\item[Remote] tells the Tray Monitor that this client can be used as a
proxy to connect to a Director. This operation mode allows to initiate
connections between Director and File Daemon from the client side, which is
invaluable in situations where the DIR cannot (reliably) contact the
File Daemon. Such a client-initiated connections can then be used to run
jobs on clients inaccessible by the Director.
See~\vref{blb:ch:ui-traymon:proxyconn} for more details.
\end{bdescription}


\subsubsection{Director Configuration}

In addition to the common configuration settings described above, one
additional parameter is available here:

\begin{bdescription}
\item[Use SetIp] causes the Tray Monitor program to send a \texttt{setip}
  command to the Director, which will enable the Director to communicate with
  this client.

  This setting can be useful in cases where the client computer has no fixed
  \acs{IP} address or \acs{DNS} name, but can be reached directly by the Director. Once the
  \bcommandname{setip} command was executed, the Director can use regular ways to
  operate on this client, for example starting scheduled Jobs or querying the
  Client status.

  For the \bcommandname{setip} command to succeed, it is required to use
  exactly identical Client and Console resource names in the Director's
  configuration. The Main manual describes this feature in detail.
  %TODO: link to proper location when integrated into main manual
  %NOTE: I don't like to repeat the explanation here -- duplicating
  % information is to be avoided as much as possible to keep the whole
  % set of manuals maintainable. I admit that we need better structure
  % and easier linking between documentation parts... ARL
  %% partial ANSWER: with the macros defined for the current doc building process, it is kind of easy to link or reference at least any part of the documentation
  %% to another one. Would it include the whitepapers means there are more work to do.
  %% Another thing is that maybe we should consider moving to a real documentation system (Red Hat is IMHO a very good example). Philippe - 28-sept-2018
\end{bdescription}


\subsubsection{Configuration file structure}

As usual for \bacula{} components, the configuration for the Tray Monitor program
is stored as a plain text file; the format is exactly the same as used with
all the other components.

When you configure \bacula{} Tray Monitor with the \acs{GUI}, a certain knowledge
of the \bacula{} configuration scheme is surely helpful. When you edit the file
with a text editor, this knowledge becomes essential.

However, there is a rather simple mapping from fields in the \acs{GUI} to
configuration file contents: Each tab represents a resource of the specified
type, and each actual setting is named identical or similarly in both the \acs{GUI}
and the file. For example, the following configuration file
\begin{bVerbatim}
  Monitor {
    Name="wgb-sql14b-con"
    Refresh Interval = 1001
    Display Advanced Options = yes
  }
  Client {
    Name = "wgb-sql14b-fd"
    Address = "localhost"
    Password = "85test-onremote"
    Port = 9102
    Connect Timeout = 10
    Remote = yes
    Monitor = yes
  }
\end{bVerbatim}
exactly represents the settings in the \acs{GUI} as seen in figure~\vref{blb:fig:tray-monitor-conf-mon-cli}.

\bimageH[11.7cm]{tray-monitor-conf-mon-cli}{Tray Monitor Configuration as seen in Listing}{blb:fig:tray-monitor-conf-mon-cli}

\subsubsection{Default locations of configuration files}

The default locations of user specific configuration files that are written by
the GUI are \bfilename{/home/username/.bacula-tray-monitor.conf} (Linux) and
\bfilename{C:\Users\username\AppData\Roaming\bacula-tray-monitor.conf} (Windows,
note that AppData is a hidden directory).
 
\subsection{Monitoring}
\bimageH[8.91cm]{tray-monitor-mon-cli}{Tray Monitor Client Display}{blb:fig:tray-monitor-mon-cli}

After double-clicking the Tray Monitor icon, or when selecting the
``Display\ldots{}'' item in its context menu, the monitor screen is shown. In this
screen, a tab selects which of the configured components of \bacula{} to show.

The monitoring panel consists of three areas, well known to any \bacula{}
administrator, but probably not so easily understood by users without
prior \bacula{} knowledge. On top, the general daemon status is shown. This gives
information about the identity of the component, in particular the configured
name, the version of the software, which plugins are currently loaded, and when
the service process was started. In addition, some global settings may be shown
-- for a File Daemon, this is the configured Bandwidth Limit. Note that
in-depth diagnostic information as presented by the \btool{bconsole}
\bcommandname{status} command is \textbf{not} shown.
\medskip

Below the general status a list shows all currently running jobs. This list
contains the JobID and the job's name, and as seen in
figure~\vref{blb:fig:tray-monitor-mon-cli}, with the number of files and the amount
of data already processed also gives some indication of job progress. The Error
counter column, while looking important, is much less so than it appears,
since actual error causes or messages can not be seen here.
\medskip

Finally, the lowermost part of the monitoring panel shows an overview of the
last jobs run by the particular daemon. Status text field is color coded (e.g.
green for successful jobs).
\bigskip

Note that column headers can be clicked to change the sort order of the table,
and column widths can be modified by dragging the table header column
separators.
\bigskip

In the bottom right corner of the monitoring panel there is an icon to
immediately refresh the display, while outside of the monitoring panel and the
tab view, in the bottom left corner of the display window, the automatic
refresh interval can be adjusted.

The ``Close'' button at the bottom right corner will close the display window
in the same way as a click on the window decoration close button does.


\subsection{Running Jobs}
You can start a job by selecting ``Run\ldots{}'' from the context menu of the
tray icon.

If more than one Director or Clients with ``Remote'' capability are configured,
a window will be shown to select the Director to use for the Job to be run.
This window will appear as shown in
figure~\vref{blb:fig:tray-monitor-run-dirselect}. The name used in the selection
drop-down menu is the one configured in the ``Name:'' setting of the component,
see above in the ``Configuration'' section.

\bimageH[2.38cm]{tray-monitor-run-dirselect}{Selecting a Director to run a Job}{blb:fig:tray-monitor-run-dirselect}

The ``Run Job'' window will be shown, which presents Job properties
and an estimate of how much data will be backed up. The estimate will be
updated when Job properties are changed. Be aware that this estimate is
not based on current backup source information, i.\,e. it is not the
result that you would get with the \bcommandname{estimate} command, but it is
derived using a simple regression from Catalog information.
The information presented is, however, usually enough to determine if
the Job can finish during the remaining working hours.
\medskip

Depending on the general Monitor configuration setting to ``Display Advanced Options'',
a second, tabbed panel will be available and
allow to change many other Job parameters, such as the backup level,
the File Set to use, or the backup target like Pool and Storage device.
\medskip

All settings are subject to access control restrictions configured at
the Director, thus it is possible to have specific and secure options
per each user (with each user connecting to their ``own'' access
controlled Console resource).
%TODO: Point to relevant main manual chapter, or - better - a
% streamlined document describing \acsp{ACL}

\subsection{Local Scheduling -- the Command Directory}
\label{blb:TrayMonitorCommandDirectoryChapter}
The Tray Monitor configuration item \texttt{Command Directory} can be used to
automate \bacula{} activities through the Tray Monitor itself. In particular it
allows to schedule jobs on the client machine, and not (only) inside the
Director's configuration.

If configured with a path readable by the Tray Monitor program, the
Tray Monitor, while running, will regularly look for files ending on
\bfilename{".bcmd"} and process them. Once a file was processed, it will
be renamed to end with \bfilename{.bcmd.ok}. These command files need to
follow a specific syntax:

\begin{bVerbatim}
  component-name: bconsole-command
\end{bVerbatim}

\texttt{component-name} must be one of the defined Director or Client
resources (Client only with \bvalueddirective{Remote}{Yes}).

\texttt{bconsole-command} is just a plain \bconsolename{} command which
will be sent to the Director (possibly through the File Daemon).

The most useful command here is \bcommandname{run job=job-name} to initiate backups,
where \texttt{job-name} would be the one that backs up from the local machine.
\bigskip

The solution to schedule backups of the local machine is accordingly
to have \btool{cron} or Windows' Task Scheduler create appropriate
command files in the right location.

Part of that scheduled task could be to verify connectivity to the
Director. An example suitable on a Linux or Unix system might look like
this:

\begin{bVerbatim}
  #!/bin/sh
  if ping -c 1 director &> /dev/null
  then
    echo "my-dir: run job=backup" > /path/to/commands/backup.bcmd
  fi
\end{bVerbatim}

On a Windows system, a batch script like the following could be used:

\begin{bVerbatim}
  @ECHO OFF
  ping -n 1 -4 lsb-236.lsb.os.baculasystems.com >nul:
  if ERRORLEVEL 1 (
     echo Director not reachable!
  ) else (
     echo wgb-sql14b-fd: run job=wgb-sql14b-all>%LOCALAPPDATA%\Bacula\commands\runnow.bcmd
  )
\end{bVerbatim}

Using \btool{cron} to start this sort of script is pretty simple for
a user with some Linux or Unix knowledge. Unfortunately, creating a
scheduled task on a Windows machine is somewhat difficult to explain
due to the number of required mouse clicks. We may provide step-by-step
instructions at a later time.


\subsection{Proxied Connection}
\label{blb:ch:ui-traymon:proxyconn}
With the Tray Monitor program providing a user friendly interface,
and support for local scheduling, there's only one element missing
to enable \bacula{} backing up computers that are unreliably available
and may not be accessible from the Director itself: A way to enable the
Director to run a Job  on a given client, even if, from the Director,
the client can not be accessed.

For many desktop and most laptop users, \bacula{} until now was not a
very convenient solution, because it relied on the Director being
configured with the client computer's \acs{IP} address or host name, and
then the Director being able to connect to the client.

In many corporate networks, however, there is neither \acs{DNS} resolution nor
static \acs{IP} addressing in place for desktop computers. Furthermore, if
the client computer is connected outside of the organization's \acs{LAN},
for example in a home office behind a \acs{NAT}'ing router, there will be no
reliable way to connect to it at all.

In those situations, it is essential that the File Daemon connects to the
Director, and the Director then uses this \acs{TCP} connection to control the
backup Job.

This is possible as of version~8.6 of \bee{}
%TODO: and version 7.? of the community edition
using the \textit{Client-Initiated Connection} feature. Note that both
Director and File Daemon need to support this functionality.

As we assume that such a setup of \bacula{} will usually be implemented to
back up user computers, we will provide the documentation including
a restricted console using \acsp{ACL}.
\bigskip

We will first provide a description of the connection flow for a job
that makes use of the proxied console connection, and then describe
the required configuration in detail.

\subsubsection{Connection Flow}
For a Client-Initiated Backup to work through an FD-initiated connection,
the following sequence of events will happen:
\begin{benumerate}
\item Console wants to start a Job, either manually or automated.\\
  The Director to run this job on should be the local FD, and, if the
  Tray Monitor is used, it should be configured with
  \texttt{Remote = Yes}.
\item The Console (most likely, the Tray Monitor program) connects to the
  local FD.\\
  Inside the FD configuration, such incoming connections are represented
  as a Director resource with two new settings: \texttt{Remote} and
  \texttt{Console}.
\item The Console submits the \texttt{proxy} command to the FD.\\
  Following this, the FD will use the Console resource referenced by the
  Director resource the current Console connection uses and use the
  provided information (Address, Name, and Password) to try to establish
  a connection to the ``real'' Director. If that succeeds, all further
  incoming console commands are directly passed on to that Director.
\item The Console submits the backup Job \bcommandname{run} command with the
  new option \texttt{fdcalled=1}.
\item The Director checks against \acsp{ACL} configured inside its Console
  resource representing the current connection if the Job is allowed,
  and if it is, it first marks the existing connection from the FD as
  related to this Job, and then runs the Job.
\item When the DIR starts using the established connection for
  a Job, the FD closes the connection to the incoming console; the connection
  to the DIR is used exclusively for Job-related communication, and
  thus a console connected to it could not be used for anything useful
  from this moment on.
\item The Director, following its regular mode of operation, eventually
  sends a command to the FD telling it which Storage Daemon to contact
  for data transfers.
\item The File Daemon then connects to the SD as usual.
\item The Backup Job progresses as usual, and at the end, the FD will
  report the final Job status to the DIR.
\item Eventually, the \acs{TCP} connection between FD and DIR will be properly
  shutdown.
\end{benumerate}

The above step-by-step description already indicates where specific
configuration for this operational mode is required:
\begin{bitemize}
\item The console program (either \btool{bconsole} or the \btool{Tray Monitor})
  needs to be configured to connect to a File Daemon (usually
  the one running locally) instead of directly to a DIR.
\item The File Daemon used needs to be configured with at least one
  additional Director resource with specific settings, one of them
  being the reference to a \bresourcename{Console} resource.
\item In the Director's configuration, a client-specifc \bConsole{}
  resource should be used, and this should be restricted in capabilities
  as far as possible.
\item A Job that has to use a FD-initiated connection needs to be
  started with a specific new option, namely \texttt{fdcalled=1}.
\end{bitemize}

For an experienced \bacula{} administrator, having to add \bConsole{} and more
\bDirector{} resources to a File Daemon configuration will be unexpected.
For new \bacula{} administrators, the complexity of the configuration scheme
(which, as usual, is a result of the built-in flexibility) may be
intimidating.

For this reason, we provide an overview of the required configuration
in figure~\vref{blb:fig:tray-monitor-and-related-conf-overview}.

\bimageH[\linewidth,angle=90]{cib-overview}{Configuration overview: resources and their relationships}{blb:fig:tray-monitor-and-related-conf-overview}
%% \begin{figure}[p]
%% \includegraphics[angle=90,keepaspectratio,width=\linewidth,totalheight=19cm]{Overview}
%% \caption{Configuration overview: resources and their relationships}
%% \label{blb:fig:tray-monitor-and-related-conf-overview}
%% \end{figure}

In the overview figure, we have included a \bconsole{}
configuration which has not yet been mentioned. However, it works
essentially identical to the Tray Monitor program connectivity, but
allows -- and requires! -- the user to run arbitrary commands with all
possible options. At this time, this is the only way to restore
data using Client-Initiated connections.
\medskip

The figure \vref{blb:fig:tray-monitor-and-related-conf-overview} shows all needed configuration entities, in some cases
excerpts from the full configuration, in others the complete files.

Colored arrows indicate the relationships between resources. Most of
them are also relationships between daemons potentially running on
separate hosts, accordingly, any of those will require \acs{TCP}/\acs{IP}
connectivity, including routing, name resolution, firewall and
\acs{TCP}~Wrapper configuration. However, managing those aspects should
not be an additional effort for \bacula{} administrators, as no additional
requirements are introduced, nor will it be a problem for most users,
as the common desktop configurations allow outgoing connections already.

\subsubsection{Resource relationships}
When looking at the configuration overview in
figure~\vref{blb:fig:tray-monitor-and-related-conf-overview}, it is easy
to spot distinct relationships. We will discuss them, starting at the
console program used.

\paragraph{Console} programs intended to use a proxied connection to
the Director, or intended to start a Job through a client-initiated
connection, need to be configured to contact the File Daemon used to
serve as proxy to the Director, not the Director itself. Thus, the
configuration will (usually) use \bdirectivename{Address = localhost} and a
port number of \bvalue{9102}.

Note that \btool{bconsole} is essentially agnostic of the fact that
it connects to a File Daemon, not a Director, and thus uses a
Director configuration resource, while the Tray Monitor program does
know about those features and accordingly uses a slightly different
configuration scheme with a Client resource with \bdirectivename{Remote = Yes}
set.

Also, \btool{bconsole} and the Tray Monitor program use different
resources to define themselves (which is where the name used for
authentication is taken from), so there will be different ``Identity''
resources in use. It is possible and may be reasonable to use the
same name and password, though, as the File Daemon can use the same
resource to configure the properties of the connecting agent.
\medskip

In figure~\vref{blb:fig:tray-monitor-and-related-conf-overview}, the
relationship between Tray Monitor and (local) File Daemon configuration
resources is shown in green, and the relationship from a
\btool{bconsole} configuration to that of the File Daemon is shown in
orange.

\paragraph{File Daemon} configuration for proxied connections to a
Director are a bit more complex.

First, the File Daemon needs to accept incoming user agent connections
from \btool{bconsole} or the Tray Monitor program. However, these
connections will (potentially) not only be used to query the FD's
status, but also to submit more powerful commands, so a Monitor resource
is not suitable.

Instead, the File Daemon has essentially to act as a Director for the
user agent, so this functionality is configured in a \bDirector{} resource.

This is essentially just a ``normal'' \bDirector{} resource, but it is
advisable to have distinct ones for ``real'' \bacula{} Directors as needed
(usually there will only be one ``real'' Director) and for incoming user agents (also, there
will usually be exactly one of them).

This is because direct Director connections may still be required in
some cases, and it is surely advisable to have different passwords
for a Director and for a user agent. Also, it is much easier to
understand log files if different names are used for real Directors and
user agent connections.

To enable the proxy functionality of the File Daemon, the directive
\bdirectivename{Remote} needs to be set to \bvalue{yes} for those \bDirector{}
resources representing incoming user agent connections.

In addition, a \bdirectivename{Console} directive should be present and indicate
a \bConsole{} resource used to contact the ``real'' Director -- see below
for details.

If no Console is referenced, the File Daemon will automatically select
a Console it finds in its configuration, but since it is possible to have
several of them defined, each with different properties, it is much
safer and more clearly understandable what is configured if an explicit
Console reference is used.

In the overview in figure~\vref{blb:fig:tray-monitor-and-related-conf-overview}, this Console reference is indicated in blue.
\bigskip

The Console resource of the File Daemon specifies connection
information and credentials to contact a ``real'' Director as usual.

It is advisable to use distinct names and passwords for each such
Director connection, so that access can be controlled at least with the
granularity of client machines.

However, if different users on a given machine will be using proxied
connections to one or more Directors, there should be distinct Director
resources per user, each using its own set of credentials.

As the File Daemon configuration file should be protected against user
access, this allows separating user permissions and will allow to
distinguish activity logged.

However, the local machine administrator will always be able to read
all user credentials, thus needs to be trustworthy to at least the
extent all the users combined are trusted.

Additional considerations apply to logging of
File Daemon activity. Usually all File Daemon activity is logged to a
default Director (Job-related activity is always logged to the initiating
Director). However, in environments making use of the Client-Initiated
Connection facilities, the central Director may not be a suitable target
for the File Daemon messages as it will be inaccessible most of the
time.

Thus we recommend to configure the File Daemon to write its messages to
a local log file or logging daemon.

\paragraph{Director} configuration for use in an environment using
Client-Initiated Connections relies heavily on the use of Named Consoles
with \acsp{ACL}. In the configuration overview of
figure~\vref{blb:fig:tray-monitor-and-related-conf-overview}, we show one
such Console resource and red arrows from the File Daemon's \bConsole{}
resource indicate how it is referenced from the (proxying) File Daemon.

We also show the ``regular'' configuration, with the File Daemon being
contacted by the Director (the Director-side Client resource is not
in the overview) with a faint red arrow. The important part here is that
authentication information is not shared between the two different
connection direction setups.

\subparagraph{Access Control Lists} should be in place in the
Director-side configuration for all Console resources that are
used by non-administrative users. Only \bacula{} instance administrators
should ever have full access to a Director.

Using the configuration scheme outlined, it is easily possible to have
per-user Console configurations, typically restricting access to only
the required resources. Most of the time, for example, there will
be only one client and one backup Job allowed for such a Console
connection.

In the overview graph we present a simplified, not very closely
restricted Console resource.

In practice, much more restrictive configurations will be applied.
We trust that a \bacula{} administrator will know which commands their users
are supposed to execute, and which resources they need to access.
However, with the Tray Monitor program, it may not be obvious which
commands in particular are required, thus we provide the full list
of mandatory commands here. Note that resource (for example, Job, Pool
and Storage) restrictions need to be added.

A typical \bdirectivename{Command ACL} directive for a Console used by a
Tray Monitor which is allowed to run Jobs will look require to
have at least the following contents:
\begin{bVerbatim}
  CommandACL = status, .estimate, .clients, .jobs, .pools, .storage, .filesets, run
\end{bVerbatim}


\subsubsection{Using \btool{bconsole} for User-Initiated Jobs}
Besides using the Tray Monitor utility to monitor and control a
Director, it is also possible to use the console program
\btool{bconsole} with the File Daemon as a proxy, including the
ability to start jobs which can use the File Daemon-initiated Director
connection.

At the current time, this is actually the only reliable way to
enable users with a restricted console connection and without full
access from the Director to their desktop to restore from their backups.

To use these facilities, three items are important:
\begin{benumerate}
\item The programs involved need to be configured accordingly, i.\,e. as
  outlined above.
\item To initiate the proxied connection through the File Daemon,
  before any command intended for the Director is entered, the command
  \texttt{proxy} needs to be submitted. It should return with an ``Ok''
  message.
\item Any job started that should use the established console
  connection to the Director needs the additional parameter
  \texttt{fdcalled=1} in its command line.
\end{benumerate}

Note that the extra command and parameter are not required when
submitting commands through the Tray Monitor program's command file
facility. Also note that once a job has started, the console
connection between File Daemon and \btool{bconsole} will be terminated,
causing \btool{bconsole} to end. Job status can be observed with a new
\btool{bconsole} invocation; in fact, the status of the local File
Daemon can then be observed without going through the Director. An example session is shown in figure~\vref{blb:fig:bconsole-fd-pcc-session}.

\bimageH[7.5cm]{bconsole-fd-pcc-session}{Remote, Proxied \texttt{bconsole} Session with Job Run}{blb:fig:bconsole-fd-pcc-session}

%%\subsubsection{Configuration File}

\subsection{Monitoring}

\subsection{Running Jobs}

\subsection{Proxied Connection}


\subsection{Supported Platforms}
The Tray Monitor program is available for the following platforms:
\begin{bitemize}
\item Microsoft Windows, both 32 and 64 bit, on all Windows versions still
  under vendor support. % no, we will not answer questions re Server 2k3 or XP
\item Linux:
  \begin{bitemize}
  % TODO: Complete the list: distros, versions, desktops
  \item Ubuntu: 14.04, x86 and x86-64, with XFCE desktop
  \item Red Hat Enterprise Linux and clones like CentOS: 6.x, 7.x,
    x86 and x86-64, with XFCE desktop
  \end{bitemize}
  %\item Mac OS X %%%TODO: verify

\subsection{Command Line Options}
The Tray Monitor program accepts the following command line options and
parameters:
\begin{bdescription}
  \item[-c \bbracket{filename}] Configuration file to use. If the file does not exist,
 the program starts with an empty configuration and shows the configuration
  panel.
  \item [-d \bbracket{number}] Debug level. The higher the number, the more information
  is displayed.
  \item [-dt] Print timestamps in debug output.
  \item[-t] Test the configuration and exit. No output is a good sign.
  \item[-W \bzeroone{}] Force detection of systray capability. A zero indicates to
  run without tray icon, a one forces to use tray capability even if not
  detected.
  \item [-?] Display short help and exit.
\end{bdescription}
\end{bitemize}
