\chapter{The Current State of \bacula{}}
\label{blb:StateChapter}
\index[general]{Current state of \bacula{}}

In other words, what is and what is not currently implemented and functional.

\section{What is Implemented}
\index[general]{Implemented!What}
\index[general]{What is Implemented}

\begin{bitemize}
\item Job Control
   \begin{bitemize}
   \item Network backup/restore with centralized Director.
   \item Internal scheduler for automatic
      \bilink{Job}{blb:JobDef} execution.
   \item Scheduling of multiple Jobs at the same time.
   \item You may run one Job at a time or multiple simultaneous Jobs
         (sometimes called multiplexing).
   \item Job sequencing using priorities.
   \item \bilink{Console}{blb:UADef} interface to the Director allowing complete
      control.  A shell, Qt5 \acs{GUI}, and Web versions of
      the Console program are available.  Note, the Qt5 \acs{GUI} program called
      the \bat{}, offers many additional features over the shell program.
   \end{bitemize}

\item Security
   \begin{bitemize}
   \item Verification of files previously cataloged, permitting a Tripwire like
      capability (system break-in detection).
   \item \acs{CRAM}-\acs{MD}5 password authentication between each component (daemon).
   \item Configurable
      \bilink{\acs{TLS} (\acs{SSL}) communications encryption}{blb:CommEncryption} between each
            component.
   \item Configurable
   \bilink{Data (on Volume) encryption}{blb:DataEncryption}
      on a Client by Client basis.
   \item Computation of \acs{MD}5, \acs{SHA}1 signatures of the file data if requested.
   \end{bitemize}


\item Restore Features
   \begin{bitemize}
   \item Restore of one or more files selected interactively either for the
      current backup or a backup prior to a specified time and date.
   \item Restore of a complete system starting from bare  metal. This is mostly
      automated for Linux systems and  partially automated for Solaris. See
      \bilink{Disaster Recovery Using \bacula{}}{blb:RescueChapter}. This is also
      reported to work on Win2K/XP systems.
    \item Listing and Restoration of files using stand-alone \btool{bls} and \btool{bextract}
      tool programs. Among other things, this permits extraction of
      files when \bacula{} and/or the catalog are not available. Note, the
      recommended way to restore files is using the \bcommandname{restore} command in the
      Console. These programs are designed for use as a last resort.
   \item Ability to restore the catalog database rapidly by using bootstrap
      files (previously saved).
   \item Ability to recreate the catalog database by scanning backup Volumes
      using the \btool{bscan} program.
   \end{bitemize}

\item SQL Catalog
   \begin{bitemize}
   \item Catalog database facility for remembering Volumes, Pools, Jobs,  and
      Files backed up.
    \item Support for \mysql{} and \postgresql{} Catalog databases. \sqlite{} is
      deprecated and will ultimately be removed.  We strongly recommend that you
     not use \sqlite{}.
   \item User extensible queries to the \mysql{} and \postgresql{} databases.
   \end{bitemize}

\item Advanced Volume and Pool Management
   \begin{bitemize}
   \item Labeled Volumes, preventing accidental overwriting  (at least by
      \bacula{}).
   \item Any number of Jobs and Clients can be backed up to a single  Volume.
      That is, you can backup and restore Linux, Unix, Sun, and  Windows machines to
      the same Volume.
   \item Multi-volume saves. When a Volume is full, \textbf{\bacula{}}  automatically
      requests the next Volume and continues the backup.
   \item
      \bilink{Pool and Volume}{blb:PoolResource} library management
      providing Volume flexibility (e.g. monthly, weekly, daily Volume sets,  Volume
      sets segregated by Client, \ldots{}).
   \item Machine independent Volume data format. Linux, Solaris, and Windows
      clients can all be backed up to the same Volume if desired.
   \item The Volume data format is upwards compatible so that old Volumes
      can always be read.
   \item A flexible
      \bilink{message}{blb:MessagesChapter}  handler including routing
      of messages from any daemon back to the  Director and automatic email
      reporting.
   \item Data spooling to disk during backup with subsequent write to tape from
      the spooled disk files. This prevents tape ``shoe shine''  during
      Incremental/Differential backups.
   \end{bitemize}

\item Advanced Support for most Storage Devices
    \begin{bitemize}
   \item Autochanger support using a simple shell interface that can interface
      to virtually any autoloader program. A script for \mtx{} is  provided.
   \item Support for autochanger barcodes -- automatic tape labeling from
      barcodes.
   \item Automatic support for multiple autochanger magazines either using
      barcodes or by reading the tapes.
   \item Support for multiple drive autochangers.
   \item Raw device backup/restore. Restore must be to the same device.
   \item All Volume blocks (approximately 64K bytes) contain a data checksum.
   \item Migration support -- move data from one Pool to another or
         one Volume to another.
   \item Supports writing to \acs{DVD}.
   \end{bitemize}

\item Multi-Operating System Support
   \begin{bitemize}
   \item Programmed to handle arbitrarily long filenames and messages.
   \item \acs{GZIP} compression on a file by file basis done by the Client program  if
      requested before network transit.
    \item Saves and restores \acs{POSIX} \acsp{ACL} and Extended Attributes on most \acs{OS}es if
      enabled.
   \item Access control lists for Consoles that permit restricting user access
      to only their data.
   \item Support for save/restore of files larger than 2GB.
   \item Support for 64 bit machines, e.g. amd64, Sparc.
   \item Support ANSI and IBM tape labels.
   \item Support for Unicode filenames (e.g. Chinese) on Win32 machines
   \item Consistent backup of open files on Win32 systems (WinXP, Win2003,
         and Vista)
         but not Win2000, using Volume Shadow Copy (\acs{VSS}).
   \item Support for path/filename lengths of up to 64K on Win32 machines
         (unlimited on Unix/Linux machines).
   \end{bitemize}

\item Miscellaneous
   \begin{bitemize}
   \item Multi-threaded implementation.
   \item A comprehensive and extensible
      \bilink{configuration file}{blb:DirectorChapter} for each daemon.
   \end{bitemize}
\end{bitemize}

\section{Advantages Over Other Backup Programs}
\index[general]{Advantages of \bacula{} Over Other Backup Programs }
\index[general]{Programs!Advantages of \bacula{} Over Other Backup }

\begin{bitemize}
\item Since there is a client for each machine, you can backup
   and restore clients of any type ensuring that all attributes
   of files are properly saved and restored.
\item It is also possible to backup clients without any client
   software by using \acs{NFS} or Samba.  However, if possible, we
   recommend running a Client File daemon on each machine to be
   backed up.
\item \bacula{} handles multi-volume backups.
\item A full comprehensive \acs{SQL} standard database of all files backed up.  This
   permits online viewing of files saved on any particular  Volume.
\item Automatic pruning of the database (removal of old records) thus
   simplifying database administration.
\item Any \acs{SQL} database engine can be used making \bacula{} very flexible.
  Drivers currently exist for \mysql{}, \postgresql{}, and \sqlite{}.
  Note: SQLite is deprecated; it is strongly recommended not to use it.
\item The modular but integrated design makes \bacula{} very scalable.
\item Since \bacula{} uses client file servers, any database or
   other application can be properly shutdown by \bacula{} using the
   native tools of the system, backed up, then restarted (all
   within a \bacula{} Job).
\item \bacula{} has a built-in Job scheduler.
\item The Volume format is documented and there are simple \texttt{C} programs to
   read/write it.
\item \bacula{} uses well defined (\acs{IANA} registered) \acs{TCP}/\acs{IP} ports -- no rpcs,  no
   shared memory.
\item \bacula{} installation and configuration is relatively simple compared  to
   other comparable products.
\item According to one user \bacula{} is as fast as the big major commercial
   applications.
\item According to another user \bacula{} is four times as fast as another
   commercial application, probably because that application  stores its catalog
   information in a large number of individual  files rather than an \acs{SQL} database
   as \bacula{} does.
\item Aside from several \acs{GUI} administrative interfaces, \bacula{} has a
   comprehensive shell administrative interface, which allows the
   administrator to use tools such as \btool{ssh} to administrate any part of
   \bacula{} from anywhere (even from home).
\item \bacula{} has a Rescue \acs{CD} for Linux systems with the following features:
   \begin{bitemize}
   \item You build it on your own system from scratch with one simple  command:
      \btool{make} -- well, then make burn.
   \item It uses your kernel
   \item It captures your current disk parameters and builds scripts that  allow
      you to automatically repartition a disk and format it to  put it back to what
      you had before.
   \item It has a script that will restart your networking (with the right  \acs{IP}
      address)
   \item It has a script to automatically mount your hard disks.
   \item It has a full \bacula{} \acs{FD} statically linked
   \item You can easily add additional data/programs, \ldots{} to the disk.
   \end{bitemize}

\end{bitemize}

\section{Current Implementation Restrictions}
\index[general]{Current Implementation Restrictions }
\index[general]{Restrictions!Current Implementation }

\begin{bitemize}
\item It is very unusual to attempt to restore two Jobs
   that ran simultaneously in a single restore, but if
   you do, please be aware that unless you had
   data spooling turned on and the spool file held the full
   contents of both Jobs during the backup, the restore will not
   work correctly. In other terms, \bacula{} cannot restore
   two jobs in the same restore if the Jobs' data blocks were
   intermixed on the backup medium. The problem is resolved by
   simply doing two restores, one for each Job.
   Normally this can happen only if you manually enter specific
   JobIds to be restored in a single restore Job.
\item \bacula{} can generally restore any backup made from one client
   to any other client. However, if the architecture is significantly
   different (i.e. 32 bit architecture to 64 bit or Win32 to Unix),
   some restrictions may apply (e.g. Solaris door files do not exist
   on other Unix/Linux machines; there are reports that Zlib compression
   written with 64 bit machines does not always read correctly on a 32 bit
   machine).
\end{bitemize}

\section{Design Limitations or Restrictions}
\index[general]{Restrictions!Design Limitations or }
\index[general]{Design Limitations or Restrictions }

\begin{bitemize}
\item Names (resource names, Volume names, and such) defined in \bacula{}
   configuration files are limited to a fixed number of
   characters.  Currently the limit is defined as 127 characters.  Note,
   this does not apply to filenames, which may be arbitrarily long.
\item Command line input to some of the stand alone tools -- e.g. \btool{btape},
   \btool{bconsole} is restricted to several hundred characters maximum.
   Normally, this is not a restriction, except in the case of listing
   multiple Volume names for programs such as \btool{bscan}. To avoid
   this command line length restriction, please use a \bfilename{.bsr}
   file to specify the Volume names.
\item \bacula{} configuration files for each of the components can be
   any length. However, the length of an individual line is limited
   to 500 characters after which it is truncated.  If you need lines
   longer than 500 characters for directives such as \acsp{ACL} where
   they permit a list of names are character strings simply
   specify multiple short lines repeating the directive on
   each line but with different list values.

\end{bitemize}

\section{Items to Note}
\index[general]{Items to Note}
\begin{bitemize}
\item \bacula{}'s Differential and Incremental \emph{normal} backups are based
  on time stamps.  Consequently, if you move files into an existing directory
  or move a whole directory into the backup fileset after a Full backup, those
  files will probably not be backed up by an Incremental save because they will
  have old dates.  This problem is corrected by using Accurate mode backups
  or by explicitly updating the date/time stamp on all moved files.
\item In older versions of \bacula{} ($\leq{}$ 3.0.x), if you have over 4 billion file
  entries stored in your database, the database FileId is likely to overflow.
  This limitation does not apply to current \bacula{} versions.
\item In non \emph{Accurate} mode, files deleted after a Full save will be
  included in a restoration. This is typical for most similar backup programs.
  To avoid this, use Accurate mode backup.
\end{bitemize}
