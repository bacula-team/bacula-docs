\subsection{Include All Windows Drives in FileSet}

The \texttt{alldrives} Windows Plugin allows you to include all local drives
with a simple directive. This plugin is available in the Windows 64 and 32 bit
installer.

\begin{bVerbatim}
FileSet {
 Name = EverythingFS
 ...
 Include {
   Plugin = "alldrives"
 }
}
\end{bVerbatim}

You exclude some specific drives with the \texttt{exclude} option.

\begin{bVerbatim}
FileSet {
 Name = EverythingFS
 ...
 Include {
   Plugin = "alldrives: exclude=D,E"
 }
}
\end{bVerbatim}

The alldrives Windows plugin only considers regular drives (i.e. \bdirectoryname{C:/},
\bdirectoryname{D:/}, \bdirectoryname{E:/}).
Mount points inside the directory tree will no be part of the snapshots
unless you set ``\bdirectivename{OneFS = no}'' in the \bresourcename{Options}
block of the FileSet, which will cause \bacula{} to snapshot everything.

\subsection{Microsoft \acs{VSS} Writer Plugin}
\index[general]{Microsoft \acs{VSS} Writer Plugin}
We provide a single plugin named \btool{vss-fd.dll} that
permits you to backup a number of different components
on Windows machines.   This plugin is available from \bacula{} Systems
as an option.

Please \bfootref{mailto:support@baculasystems.com}{write to the Support Team} to find more information about our \acs{VSS} products.

\begin{bitemize}
\item System State writers
   \begin{bitemize}
   \item Registry
   \item Event Logs
   \item COM+ REGDB (COM Registration Database)
   \item System (Systems files -- most of what is under c:/windows and more)
   \item \ac{WMI}
   \item \ac{NTDS} (Active Directory)
   \item \ac{NTFRS} (SYSVOL etc replication -- Windows 2003 domains)
   \item \ac{DFS} Replication (SYSVOLS etc replication -- Windows 2008 domains)
   \item \ac{ASR} Writer
   \end{bitemize}
   This component is known to work.
\item \mssql{} databases (except those owned by Sharepoint if that plugin is
  specified).\\
  This component has been tested, but only works for Full backups. Please
  do not attempt to use it for incremental backups. The Windows writer
  performs block level delta for Incremental backups, which are only
  supported by \bacula{} version 4.2.0, not yet released.  If you use
  this component, please do not use it in production without careful
  testing.
\item Exchange (all exchange databases) \\
  We have tested this component and found it to work, but only for Full
  backups.  Please do not attempt to use it for incremental or differential
  backups.  We are including this component for you to test.  Please do not
  use it in production without careful testing.  \\ \bacula{} Systems has a
  White Paper that describes backup and restore of MS Exchange 2010 in
  detail.
\end{bitemize}

Each of the above specified Microsoft components can be backed up
by specifying a different plugin option within the \bacula{} FileSet.
All specifications must start with \textbf{vss:} and be followed
with a keyword which indicates the writer, such as \textbf{/@SYSTEMSTATE/} 
(see below).
To activate each component you use the following:

\begin{bitemize}
\item System State writers
  \begin{bVerbatim}
Plugin = "vss:/@SYSTEMSTATE/"
  \end{bVerbatim}
  Note, exactly which subcomponents will be backed up depends on
  which ones you have enabled within Windows.  For example, on a standard
  default Vista system only ASR Writer, COM+ REGDB, System State, and \acs{WMI}
  are enabled.
\item \mssql{} databases (except those owned by Sharepoint if that plugin is
specified)
  \begin{bVerbatim}
Plugin = "vss:/@MSSQL/"
  \end{bVerbatim}
   The Microsoft
   literature says that the mssql writer is only good for snapshots
   and it needs to be
   enabled via a registry tweak or else the older MSDE writer will be
   invoked instead.
\item Exchange (all exchange databases)
  \begin{bVerbatim}
Plugin = "vss:/@EXCHANGE/"
  \end{bVerbatim}
\end{bitemize}

The plugin directives must be specified exactly as shown above.
A Job may have one or more of the \textbf{vss} plugins components specified.


Also ensure that the \btool{vss-fd.dll} plugin is in the plugins directory
on the FD doing the backup, and that the plugin directory config line is
present in the FD's configuration file (\bfilename{bacula-fd.conf}).

\subsubsection{Backup}
If everything is set up correctly as above then the backup should
include the system state. The system state files backed up will appear
in a \btool{bconsole} or \bat{} restore like:

\begin{bVerbatim}
/@SYSTEMSTATE/
/@SYSTEMSTATE/ASR Writer/
/@SYSTEMSTATE/COM+ REGDB Writer/
\end{bVerbatim}
etc



Only a complete backup of the system state is supported at this time.  That
is it is not currently possible to just back up the Registry or Active
Directory by itself.  In almost all cases a complete backup is a good idea
anyway as most of the components are interconnected in some way.  Also, if
an incremental or differential backup is specified on the backup Job then a
full backup of the system state will still be done.  The size varies 
according to your installation.  We have seen up to 6GB
under Windows 2008, mostly because of the ``System'' writer, and
up to 20GB on Vista.  The actual size depends on how many Windows
components are enabled.

The system state component automatically respects all the excludes present
in the FilesNotToBackup registry key, which includes things like \bdirectoryname{\%TEMP\%},
\bfilename{pagefile.sys}, \bfilename{hiberfil.sys}, etc.  Each plugin may additionally specify
files to exclude, eg the \acs{VSS} Registry Writer will tell \bacula{} to not back
up the registry hives under \bdirectoryname{C:\textbackslash{}WINDOWS\textbackslash{}system32\textbackslash{}config} because they
are backed up as part of the system state.

\subsubsection{Restore}
In most cases a restore of the entire backed up system state is
recommended. Individual writers can be selected for restore, but currently
not individual components of those writers. To restore just the Registry,
you would need to mark \texttt{@SYSTEMSTATE} (only the directory, not the
subdirectories), and then do \textbf{mark Registry*} to mark the Registry writer
and everything under it.

Restoring anything less than a single component may not produce the
intended results and should only be done if a specific need arises and you
know what you are doing, and not without testing on a non-critical system
first.

To restore Active Directory, the system will need to be booted into
Directory Services Restore Mode, an option at Windows boot time.

Only a non-authoritative restore of \acs{NTFRS}/\acs{DFSR} is supported at this
time. There exists Windows literature to turn a \ac{DC}
restored in non-authoritative mode back into an authoritative Domain
Controller. If only one \ac{DC} exists it appears that Windows does an
authoritative restore anyway.

Most \acs{VSS} components will want to restore to files that are currently in
use. A reboot will be required to complete the restore (eg to bring the
restored registry online).

Starting another restore of \acs{VSS} data after the restore of the registry
without first rebooting will not produce the intended results as the
``to be replaced next reboot'' file list will only be updated in the
``to be replaced'' copy of the registry and so will not be actioned.

\subsubsection{Example}
Suppose you have the following backup FileSet:

\begin{bVerbatim}
@SYSTEMSTATE/
  System Writer/
    instance_{GUID}
    System Files/
  Registry Writer/
    instance_{GUID}
    Registry/
  COM+ REGDB Writer/
    instance_{GUID}
    COM+ REGDB/
  NTDS/
    instance_{GUID}
    ntds/
\end{bVerbatim}

If only the Registry needs to be restored, then you could use the
following commands in \btool{bconsole}:

\begin{bVerbatim}
markdir @SYSTEMSTATE
cd @SYSTEMSTATE
markdir "Registry Writer"
cd "Registry Writer"
mark instance*
mark "Registry"
\end{bVerbatim}

\subsubsection{Windows Plugins Items to Note}
\begin{bitemize}
\item Reboot Required after a Plugin Restore\\
In general after any \acs{VSS} plugin is used to restore a component, you will
need to reboot the system.  This is required because in-use files cannot be
replaced during restore time, so they are noted in the registry and
replaced when the system reboots.
\item After a System State restore, a reboot will generally take
longer than normal because the pre-boot process must move the newly restored
files into their final place prior to actually booting the \acs{OS}.
\item One File from Each Drive needed by the Plugins must be backed up\\
At least one file from each drive that will be needed by the plugin must
have a regular file that is marked for backup.  This is to ensure that the
main \bacula{} code does a snapshot of all the required drives. At a later
time, we will find a way to accomplish this automatically.
\item \bacula{} does not Automatically Backup Mounted Drives\\
Any drive that is mounted in the normal file structure using a mount point
or junction point will not be backed up by \bacula{}.  If you want it backed
up, you must explicitly mention it in a \bacula{} ``\bdirectivename{File}'' directive in your
FileSet.
\item When doing a backup that is to be used as a Bare Metal Recovery, do
not use the \acs{VSS} plugin. The reason is that during a Bare Metal Recovery, 
\acs{VSS} is not available nor are the writers from the various components that
are needed to do the restore.  You might do full backup to be used with
a Bare Metal Recovery once a month or once a week, and all other days,
do a backup using the \acs{VSS} plugin, but under a different Job name.  Then
to restore your system, use the last Full non-\acs{VSS} backup to restore your
system, and after rebooting do a restore with the \acs{VSS} plugin to get
everything fully up to date.
\end{bitemize}

This plugin is available as an option. Please contact \bacula{} Systems to
get access to the \acs{VSS} Plugin packages and the documentation.

\subsection{Support for NDMP Protocol}

The new \btool{ndmp} Plugin is able to backup a \acs{NAS} through \acs{NDMP} protocol
using \textbf{Filer to server} approach, where the Filer is backing up across
the \acs{LAN} to your \bacula{} server.

Accurate option should be turned on in the Job resource.
\begin{bVerbatim}
Job {
 Accurate = yes
 FileSet = NDMPFS
 ...
}

FileSet {
 Name = NDMPFS
 ...
 Include {
   Plugin = "ndmp:host=nasbox user=root pass=root file=/vol/vol1"
 }
}
\end{bVerbatim}

This plugin is available as an option. Please contact \bacula{} Systems to get
access to the \acs{NDMP} Plugin packages and the documentation.

\subsection{Incremental Accelerator Plugin for NetApp}

The Incremental Accelerator for NetApp Plugin is designed to simplify the
backup and restore procedure of your NetApp \acs{NAS} hosting a huge number of files.

\smallskip{} When using the NetApp \acs{HFC} Plugin, \bacula{} Enterprise will query the
NetApp device to get the list of all files modified since the last backup
instead of having to walk through the entire filesystem. Once \bacula{} have the
list of all files to back's up, it will use a standard network share (such as
\acs{NFS} or \acs{CIFS}) to access files.

\smallskip{}
This plugin is available as an option. Please contact \bacula{} Systems to get
access to the Incremental Accelerator Plugin for NetApp packages and the
documentation.


\subsection{PostgreSQL Plugin}

The \postgresql{} plugin is designed to simplify the backup and restore procedure
of your \postgresql{} cluster, the backup administrator doesn't need to learn about
internals of \postgresql{} backup techniques or write complex scripts. The plugin
will automatically take care for you to backup essential information such as
configuration, users definition or tablespaces. The \postgresql{} plugin supports
both dump and \ac{PITR} backup techniques.

\smallskip{}
This plugin is available as an option. Please contact \bacula{} Systems to get
access to the \postgresql{} Plugin packages and the documentation.


\subsection{VMWare vSphere VADP Plugin}

The \bacula{} Enterprise vSphere plugin provides virtual
machine bare metal recovery, while the backup at the guest level simplify data
protection of critical applications.

The plugin integrates the VMware's \ac{CBT} technology to
ensure only blocks that have changed since the initial Full, and/or the last
Incremental or Differential Backup are sent to the current Incremental or
Differential backup stream to give you more efficient backups and reduced
network load.

\smallskip{}
This plugin is available as an option. Please contact \bacula{} Systems to get
access to the vSphere Plugin packages and the documentation.

\subsection{Oracle RMAN Plugin}

The \bacula{} Enterprise Oracle Plugin is designed to simplify the backup and
restore procedure of your Oracle Database instance, the backup administrator
don't need to learn about internals of Oracle backup techniques or write
complex scripts.  The \bacula{} Enterprise Oracle plugin supports both dump and
\ac{PITR} with \ac{RMAN} backup techniques.

\smallskip{}
This plugin is available as an option. Please contact \bacula{} Systems to get
access to the Oracle Plugin packages and the documentation.
