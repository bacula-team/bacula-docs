\chapter{Disaster Recovery Using \bacula{}}
\label{blb:RescueChapter}
\index[general]{Disaster Recovery Using \bacula{}}
\index[general]{\bacula{}!Disaster Recovery Using}
\index[general]{Recovery!Disaster Recovery}
\index[general]{Rescue!Disaster Recovery}

\section{General}
\index[general]{General}

When disaster strikes, you must have a plan, and you must have prepared in
advance otherwise the work of recovering your system and your files will be
considerably greater.  For example, if you have not previously saved the
partitioning information for your hard disk, how can you properly rebuild
it if the disk must be replaced?
Unfortunately, many of the steps one must take before and immediately after
a disaster are very operating system dependent.  

\smallskip
\bacula{} Systems provides special Linux\acs{BMR} and Win\acs{BMR}packages that
largely automate and simplify recovery even with a very large
number of machines. The documentation and details of these products
are contained in separate white papers.  If you need them,
please ask your \bacula{} Systems sales representative.

\smallskip
Below are a few additional points that may or may not help.


\label{blb:considerations1}
\section{Important Considerations}
\index[general]{Important Considerations}
\index[general]{Considerations!Important}

Here are a few important considerations concerning disaster recovery that
you should take into account before a disaster strikes.

\begin{bitemize}
\item If the building which houses your computers burns down or is otherwise
   destroyed, do you have off-site backup data?
\item Disaster recovery is much easier if you have several machines. If  you
   have a single machine, how will you handle unforeseen events  if your only
   machine is down?
\item Do you want to protect your whole system and use \bacula{} to  recover
   everything? or do you want to try to restore your system from  the original
   installation disks and apply any other updates and  only restore user files?
\end{bitemize}


\label{blb:FreeBSD1}
\section{FreeBSD Bare Metal Recovery}
\index[general]{Recovery!FreeBSD Bare Metal}
\index[general]{Rescue!FreeBSD Bare Metal}
\index[general]{FreeBSD Bare Metal Recovery}

The same basic techniques described above also apply to FreeBSD. Although we
don't yet have a fully automated procedure, Alex Torres Molina has provided us
with the following instructions with a few additions from Jesse Guardiani and
Dan Langille:

\begin{benumerate}
\item Boot with the FreeBSD installation disk
\item Go to Custom, Partition and create your slices and go to Label and
   create the partitions that you want. Apply changes.
\item Go to Fixit to start an emergency console.
\item Create devs \bfilename{ad0} .. .. if they don't exist under \bdirectoryname{/mnt2/dev} (in my  situation)
   with \btool{MAKEDEV}. The device or devices you  create depend on what hard drives you
   have. \bfilename{ad0} is your  first ATA drive. \bfilename{da0} would by your first\acs{SCSI} drive.  Under
\acs{OS} version 5 and greater, your device files are  most likely automatically
created for you.
\item \begin{bVerbatim}
mkdir /mnt/disk
\end{bVerbatim}

   this is the root of the new disk
 \item \begin{bVerbatim}
mount /mnt2/dev/ad0s1a /mnt/disk
mount /mnt2/dev/ad0s1c /mnt/disk/var
mount /mnt2/dev/ad0s1d /mnt/disk/usr
.....
 \end{bVerbatim}
The same hard drive issues as above apply here too.  Note, under \acs{OS} version 5
or higher, your disk devices may  be in \bdirectoryname{/dev} not \bdirectoryname{/mnt2/dev}.
\item Network configuration
  \begin{bVerbatim}
ifconfig xl0 ip/mask + route add default ip-gateway
\end{bVerbatim}
\item \begin{bVerbatim}
mkdir /mnt/disk/tmp
\end{bVerbatim}
\item \begin{bVerbatim}
cd /mnt/disk/tmp
\end{bVerbatim}
\item Copy \bfilename{bacula-fd} and \bfilename{bacula-fd.conf} to this path
\item If you need to, use \btool{sftp} to copy files, after which you must do this:
  \begin{bVerbatim}
ln -s /mnt2/usr/bin /usr/bin
  \end{bVerbatim}
\item  \begin{bVerbatim}
chmod u+x bacula-fd
\end{bVerbatim}
\item Modify \bfilename{bacula-fd.conf} to fit this machine
\item Copy \bfilename{/bin/sh} to \bfilename{/mnt/disk}, necessary for \btool{chroot}
\item Don't forget to put your bacula-dir's \acs{IP} address and domain  name in
   \bfilename{/mnt/disk/etc/hosts} if it's not on a public net.  Otherwise the \acs{FD} on the
   machine you are restoring to  won't be able to contact the \acs{SD} and DIR on the
remote machine.
\item \begin{bVerbatim}
mkdir -p /mnt/disk/var/db/bacula
\end{bVerbatim}
\item  \begin{bVerbatim}
  chroot /mnt/disk /tmp/bacula-fd -c /tmp/bacula-fd.conf
\end{bVerbatim}
   to start bacula-fd
\item Now you can go to bacula-dir and restore the job with the entire
   contents of the broken server.
\item You must create \bdirectoryname{/proc}
\end{benumerate}

\label{blb:solaris}
\section{Solaris Bare Metal Recovery}
\index[general]{Solaris Bare Metal Recovery}
\index[general]{Recovery!Solaris Bare Metal}

The same basic techniques described above apply to Solaris:

\begin{bitemize}
\item the same restrictions as those given for Linux apply
\item you will need to create a Rescue disk
   \end{bitemize}

However, during the recovery phase, the boot and disk preparation procedures
are different:

\begin{bitemize}
\item there is no need to create an emergency boot disk  since it is an
   integrated part of the Solaris boot.
\item you must partition and format your hard disk by hand  following manual
   procedures as described in W. Curtis Preston's  book ``Unix Backup \&
   Recovery''
\end{bitemize}

Once the disk is partitioned, formatted and mounted, you can continue with
bringing up the network and reloading \bacula{}.

\section{Preparing Solaris Before a Disaster}
\index[general]{Preparing Solaris Before a Disaster}
\index[general]{Disaster!Preparing Solaris Before a}

As mentioned above, before a disaster strikes, you should prepare the
information needed in the case of problems. To do so, in the \textbf{\bdirectoryname{rescue/solaris}} subdirectory enter:

\begin{bVerbatim}
su
./getdiskinfo
./make_rescue_disk
\end{bVerbatim}

The \btool{getdiskinfo} script will, as in the case of Linux described above,
create a subdirectory \textbf{\bdirectoryname{diskinfo}} containing the output from several system
utilities. In addition, it will contain the output from the \btool{SysAudit}
program as described in Curtis Preston's book. This file \bfilename{diskinfo/sysaudit.bsi}
will contain the disk partitioning information that
will allow you to manually follow the procedures in the ``Unix Backup \& Recovery''
book to repartition and format your hard disk. In addition, the
\btool{getdiskinfo} script will create a \btool{start\_network} script.

Once you have your disks repartitioned and formatted, do the following:

\begin{bitemize}
\item Start Your Network with the \btool{start\_network} script
\item Restore the \bacula{} File daemon as documented above
\item Perform a \bacula{} restore of all your files using the same  commands as
   described above for Linux
\item Re-install your boot loader using the instructions outlined  in the
   ``Unix Backup \& Recovery'' book  using installboot
\end{bitemize}

