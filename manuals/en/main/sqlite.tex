\chapter{Installing and Configuring \sqlite{}}
\label{blb:SqlLiteChapter}
\index[general]{Installing and Configuring \sqlite{} }
\index[general]{\sqlite{}!Installing and Configuring }

Note: \sqlite{} implementation remains in the source code, but \sqlite{}
is no longer supported by the Bacula project.  We strongly recommend that you
use either MySQL or PostgreSQL. The code will remain in the source as
long as the community contributes fixes for each version.

Please note that \sqlite{} is not network enabled,
which means that it must be linked into the Director rather than accessed
by the network as MySQL and PostgreSQL are. This has two consequences:
\begin{benumerate}
\item \sqlite{} cannot be used in the \textbf{BWeb} web GUI package.
\item If you use \sqlite{}, and your Storage daemon is not on the same
machine as your Director, you will need to transfer your database
to the Storage daemon's machine before you can use any of the SD tools
such as \btool{bscan}, \ldots{}
\end{benumerate}

\section{Installing and Configuring \sqlite{} -- Phase I}
\index[general]{Phase I!Installing and Configuring \sqlite{} -- }
\index[general]{Installing and Configuring \sqlite{} -- Phase I }

If you use the \btool{./configure {-}{-}with-sqlite} statement for configuring \textbf{\bacula{}},
you will need \sqlite{} version 2.8.16 or later installed. Our standard
location (for the moment) for \sqlite{} is in the dependency package \bdirectoryname{depkgs/sqlite-2.8.16}. Please note that the version will be updated as new
versions are available and tested.

Installing and Configuring is quite easy.

\begin{benumerate}
\item Download the \bacula{} dependency packages
\item Detar it with something like:

\begin{bVerbatim}
tar xvfz depkgs.tar.gz
\end{bVerbatim}

Note, the above command requires \acs{GNU} \tar{}. If you do not  have \acs{GNU} \tar{}, a
command such as:

\begin{bVerbatim}
zcat depkgs.tar.gz | tar xvf -
\end{bVerbatim}

   will probably accomplish the same thing.

   \item and build it:
\begin{bVerbatim}
cd depkgs
make sqlite
\end{bVerbatim}
\end{benumerate}


Please note that the \btool{./configure} used to build \textbf{\bacula{}} will need to
include \textbf{{-}{-}with-sqlite} or \textbf{{-}{-}with-sqlite3} depending
one which version of \sqlite{} you are using. You should not use the \textbf{{-}{-}enable-batch-insert}
configuration parameter for \bacula{} if you
are using \sqlite{} version 2 as it is probably not thread safe.  If you
are using \sqlite{} version 3, you may use the \textbf{{-}{-}enable-batch-insert}
configuration option with \bacula{}, but when building \sqlite{}3 you \textbf{must}
configure it with \textbf{{-}{-}enable-threadsafe} and
\textbf{{-}{-}enable-cross-thread-connections}.

By default, \sqlite{}3 is now run with \textbf{PRAGMA synchronous=OFF} this
increases the speed by more than 30 times, but it also increases the
possibility of a corrupted database if your server crashes (power failure
or kernel bug).  If you want more security, you can change the PRAGMA
that is used in the file \bfilename{src/version.h}.


At this point, you should return to completing the installation of \textbf{\bacula{}}.


\section{Installing and Configuring \sqlite{} -- Phase II}
\label{blb:sqlite_phase2}
\index[general]{Phase II!Installing and Configuring \sqlite{} -- }
\index[general]{Installing and Configuring \sqlite{} -- Phase II }

This phase is done \textbf{after} you have run the \btool{./configure} command to
configure \textbf{\bacula{}}.

\textbf{\bacula{}} will install scripts for manipulating the database (create,
delete, make tables etc) into the main installation directory. These files
will be of the form \bfilename{*\_bacula\_*} (e.g. \bfilename{create\_bacula\_database}).
These files are also available in the \bdirectoryname{\bbracket{bacula-src}/src/cats} directory after
running \btool{./configure}. If you inspect \bfilename{create\_bacula\_database}, you will see
that it calls \bfilename{create\_sqlite\_database}. The \bfilename{*\_bacula\_*}files are provided
for convenience. It doesn't matter what database you have chosen;
\bfilename{create\_bacula\_database} will always create your database.

At this point, you can create the \sqlite{} database and tables:

\begin{benumerate}
\item cd \bbracket{install-directory}

   This directory contains the \bacula{} catalog  interface routines.

\item \btool{./make\_sqlite\_tables}

  This script creates the \sqlite{} database as well as the  tables used by \textbf{\bacula{}}.
  This script will be  automatically setup by the \btool{./configure}
  program  to create a database named \textbf{bacula.db} in \textbf{\bacula{}'s}  working
  directory.
\end{benumerate}

\section{Linking \bacula{} with \sqlite{}}
\index[general]{\sqlite{}!Linking \bacula{} with }
\index[general]{Linking \bacula{} with \sqlite{} }

If you have followed the above steps, this will all happen automatically and
the \sqlite{} libraries will be linked into \textbf{\bacula{}}.

\section{Testing \sqlite{}}
\index[general]{\sqlite{}!Testing }
\index[general]{Testing \sqlite{} }

We have much less ``production'' experience using \sqlite{} than using \mysql{}.
\sqlite{} has performed flawlessly for us in all our testing.  However,
several users have reported corrupted databases while using \sqlite{}.  For
that reason, we do not recommend it for production use.

If \bacula{} crashes with the following type of error when it is started:
\begin{bVerbatim}
Using default Catalog name=MyCatalog DB=bacula
Could not open database "bacula".
sqlite.c:151 Unable to open Database=/var/lib/bacula/bacula.db.
ERR=malformed database schema - unable to open a temporary database file
for storing temporary tables
\end{bVerbatim}

this is most likely caused by the fact that some versions of
\sqlite{} attempt to create a temporary file in the current directory.
If that fails, because \bacula{} does not have write permission on
the current directory, then you may get this errr.  The solution is
to start \bacula{} in a current directory where it has write permission.


\section{Re-initializing the Catalog Database}
\index[general]{Database!Re-initializing the Catalog }
\index[general]{Re-initializing the Catalog Database }

After you have done some initial testing with \textbf{\bacula{}}, you will probably
want to re-initialize the catalog database and throw away all the test Jobs
that you ran. To do so, you can do the following:

\begin{bVerbatim}
cd <install-directory>
./drop_sqlite_tables
./make_sqlite_tables
\end{bVerbatim}

Please note that all information in the database will be lost and you will be
starting from scratch. If you have written on any Volumes, you must write an
end of file mark on the volume so that \bacula{} can reuse it. Do so with:

Stop \bacula{} or unmount the drive
\begin{bVerbatim}
mt -f /dev/nst0 rewind
mt -f /dev/nst0 weof
\end{bVerbatim}

Where you should replace \bfilename{/dev/nst0} with the appropriate tape drive
device name for your machine.
