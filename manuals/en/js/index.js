var Docs = {
	docs_deps: null,
	docs_types: [ 'main', 'console', 'operator' ],
	current_doc_type: null,
	current_key: null,
	patterns: {
		version: /^\d+\.\d+$/,
		key: /^[\w\d\:\.\-]{1,127}$/
	},
	urls: {
		base: document.location.href.replace(/\/index.html.*$/, ''),
		docs_content: 'html',
		docs_def: 'html/main/docs.json'
	},
	init: function() {
		// version is currently not used (only validation for version works)
		var version = this.get_url_param('version');
		if (this.validate_product_version(version) == false) {
			this.go_to_default_page();
		}
		var doc_type = this.get_url_param('manual') || 'main';
		if (this.validate_doc_type(doc_type) == false) {
			this.go_to_default_page();
		}
		var key = this.get_url_param('key');
		if (this.validate_doc_key(key) == false) {
			this.go_to_default_page();
		}
		this.current_doc_type = doc_type;
		this.current_key = key;
		this.load_docs();
	},
	validate_product_version: function(version) {
		return this.patterns.version.test(version);
	},
	validate_doc_key: function(key) {
		return this.patterns.key.test(key);
	},
	validate_doc_type: function(doc_type) {
		return this.docs_types.indexOf(doc_type) !== -1;
	},
	load_docs: function() {
		var url = this.urls.base + '/' + this.urls.docs_def;
		this.send_request(url);
	},
	set_doc_content: function(value) {
		this.docs_deps = value;	
	},
	get_doc_content: function() {
		return (this.docs_deps.hasOwnProperty(this.current_key)) ? this.docs_deps[this.current_key] : "";
	},
	get_doc_by_params: function(doc_type, key) {
		var doc_url = [ this.urls.base, this.urls.docs_content, this.current_doc_type, this.get_doc_content() ].join('/');
		doc_url += '#' + this.current_key;
		window.location.replace(doc_url);
	},
	go_to_default_page: function() {
		var default_url = [ this.urls.base, this.urls.docs_content, 'main', 'index.html' ].join('/');
		document.location.replace(default_url);
	},
	get_xhr: function() {
		var xhr;
		if (window.XMLHttpRequest) {
			xhr = new XMLHttpRequest();
		} else {
			xhr = new ActiveXObject("Microsoft.XMLHTTP");
 		}
		return xhr;
	},
	send_request: function(url) {
		var xhr = this.get_xhr();
		var result;
		xhr.open('GET', url, true);
		xhr.onreadystatechange = function () {
			if (xhr.readyState == 4 && xhr.status == 200) {
				var result = eval('(' + xhr.responseText + ')');
				this.set_doc_content(result);
				this.get_doc_by_params();
			}
		}.bind(this);
		xhr.send();
	},
	get_url_param: function(name) {
		name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
		var regex = new RegExp("[\\?&;]" + name + "=([^&;#]*)"),
		results = regex.exec(location.search);
		var ret = (results != null) ? decodeURIComponent(results[1].replace(/\+/g, " ")) : "";
		return ret.trim();
	}
}

Docs.init();
