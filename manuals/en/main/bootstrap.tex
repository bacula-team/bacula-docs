\chapter{The Bootstrap File}
\label{blb:BootstrapChapter}
\index[general]{File!Bootstrap }
\index[general]{Bootstrap File }

The information in this chapter is provided so that you may either create your
own bootstrap files, or so that you can edit a bootstrap file produced by
\textbf{\bacula{}}. However, normally the bootstrap file will be automatically created
for you during the 
\bxlink{\bacula{} Console}{blb:ConsoleChapter}{console}{chapter} of the \bconsoleman{}, or
by using a 
\bilink{Write Bootstrap}{blb:writebootstrap} record in your Backup
Jobs, and thus you will never need to know the details of this file. 

The \textbf{bootstrap} file contains \acs{ASCII} information that permits precise
specification of what files should be restored, what volume they are on,
and where they are on the volume. It is a relatively compact
form of specifying the information, is human readable, and can be edited with
any text editor.

\section{Bootstrap File Format}
\index[general]{Format!Bootstrap}
\index[general]{Bootstrap File Format }

The general format of a \textbf{bootstrap} file is:

\textbf{\bbracket{keyword}= \bbracket{value}}

Where each \textbf{keyword} and the \textbf{value} specify which files to restore.
More precisely the \textbf{keyword} and their \textbf{values} serve to limit which
files will be restored and thus act as a filter. The absence of a keyword
means that all records will be accepted.

Blank lines and lines beginning with a pound sign (\#) in the bootstrap file
are ignored.

There are keywords which permit filtering by Volume, Client, Job, FileIndex,
Session Id, Session Time, \ldots{}

The more keywords that are specified, the more selective the specification of
which files to restore will be. In fact, each keyword is \textbf{AND}ed with
other keywords that may be present.

For example,

\begin{bVerbatim}
Volume = Test-001
VolSessionId = 1
VolSessionTime = 108927638
\end{bVerbatim}

directs the Storage daemon (or the \btool{bextract} program) to restore only
those files on Volume Test-001 \textbf{AND} having VolumeSessionId equal to one
\textbf{AND} having VolumeSession time equal to 108927638.

The full set of permitted keywords presented in the order in which they are
matched against the Volume records are:

\begin{bdescription}

\item [Volume]
   \index[general]{Volume }
   The value field specifies what Volume the following commands apply to.
   Each Volume specification becomes the current Volume, to which all the
   following commands apply until a new current Volume (if any) is
   specified.  If the Volume name contains spaces, it should be enclosed in
   quotes. At lease one Volume specification is required.

\item [Count]
   \index[general]{Count}
   The value is the total number of files that  will be restored for this Volume.
   This allows the Storage  daemon to know when to stop reading the Volume.
   This value is optional.

\item [VolFile]
   \index[general]{VolFile}
   The value is a file number, a list of file numbers, or a range of file
   numbers to match on the current Volume.  The file number represents the
   physical file on the Volume where the data is stored.  For a tape
   volume, this record is used to position to the correct starting file,
   and once the tape is past the last specified file, reading will stop.

\item [VolBlock]
   \index[general]{VolBlock}
   The value is a block number, a list of block numbers, or a range of
   block numbers to match on the current Volume.  The block number
   represents the physical block within the file on the Volume where the
   data is stored.


\item [VolSessionTime]
   \index[general]{VolSessionTime }
   The value specifies a Volume Session Time to  be matched from the current
   volume.

\item [VolSessionId]
   \index[general]{VolSessionId }
   The value specifies a VolSessionId, a list of volume session ids, or a
   range of volume session ids to be matched from the current Volume.  Each
   VolSessionId and VolSessionTime pair corresponds to a unique Job that is
   backed up on the Volume.

\item [JobId]
   \index[general]{JobId }
   The value specifies a JobId, list of JobIds, or range of JobIds  to be
   selected from the current Volume. Note, the JobId may not be  unique if you
   have multiple Directors, or if you have reinitialized your  database. The
   JobId filter works only if you do not run  multiple simultaneous jobs.
   This value is optional and not used by \bacula{} to restore files.

\item [Job]
   \index[general]{Job }
   The value specifies a Job name or list of Job names to  be matched on the
   current Volume. The Job corresponds to a unique  VolSessionId and
   VolSessionTime pair. However, the Job is perhaps a  bit more readable by
   humans. Standard regular expressions (wildcards)  may be used to match Job
   names. The Job filter works only if you do  not run multiple simultaneous
   jobs.
   This value is optional and not used by \bacula{} to restore files.

\item [Client]
   \index[general]{Client }
   The value specifies a Client name or list of Clients to  will be matched on
   the current Volume. Standard regular expressions  (wildcards) may be used to
   match Client names. The Client filter works  only if you do not run multiple
   simultaneous jobs.
   This value is optional and not used by \bacula{} to restore files.

\item [FileIndex]
   \index[general]{FileIndex}
   The value specifies a FileIndex, list of FileIndexes,  or range of FileIndexes
   to be selected from the current Volume.  Each file (data) stored on a Volume
   within a Session has a unique  FileIndex. For each Session, the first file
   written is assigned  FileIndex equal to one and incremented for each file
   backed up.

   This for a given Volume, the triple VolSessionId, VolSessionTime,  and
   FileIndex uniquely identifies a file stored on the Volume. Multiple  copies of
   the same file may be stored on the same Volume, but for  each file, the triple
   VolSessionId, VolSessionTime, and FileIndex  will be unique. This triple is
   stored in the Catalog database for  each file.

   To restore a particular file, this value (or a range of FileIndexes) is
   required.

\phantomsection
\item [FileRegex]\label{blb:FileRegex}
   \index[general]{FileRegex}
   The value is a regular expression.  When specified, only matching
   filenames will be restored.

\begin{bVerbatim}
   FileRegex=^/etc/passwd(.old)?
\end{bVerbatim}

\item [Slot]
   \index[general]{Slot }
   The value specifies the autochanger slot. There may  be only a single
   \textbf{Slot} specification for each Volume.

\item [Stream]
   \index[general]{Stream }
   The value specifies a Stream, a list of Streams,  or a range of Streams to be
   selected from the current Volume.  Unless you really know what you are doing
   (the internals of  \textbf{\bacula{}}), you should avoid this specification.
   This value is optional and not used by \bacula{} to restore files.

\item [*JobType]
   \index[general]{*JobType }
   Not yet implemented.

\item [*JobLevel]
   \index[general]{*JobLevel }
   Not yet implemented.
\end{bdescription}

The \textbf{Volume} record is a bit special in that it must be the first record.
The other keyword records may appear in any order and any number following a
Volume record.

Multiple Volume records may be specified in the same bootstrap file, but each
one starts a new set of filter criteria for the Volume.

In processing the bootstrap file within the current Volume, each filter
specified by a keyword is \textbf{AND}ed with the next. Thus,

\begin{bVerbatim}
Volume = Test-01
Client = "My machine"
FileIndex = 1
\end{bVerbatim}

will match records on Volume \textbf{Test-01} \textbf{AND} Client records for
\textbf{My machine} \textbf{AND} FileIndex equal to \textbf{one}.

Multiple occurrences of the same record are \textbf{OR}ed together. Thus,

\begin{bVerbatim}
Volume = Test-01
Client = "My machine"
Client = "Backup machine"
FileIndex = 1
\end{bVerbatim}

will match records on Volume \textbf{Test-01} \textbf{AND} (Client records for
\textbf{My machine} \textbf{OR} \textbf{Backup machine}) \textbf{AND} FileIndex equal to
\textbf{one}.

For integer values, you may supply a range or a list, and for all other values
except Volumes, you may specify a list. A list is equivalent to multiple
records of the same keyword. For example,

\begin{bVerbatim}
Volume = Test-01
Client = "My machine", "Backup machine"
FileIndex = 1-20, 35
\end{bVerbatim}

will match records on Volume \textbf{Test-01} \textbf{AND} \textbf{(}Client records for
\textbf{My machine} \textbf{OR} \textbf{Backup machine}\textbf{)} \textbf{AND}
\textbf{(}FileIndex 1 \textbf{OR} 2 \textbf{OR} 3 \ldots{} \textbf{OR} 20 \textbf{OR} 35\textbf{)}.

As previously mentioned above, there may be multiple Volume records in the
same bootstrap file. Each new Volume definition begins a new set of filter
conditions that apply to that Volume and will be \textbf{OR}ed with any other
Volume definitions.

As an example, suppose we query for the current set of tapes to restore all
files on Client \textbf{Rufus} using the \bcommandname{query} command in the console
program:

\begin{bVerbatim}
Using default Catalog name=MySQL DB=bacula
*query
Available queries:
     1: List Job totals:
     2: List where a file is saved:
     3: List where the most recent copies of a file are saved:
     4: List total files/bytes by Job:
     5: List total files/bytes by Volume:
     6: List last 10 Full Backups for a Client:
     7: List Volumes used by selected JobId:
     8: List Volumes to Restore All Files:
Choose a query (1-8): 8
Enter Client Name: Rufus
+-------+------------------+------------+-----------+----------+------------+
| JobId | StartTime        | VolumeName | StartFile | VolSesId | VolSesTime |
+-------+------------------+------------+-----------+----------+------------+
| 154   | 2002-05-30 12:08 | test-02    | 0         | 1        | 1022753312 |
| 202   | 2002-06-15 10:16 | test-02    | 0         | 2        | 1024128917 |
| 203   | 2002-06-15 11:12 | test-02    | 3         | 1        | 1024132350 |
| 204   | 2002-06-18 08:11 | test-02    | 4         | 1        | 1024380678 |
+-------+------------------+------------+-----------+----------+------------+
\end{bVerbatim}

The output shows us that there are four Jobs that must be restored. The first
one is a Full backup, and the following three are all Incremental backups.

The following bootstrap file will restore those files:

\begin{bVerbatim}
Volume=test-02
VolSessionId=1
VolSessionTime=1022753312
Volume=test-02
VolSessionId=2
VolSessionTime=1024128917
Volume=test-02
VolSessionId=1
VolSessionTime=1024132350
Volume=test-02
VolSessionId=1
VolSessionTime=1024380678
\end{bVerbatim}

As a final example, assume that the initial Full save spanned two Volumes. The
output from \bcommandname{query} might look like:

\begin{bVerbatim}
+-------+------------------+------------+-----------+----------+------------+
| JobId | StartTime        | VolumeName | StartFile | VolSesId | VolSesTime |
+-------+------------------+------------+-----------+----------+------------+
| 242   | 2002-06-25 16:50 | File0003   | 0         | 1        | 1025016612 |
| 242   | 2002-06-25 16:50 | File0004   | 0         | 1        | 1025016612 |
| 243   | 2002-06-25 16:52 | File0005   | 0         | 2        | 1025016612 |
| 246   | 2002-06-25 19:19 | File0006   | 0         | 2        | 1025025494 |
+-------+------------------+------------+-----------+----------+------------+
\end{bVerbatim}

and the following bootstrap file would restore those files:

\begin{bVerbatim}
Volume=File0003
VolSessionId=1
VolSessionTime=1025016612
Volume=File0004
VolSessionId=1
VolSessionTime=1025016612
Volume=File0005
VolSessionId=2
VolSessionTime=1025016612
Volume=File0006
VolSessionId=2
VolSessionTime=1025025494
\end{bVerbatim}

\section{Automatic Generation of Bootstrap Files}
\index[general]{Files!Automatic Generation of Bootstrap }
\index[general]{Automatic Generation of Bootstrap Files }

One thing that is probably worth knowing: the bootstrap files that are
generated automatically at the end of the job are not as optimized as those
generated by the \bcommandname{restore} command. This is because during Incremental and
Differential jobs, the records pertaining to the files written for the
Job are appended to the end of the bootstrap file.
As consequence, all the files saved to an Incremental or Differential job will be
restored first by the Full save, then by any Incremental or Differential
saves.

When the bootstrap file is generated for the \bcommandname{restore} command, only one copy
(the most recent) of each file is restored.

So if you have spare cycles on your machine, you could optimize the bootstrap
files by doing the following:

\begin{bVerbatim}
   ./bconsole
   restore client=xxx select all
   done
   no
   quit
   Backup bootstrap file.
\end{bVerbatim}

The above will not work if you have multiple FileSets because that will be an
extra prompt. However, the \bcommandname{restore client=xxx select all} builds the
in-memory tree, selecting everything and creates the bootstrap file.

The \textbf{no} answers the \textbf{Do you want to run this (yes/mod/no)} question.

\label{blb:bscanBootstrap}
\section{Bootstrap for bscan}
\index[general]{bscan}
\index[general]{bscan!bootstrap}
\index[general]{bscan bootstrap}
If you have a very large number of Volumes to scan with \btool{bscan},
you may exceed the command line limit (511 characters). I that case,
you can create a simple bootstrap file that consists of only the
volume names.  An example might be:

\begin{bVerbatim}
Volume="Vol001"
Volume="Vol002"
Volume="Vol003"
Volume="Vol004"
Volume="Vol005"
\end{bVerbatim}


\section{A Final Bootstrap Example}
\index[general]{Bootstrap Example}
\index[general]{Example!Bootstrap}

If you want to extract or copy a single Job, you can do it by selecting by
JobId (code not tested) or better yet, if you know the VolSessionTime and the
VolSessionId (printed on Job report and in Catalog), specifying this is by far
the best. Using the VolSessionTime and VolSessionId is the way \bacula{} does
restores. A bsr file might look like the following:

\begin{bVerbatim}
Volume="Vol001"
VolSessionId=10
VolSessionTime=1080847820
\end{bVerbatim}

If you know how many files are backed up (on the job report), you can
enormously speed up the selection by adding (let's assume there are 157
files):

\begin{bVerbatim}
FileIndex=1-157
Count=157
\end{bVerbatim}

Finally, if you know the File number where the Job starts, you can also cause
\btool{bcopy} to forward space to the right file without reading every record:

\begin{bVerbatim}
VolFile=20
\end{bVerbatim}

There is nothing magic or complicated about a BSR file. Parsing it and
properly applying it within \bacula{} *is* magic, but you don't need to worry
about that.

If you want to see a \textbf{real} bsr file, simply fire up the \bcommandname{restore} command
in the console program, select something, then answer no when it prompts to
run the job. Then look at the file \bfilename{restore.bsr} in your working
directory.
