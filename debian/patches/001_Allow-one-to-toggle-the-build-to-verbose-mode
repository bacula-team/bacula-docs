Description: Allow one to toggle the build to verbose mode
  Setting the environment variable BUILD_VERBOSE to 1 makes the build verbose.
Author: Carsten Leonhardt <leo@debian.org>
Forwarded: https://sourceforge.net/p/bacula/mailman/message/58847316/
Last-Update: 2024-11-30

diff --git a/Makefile b/Makefile
index 09b99528..df4d402f 100644
--- a/Makefile
+++ b/Makefile
@@ -2,7 +2,12 @@
 #-------------------------------------------------------------------------
 SHELL = /bin/sh
 
-AT=@
+# if BUILD_VERBOSE is set to 1, output everything to the console
+ifeq ($(BUILD_VERBOSE),1)
+	AT=
+else
+	AT=@
+endif
 
 #
 # bacula.env file is a local file to define specific values if any
diff --git a/manuals/Makefile b/manuals/Makefile
index 212a9487..48f8beca 100644
--- a/manuals/Makefile
+++ b/manuals/Makefile
@@ -10,7 +10,13 @@ else
   $(error No environment file "$(MANUALENVFILE)" found. You can copy "$(MANUALENVFILE).dist" to "$(MANUALENVFILE)" and adjust some definitions.)
 endif
 
-AT=@
+# if BUILD_VERBOSE is set to 1, output everything to the console
+ifeq ($(BUILD_VERBOSE),1)
+	AT=
+else
+	AT=@
+endif
+
 #
 # WARNING !!! The following works *ONLY* when using MAKE with $(MAKE) -C subdir invocation.
 LICENCE_FILES=$(wildcard $(LICENCE_DIR)/*.tex)
@@ -24,8 +30,14 @@ WEB_FILES_TO_LINK=$(TRANSLATEIMGS) $(LATEX2HTMLINIT)
 PDFLATEX=$(COMPILER)
 PDFLATEXSILENT=batchmode
 PDFLATEXVERBOSE=errorstopmode
-PDFLATEXOPTIONS=-halt-on-error -interaction=$(PDFLATEXSILENT) # $(PDFLATEXVERBOSE) # 
-PDFOUTPUT="> $(DOC).out 2>&1"
+ifeq ($(BUILD_VERBOSE),1)
+	PDFLATEXOPTIONS=-halt-on-error -interaction=$(PDFLATEXVERBOSE)
+	PDFOUTPUT=""
+else
+	PDFLATEXOPTIONS=-halt-on-error -interaction=$(PDFLATEXSILENT)
+	PDFOUTPUT="> $(DOC).out 2>&1"
+endif
+
 #
 # Makefile used to produce HTML documentation
 EXTSEDFILES=html-external-references.sed
-- 
2.39.5

