Description: Backport fixes from git
Author: Carsten Leonhardt <leo@debian.org>
Last-Update: 2024-11-30
Forwarded: not-needed

--- a/latex/license.tex
+++ b/latex/license.tex
@@ -15,7 +15,7 @@
 \section{CC-BY-SA}
 \index[general]{CC-BY-SA}
 
-The \ilink{Creative Commons Attribution-ShareAlike 4.0 International
+The \bilink{Creative Commons Attribution-ShareAlike 4.0 International
 License (CC-BY-SA)}{CCbySAChapter} is used for this manual,
 which is a free and open license. Though there are certain
 restrictions that come with this license you may in general freely
@@ -30,7 +30,7 @@
 \index[general]{GPL }
 
 The vast bulk of the source code is released under the 
-\ilink{Affero GNU General Public License version 3.}{GplChapter}.
+\bilink{Affero GNU General Public License version 3.}{GplChapter}.
 
 Most of this code is copyrighted: Copyright \copyright 2000-2016,
 Kern Sibbald.
@@ -42,7 +42,7 @@
 \index[general]{LGPL }
 
 Some of the Bacula library source code is released under the 
-\ilink{GNU Lesser General Public License.}{LesserChapter} This
+\bilink{GNU Lesser General Public License.}{LesserChapter} This
 permits third parties to use these parts of our code in their proprietary
 programs to interface to Bacula. 
 
--- a/manuals/en/main/antivirus.tex
+++ b/manuals/en/main/antivirus.tex
@@ -142,7 +142,7 @@
   TCPSocket 3310
 \end{bVerbatim}
 
-Optionally, you can also restrain the TCP binding (by default the ClamAV daemon binds to INADDR_ANY):
+Optionally, you can also restrain the TCP binding (by default the ClamAV daemon binds to INADDR\_ANY):
 
 \begin{bVerbatim}
     TCPAddr 127.0.0.1
@@ -161,7 +161,7 @@
     sudo systemctl start clamd@scan
 \end{bVerbatim}
 
-\subsubsection{Bacula Enterprise Antivirus Plugin Installation}
+\subsubsection{Bacula Antivirus Plugin Installation}
 
 Installation of the Bacula Antivirus Plugin can be installed from community binary repository, or via the ./configure command line.
 
@@ -404,7 +404,7 @@
 \end{bVerbatim}
 
 The FileSet of the Backup Job must use the \texttt{Signature = MD5} or
-\texttt{Signature = SHA256} option to use the \textt{Check Malware} directive.
+\texttt{Signature = SHA256} option to use the \texttt{Check Malware} directive.
 
 By default, the Malware database is fetched from \texttt{abuse.ch}. If needed, it
 can be adapted with the Director \texttt{MalwareDatabaseCommand} directive.
--- a/manuals/en/main/jobstatus.tex
+++ b/manuals/en/main/jobstatus.tex
@@ -192,7 +192,7 @@
   Network socket configuration error. Check the TCP/IP configuration.
 
 \item [DW0063]
-  Unable to set socket option such as SO_KEEPALIVE or SO_KEEPIDLE on the socket.
+  Unable to set socket option such as SO\_KEEPALIVE or SO\_KEEPIDLE on the socket.
 
 \item [DE0064]
   TLS Error detected. The connect/accept operation cannot be done, or the password is
@@ -221,8 +221,10 @@
 
 \item [DE0072]
   TLS shutdown error.
+\end{itemize}
 
 \subsection{Storage Daemon Codes}
+\begin{itemize}
 
 \item [SS0001]
   Authentication failed.
@@ -294,7 +296,7 @@
   Network socket configuration error. Check the TCP/IP configuration.
 
 \item [SW0063]
-  Unable to set socket option such as SO_KEEPALIVE or SO_KEEPIDLE on the socket.
+  Unable to set socket option such as SO\_KEEPALIVE or SO\_KEEPIDLE on the socket.
 
 \item [SE0064]
   TLS Error detected. The connect/accept operation cannot be done, or the password is
@@ -387,9 +389,11 @@
 
 \item [SE0208]
   Volume data error after a short block. block discarded.
+\end{itemize}
 
   
 \subsection{File Daemon/Client Error Codes}
+\begin{itemize}
 
 \item [FC0001]
   Director disconnection event.
@@ -454,7 +458,7 @@
   Network socket configuration error. Check the TCP/IP configuration.
 
 \item [FW0063]
-  Unable to set socket option such as SO_KEEPALIVE or SO_KEEPIDLE on the socket.
+  Unable to set socket option such as SO\_KEEPALIVE or SO\_KEEPIDLE on the socket.
 
 \item [FE0064]
   TLS Error detected. The connect/accept operation cannot be done, or the password is
@@ -487,9 +491,12 @@
 
 \item [FE0072]
   TLS shutdown error.
+\end{itemize}
 
 \subsection{Console Error Codes}
+\begin{itemize}
 
 \item [CE0029]
   The Console cannot connect to a remote resource. Check that the
   remote service is started and can be reached via the network.
+\end{itemize}
--- a/manuals/en/main/newfeatures.tex
+++ b/manuals/en/main/newfeatures.tex
@@ -1,4 +1,4 @@
-\section{New Features in 15.0.0}
+\chapter{New Features in 15.0.0}
 
 \subsection{Security Enhancements}
 
@@ -50,7 +50,7 @@
      Termination:            Backup OK -- with warnings
 \end{bVerbatim}
 
- The list of the Malware detected in a given Job can be displayed with the \textt{list files type=malware}
+ The list of the Malware detected in a given Job can be displayed with the \texttt{list files type=malware}
  command.
 
 \begin{bVerbatim}
@@ -95,8 +95,19 @@
 Console {
    Name = "totpconsole"
    Password = "xxx"
-
+   
    Authentication Plugin = "totp"
+
+  CommandACL = *all* 
+  JobACL = *all* 
+  ClientAcl = *all* 
+  PoolACL = *all*
+  DirectoryACL = *all*
+  WhereACL = *all*
+  FileSetACL = *all*
+  StorageACL = *all*
+  CatalogACL = *all*
+  ScheduleACL = *all*
 }
 \end{bVerbatim}
 
@@ -468,7 +479,7 @@
 
 \subsubsection{Catalog Enhancements}
 
-\subsubsubsection{FileSet Content Overview}
+\subsubsection*{FileSet Content Overview}
 
 Now, the Catalog stores an overview of the FileSet definition in the **Content**
 field. If the FileSet handles files and directories, the Content field will be
@@ -487,13 +498,13 @@
 
 \end{bVerbatim}
 
-\subsubsubsection{New Job Attributes}
+\subsubsection*{New Job Attributes}
 
-New SQL attributes have been added to the Job table such as **isVirtualFull**,
-**Encrypted**, **LastReadStorageId**, **WriteStorageId**, **Rate**, **CompressRatio**,
-**StatusInfo**, and so on.
+New SQL attributes have been added to the Job table such as isVirtualFull,
+Encrypted, LastReadStorageId, WriteStorageId, Rate, CompressRatio,
+StatusInfo, and so on.
 
-\subsubsubsection{New Media Attributes}
+\subsubsection*{New Media Attributes}
 
 New SQL attributes have been added to the Media table such as **UseProtect**, **Protected**,
 **VolEncrypted**
@@ -747,7 +758,7 @@
    point and \texttt{<filter>} is a standard LDAP search object filter which support dynamic string
    substitution: \texttt{\%u} will be replaced by credential's username and \texttt{\%p} by credential's
    password, i.e. \texttt{query=dc=bacula,dc=com/(cn=\%u)}.
- \item  [starttls] - Instruct the BPAM LDAP Plugin to use the **StartTLS** extension if the LDAP Directory service
+ \item  [starttls] - Instruct the BPAM LDAP Plugin to use the \texttt{StartTLS} extension if the LDAP Directory service
    supports it and fallback to no TLS if this extension is not available.
 \item [starttlsforce] - Does the same as the `starttls` setting does but reports error on fallback.
 \end{itemize}
@@ -7944,7 +7955,7 @@
  db_address=
  db_port=0
  db_socket=
-\end{bVerbatim} %$
+\end{bVerbatim} 
 
 You can now specify the database connection port in the command line.
 
--- a/manuals/en/problems/rpm-faq.tex
+++ b/manuals/en/problems/rpm-faq.tex
@@ -176,7 +176,7 @@
 \end{bVerbatim}
 
 Further, if you are using File storage volumes rather than tapes those
-files will also need to have ownership set to user \textt{bacula} and group \textt{bacula}.
+files will also need to have ownership set to user \texttt{bacula} and group \texttt{bacula}.
 
 \item \label{blb:faq9}
    \textbf{There are a lot of rpm packages.  Which packages do I need for
@@ -314,7 +314,7 @@
 \begin{bitemize}
 \item [Wrong /var/bacula Permissions]
   By default, the Director and Storage daemon do not run with
-  root permission. If the \bdirectoryname{/var/bacula} is owned by \textt{root}, then it
+  root permission. If the \bdirectoryname{/var/bacula} is owned by \texttt{root}, then it
   is possible that the Director and the Storage daemon will not
   be able to access this directory, which is used as the Working
   Directory. To fix this, the easiest thing to do is:
--- a/tools/translatedoc.pl
+++ b/tools/translatedoc.pl
@@ -34,7 +34,7 @@
 use File::Basename ;
 use Data::Dumper ;
 use open qw/:std :utf8/;
-$edition = 'Bacula Enterprise Edition' ;
+$edition = 'Bacula Edition' ;
 sub usage {
     print "translatedoc.pl -i | --input html-source-file
  [ -o | --output html-destination-file ]
