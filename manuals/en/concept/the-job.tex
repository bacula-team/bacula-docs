\chapter{The Job}\label{chap:jobs}
\section{Definition}
The Job is the basic unit in Bacula and is run by the Director. It ties together the following items:
\begin{bitemize}
\item \textbf{Who}: The \emph{Client}, the machine to backup
\item \textbf{What}: The \emph{File Set}, which files to backup and not to backup
\item \textbf{Where}:
  \begin{bitemize}
  \item \emph{Storage}: what physical device to backup to
  \item \emph{Pool}: which set of Volumes to use
  \item \emph{Catalog}: where to keep track of files
  \end{bitemize}
\item \textbf{When}: \emph{Schedule}: when to run the Job
\end{bitemize}

\section{Types}
There are several types of jobs in Bacula:
%I think we do not need the character codes from the catalog - ARL
\begin{bitemize}
\item Backup
\item Restore
\item Admin
\item Verify
\item Copy
\item Migration
\item Archive
%\item Scan
\item Console connection
\item Internal system job
\end{bitemize}
%\begin{bitemize}
%\item Backup (B)
%\item Restore (R)
%\item Admin (D)
%\item Verify (V)
%\item Copy (C/c)
%\item Migration (M/g)
%\item Archive (A)
%\item Scan (S)
%\item Console btool (U)
%\item Internal system ”job” (I)
%\end{bitemize}

\section{Backup Job Description}
As shown in figure~\vref{fig:back-up-job-diagram}, when running a backup job:
\begin{bitemize}
\item the Bacula Director will connect to the File Daemon and Storage Daemon, 
\item the Director will then send to the File Daemon the necessary information to actually realize the backup job, like the Level, the File Set, the Storage Daemon to contact, etc.
\item then the File Daemon will contact the Storage Daemon, identify the Data to be sent and send them as well as the related meta data (file attributes) to the Storage Daemon
\item the Storage Daemon will put them into one or more Bacula Volumes, according to the Pool Resource chosen for the Job
\item the Storage Daemon will send metadata back to the Director.
\end{bitemize}
\bimageH[0.75\linewidth]{bee8-back-up-job-diagram}{The backup job diagram}{fig:back-up-job-diagram}
%% \begin{figure}
%%   \begin{center}
%%     \includegraphics[width=0.75\linewidth]{bee8-back-up-job-diagram}
%%     \caption{The backup job diagram}\label{fig:back-up-job-diagram}
%%   \end{center}
%% \end{figure}
The moment Bacula will run a Backup Job depends on how this job is started:
\begin{bitemize}
\item manually through a \emph{Console}, e.\,g with \btool{bconsole}: \bcommand{run job=name-of-the-job}
\item automatically by a defined and referenced Schedule
\item automatically by an external script or command like\\
  \bcommand{bconsole -c bconsole.conf <{}< EOF\\run job=name-of-the-job \ldots\\EOF}
\end{bitemize}

\subsection{Admin Jobs}
An Admin Job is a job which does not backup or move any data, but which can be used to execute scripts through the \emph{Run Script} resource and \emph{Console} or \emph{Command} directives.
The following definition will launch the script ``/opt/bacula/scripts/my-script.pl'' located on the Director machine according to the Schedule ``AdminSchedule'' 
\begin{bVerbatim}
  Job {
    Type = Admin
    Run Script {
      Runs When = Before
      Runs on Client = No
      Command = "/opt/bacula/scripts/my-script.pl"
    }
    Schedule = AdminSchedule
    ...
  }
\end{bVerbatim}

\subsection{Migration, Copy and Virtual Full Jobs}
Bacula provides three kinds of Jobs that do not actually backup data, but move data around. They are doing very similar things:
\begin{bitemize}
\item Migration Jobs allow the migration of job data from one volume to another one; This is usually interesting when implementing a Disk-to-Disk-to-Tape strategy, or, more general, any sort of multi tiered backup storage system.
\item Copy Jobs copy a Job's data to another volume. This is usually used when the requirements include at least two places where backup data is being kept.
\item Virtual Full Job which provide a way to consolidate several jobs into one, without requesting any data from the client, based only on existing backup data.
\end{bitemize}
All these kind of Jobs rely on a \emph{Next Pool} directive.

\subsubsection{Virtual Full Consideration}
Virtual Full backups are a way to do backups in an ``Incremental forever'' style while continuing to provide Full backups at the same time.

When backing up data at incremental or differential levels, Bacula (by default) does not do anything regarding removed or moved files or directories. Which implies that the result of a restoration could be different than the latest state of the machine. For that reason, we advise using ``Accurate'' mode which is enabled by the directive of the same name. When set to ``yes'' in a Job, Bacula will record removed missing files or directories, and depending on additional configuration, Bacula will also consider more criteria than just time stamps to determine if a file needs to be backed up. In this case, Bacula will restore the machine to the exact same state (from a backup content point of view) that it was in during the backups.

Administrators will understand that the ``Accurate'' mode takes additional resources and time when running backups.

To improve performance, you may use the ``Accurate'' directive when using Virtual Full backups only for the last incremental before the Virtual Full itself. Activating this directive for every incremental backup would be even better but could increase the backup time. 
\begin{bVerbatim}
#
# A usual job definition
Job {
  Name = "j-bacula"
  JobDefs = "DefaultJob"
  FileSet = "BaculaFileSet"
  Client = client-fd
  Schedule = s-Data2Disk
  Max Full interval = 19 days
  Run Script {
      Runs When = after
      Runs on Client = no
      Runs on Failure = yes
      Command = "/opt/bacula/scripts/run_copyjob.pl %l %i %b t-rdx j-copy-full" ;
      # Launching the copy job as soon as the backup is done
      # %l is job level
      # %i is job id
      # %b is job bytes
      # j-copy-full is the job name (see below)
      # The script "run_copyjob.pl" issues a shell command like
      # bconsole -c bconsole.conf << END_OF_DATA
      #          run jobid=%i job=j-copy-full storage=t-rdx yes
      #          quit
      # END_OF_DATA
  }
}
\end{bVerbatim}
And here is the ``Copy'' job to copy the job from Disk to RDX
\begin{bVerbatim}
Job {
    Name = "j-copy-full"
    Type = Copy
    Level = Full
    Client = client-fd
    File Set = "Empty Set"
    Messages = Standard
    Pool = PoolVFull
    Maximum Concurrent Jobs = 1
}
\end{bVerbatim}
And the Schedule used to run the several levels
\begin{bVerbatim}
  Schedule {
         Name = s-Data2Disk
         Run = Level=incremental monday-thursday,saturday at 21:00
         Run = Level=incremental accurate=yes friday at 12:30
         Run = Level=VirtualFull priority=15 friday at 12:35
}
\end{bVerbatim}


\section{Pruning}
Pruning by default occurs at the end of a Job. When doing some tests or if you only have few jobs a day, this could be fine. But as soon as the number of jobs is growing, you would prefer to manage pruning another way to let your Catalog do its best for the backup jobs, not for the database administrative tasks.

In such a situation, you will configure your clients not to activate the pruning algorithm, using the ``Auto Prune'' Directive such as the following:
\begin{bVerbatim}
  Client {
    Name = client-fd
    Address = bacula.example.com
    FDPort = 9102
    Catalog = Catalog
    Password = "do-you-want-a-very-strong-password?"
    File Retention = 15 days
    Job Retention = 50 days
    AutoPrune = no
         # No automatic pruning at the end of a job for this client
  }
\end{bVerbatim}
If you don't do anything more, your Catalog will grow infinitely. To keep it at its best, you should define an ``Admin'' job, like the following:
\begin{bVerbatim}
  Job {
     Name = "admin-manual-pruning"
     Type = Admin
     JobDefs = "DefaultJob"
     RunScript {
        Runs When = Before
        # below command relies on proper PATH!
        Command = "/bin/sh -c \"echo prune expired volume yes\" | bconsole"
        Runs On Client = no
     }
     Schedule = s-Prune
  }
\end{bVerbatim}
As a Bacula volume can contain one or more jobs (or parts of jobs) and a job contains one or more files, the pruning process will have side effects:
\begin{bitemize}
\item when pruning a Volume, all the jobs related to the Volume are pruned
  \item when pruning a Job, all the files related to this jobs are pruned
\end{bitemize}
That is the reason why you should have the following inequality:
\smallskip{}

\begin{center}
  \bhighlight{File Retention $\leqslant{}$ Job Retention $\leqslant{}$ Volume Retention}
\end{center}
%%  R_f \leqslant R_j \leqslant R_v
%%\end{equation}
%% Where:
%% \begin{bitemize}
%% \item $$ R_f $$ is the File retention period
%% \item $R_j$ is the Job retention period
%% \item $R_v$ is the Volume retention period
%% \end{bitemize}
