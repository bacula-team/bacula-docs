\chapter{The Windows Version of \bacula{}}
\label{blb:Win32Chapter}
\index[general]{Windows Version of \bacula{}}

At the current time the File daemon or Client program has been thoroughly tested on Windows and is suitable for a production environment.  A Windows version of the \bacula{} Storage daemon is also included in the installer, but it has not been extensively tested in a production environment.  As a consequence, when we speak of the Windows version of \bacula{} below, we are referring to the File daemon (client) only.

The Windows version of the \bacula{} File daemon has been tested on WinXP,
Windows 2000, Windows Server 2003, Windows Server 2008, Vista, Windows 7, Windows 8, Windows 10, and Windows 2012 systems.  The Windows version of \bacula{} is a native Windows port, but there are very few source code changes to the Unix code, which means that the Windows version is for the most part running code that has long proved stable on Unix systems.  When running, it is perfectly integrated with Windows.  See section \vref{blb:sec:windows-supported-versions} for supported versions.
\smallskip

Once installed \bacula{} normally runs as a system service.  This means that
it is immediately started by the operating system when the system is
booted, and runs in the background even if there is no user logged into the
system.

\section{Windows Supported Versions}\label{blb:sec:windows-supported-versions}
\index[general]{Supported Windows Versions}
\index[general]{Windows!Supported Versions}

It should be noted that as of 2016, the following Windows versions are no longer supported by Microsoft: Windows XP, Windows Vista, Windows 7, Windows Server 2000, and Windows Server 2003.  As a consequence, these systems are no longer officially supported by \bacula{}.  That said, we will make a best effort to support them, but cannot not guarantee that new \baculaenterprise{} software developed after the support expiration date of each Microsoft \acs{OS} will be supported.

\section{Windows Installation}
\label{blb:installation}
\index[general]{Installation}
\index[general]{Windows!Installation}

Normally, you will install the Windows version of \bacula{} from the binaries.
This install is standard Windows .exe that runs an install wizard using the
\acs{NSIS} Free Software installer, so if you have already installed Windows
software, it should be very familiar to you.

If you have a previous version of \bacula{} installed, you should stop the service, uninstall it, and remove the \bacula{} installation directory possibly saving your \bfilename{bacula-fd.conf}, \bfilename{bconsole.conf}, and \bfilename{bat.conf} files for use with the new version you will install.  The \btool{Uninstall} program is normally found in \bfilename{c:\textbackslash{}bacula\textbackslash{}Uninstall.exe}.  We also recommend that you completely remove the directory \bdirectoryname{c:\textbackslash{}bacula} because the current installer uses a different directory structure (see below).

Providing you do not already have \bacula{} installed,
the installer installs the binaries and dlls in
\bdirectoryname{c:\textbackslash{}Program Files\textbackslash{}Bacula\textbackslash{}bin}
and the configuration files
in \bdirectoryname{c:\textbackslash{}Documents and Settings\textbackslash{}All Users\textbackslash{}Application Data\textbackslash{}Bacula}
In addition, the \textbf{Start \textrightarrow{} All Programs \textrightarrow{} Bacula} menu item
will be created during the installation, and on that menu, you
will find items for editing the configuration files, displaying
the document, and starting \btool{bwx-console} or \btool{bconsole}.


Finally, proceed with the installation.

\begin{bitemize}
\item You must be logged in as Administrator to the local machine
to do a correct installation, if not, please do so before continuing.
Some users have attempted to install logged in as a domain administrator
account and experienced permissions problems attempting to run
\bacula{}, so we don't recommend that option.

\item Simply double click on the \bfilename{bacula-enterprise-win64-8.x.x.exe} \acs{NSIS} install
   icon. The  actual name of the icon will vary from one release version to
   another.

\raisebox{-2.5ex}{\includegraphics[height=6ex]{nsis}}  bacula-enterprise-win64-8.x.x.exe

\item Once launched, the installer wizard will ask you if you want  to install
   \bacula{}.

\bimageH{win32-welcome}{Windows Client Setup Wizard}{fig:win32clientsetupwizard}

\item Next you will be asked to select the installation type.

\bimageH{win32-installation-type}{Windows Installation Type}{fig:win32installationtype}

\item If you proceed, you will be asked to select the components to be
   installed. You may install the \bacula{} program (\bacula{} File Service)  and or
   the documentation. Both will be installed in sub-directories  of the install
   location that you choose later. The components  dialog looks like the
   following:

\bimageH{win32-pkg}{Win32 Component Selection Dialog}{fig:win32componentselectiondialog}
\index[general]{Upgrading}

\item If you are installing for the first time, you will  be asked to
   enter some very basic information about your configuration. If
   you are not sure what to enter, or have previously saved configuration
   files, you can put anything you want into the fields, then either
   replace the configuration files later with the ones saved, or edit
   the file.

   If you are upgrading an existing installation, the following will
   not be displayed.


\bimageH{win32-config}{Win32 Configure}{fig:win32configure}

\item While the various files are being loaded, you will see the following
   dialog:

\bimageH{win32-installing}{Windows Install Progress}{fig:win32installing}

\item Finally, the finish dialog will appear:

\bimageH{win32-finish}{Windows Client Setup Completed}{fig:win32setupcompleted}

\end{bitemize}

That should complete the installation process.

\section{Tray Icon}
\index[general]{Tray Icon}
 
Note: this section does not apply to all Windows versions 
later than Windows XP, since in those later versions Microsoft
explicitly prohibits a system service such as \bacula{} from interacting
with the user.

When the \bacula{} File Server is
ready to serve files, an icon \raisebox{-1ex}{\includegraphics{k7-idle}} representing a
cassette (or tape) will appear in the system tray
\raisebox{-2ex}{\includegraphics{tray-icon}}; right click on it and a menu will appear.\\
\bimageN{menu}{Menu on right click}{fig:win32menuonrightclick}\\
The \textbf{Events} item is currently unimplemented, by selecting the
\textbf{Status} item, you can verify whether any jobs are running or not.

When the \bacula{} File Server begins saving files, the color of the holes in the
cassette icon will change from white to green \raisebox{-1ex}{\includegraphics{k7-ok}},
and if there is an error, the holes in the cassette icon will change to red
\raisebox{-1ex}{\includegraphics{k7-error}}.

If you are using remote desktop connections between your Windows boxes, be
warned that that tray icon does not always appear. It will always be visible
when you log into the console, but the remote desktop may not display it.

\section{Post Windows Installation}
\index[general]{Post Windows Installation}
\index[general]{Windows!Post Installation}

After installing \bacula{} and before running it, you should check the contents
of the configuration files to ensure that they correspond to your
installation.  You can get to them by using:
the \textbf{Start \textrightarrow{} All Programs \textrightarrow{} Bacula} menu item.

Finally, but pulling up the Task Manager (\texttt{CTRL-ALT-DEL}), verify that \bacula{}
is running as a process (not an Application) with User Name SYSTEM. If this is
not the case, you probably have not installed \bacula{} while running as
Administrator, and hence it will be unlikely that \bacula{} can access
all the system files.

\section{Uninstalling \bacula{} on Windows}
\index[general]{Windows!Uninstalling \bacula{}}
\index[general]{Uninstalling \bacula{} on Windows}

Once \bacula{} has been installed, it can be uninstalled using the standard
Windows Add/Remove Programs dialog found on the Control panel.

\section{Dealing with Windows Problems}
\label{blb:problems}
\index[general]{Windows!Dealing with Problems}
\index[general]{Dealing with Windows Problems}

Sometimes Windows machines the File daemon may have very slow
backup transfer rates compared to other machines.  To you might
try setting the Maximum Network Buffer Size to 32,768 in both the
File daemon and in the Storage daemon. The default size is larger,
and apparently some Windows ethernet controllers do not deal with
a larger network buffer size.
\smallskip

Many Windows ethernet drivers have a tendency to either run slowly
due to old broken firmware, or because they are running in half-duplex
mode. Please check with the ethernet card manufacturer for the latest
firmware and use whatever techniques are necessary to ensure that the
card is running in duplex.
\smallskip

If you are not using the portable option, and you have \acs{VSS}
(Volume Shadow Copy) enabled in the Director, and you experience
problems with \bacula{} not being able to open files, it is most
likely that you are running an antivirus program that blocks
\bacula{} from doing certain operations. In this case, disable the
antivirus program and try another backup.  If it succeeds, either
get a different (better) antivirus program or use something like
RunClientJobBefore/After to turn off the antivirus program while
the backup is running.
\smallskip

If turning off anti-virus software does not resolve your \acs{VSS}
problems, you might have to turn on \acs{VSS} debugging.  The following
link describes how to do this:
\bref{http://support.microsoft.com/kb/887013/en-us}{support.microsoft.com/kb/887013/en-us}.
\smallskip

In Microsoft Windows Small Business Server 2003 the \acs{VSS} Writer for Exchange
is turned off by default. To turn it on, please see the following link:
\bref{http://support.microsoft.com/default.aspx?scid=kb;EN-US;Q838183}{support.microsoft.com/default.aspx?scid=kb;EN-US;Q838183}
\smallskip

The most likely source of problems is authentication when the Director
attempts to connect to the File daemon that you installed. This can occur if
the names and the passwords defined in the File daemon's configuration file
\bfilename{bacula-fd.conf} file on
the Windows machine do not match with the names and the passwords in the
Director's configuration file \bfilename{bacula-dir.conf} located on your Unix/Linux
server.
\smallskip

More specifically, the password found in the \textbf{Client} resource in the
Director's configuration file must be the same as the password in the
\textbf{Director} resource of the File daemon's configuration file. In addition, the
name of the \textbf{Director} resource in the File daemon's configuration file
must be the same as the name in the \textbf{Director} resource of the Director's
configuration file.
\smallskip

It is a bit hard to explain in words, but if you understand that a Director
normally has multiple Clients and a Client (or File daemon) may permit access
by multiple Directors, you can see that the names and the passwords on both
sides must match for proper authentication.
\smallskip

One user had serious problems with the configuration file until he realized
that the Unix end of line conventions were used and \bacula{} wanted them in
Windows format. This has not been confirmed though, and \bacula{} version 2.0.0
and above should now accept all end of line conventions (Windows,
Unix, Mac).
\smallskip

Running Unix like programs on Windows machines is a bit frustrating because
the Windows command line shell (DOS Window) is rather primitive. As a
consequence, it is not generally possible to see the debug information and
certain error messages that \bacula{} prints. With a bit of work, however, it is
possible. When everything else fails and you want to \textbf{see} what is going
on, try the following:

\begin{bVerbatim}
Start a DOS shell Window.
c:\Program Files\bacula\bacula-fd -t >out
type out
\end{bVerbatim}

The precise path to bacula-fd depends on where it is installed.
The \textbf{-t} option will cause \bacula{} to read the configuration file, print
any error messages and then exit. the \textbf{\gt{}} redirects the output to the
file named \bfilename{out}, which you can list with the \btool{type} command.

If something is going wrong later, or you want to run \textbf{\bacula{}} with a
debug option, you might try starting it as:

\begin{bVerbatim}
c:\Program Files\bacula\bin\bacula-fd -d 100 >out
\end{bVerbatim}

In this case, \bacula{} will run until you explicitly stop it, which will give
you a chance to connect to it from your Unix/Linux server.  When you start
the File daemon in debug mode it can write the output to a trace file \bfilename{bacula.trace}
in the current directory.  To enable this, before running a
job, use the console, and enter:

\begin{bVerbatim}
trace on
\end{bVerbatim}

then run the job, and once you have terminated the File daemon, you will find
the debug output in the \bfilename{bacula.trace} file, which will probably be
located in the same directory as \bfilename{bacula-fd.exe}.

In addition, you should look in the System Applications log on the Control
Panel to find any Windows errors that \bacula{} got during the startup process.
\smallskip

Finally, due to the above problems, when you turn on debugging, and specify
\bhighlight{trace=1} on a \bcommandname{setdebug} command in the Console, \bacula{} will write the debug
information to the file \bfilename{bacula.trace} in the directory from which \bacula{}
is executing.
\smallskip

If you are having problems with ClientRunBeforeJob scripts randomly dying,
it is possible that you have run into an Oracle bug.  See bug number 622 in
the \bref{http://bugs.bacula.org}{bugs.bacula.org} database.  The following information has been
provided by a user on this issue:

\begin{bVerbatim}
The information in this document applies to:
 Oracle HTTP Server - Version: 9.0.4
 Microsoft Windows Server 2003
 Symptoms
 When starting an OC4J instance, the System Clock runs faster, about 7
seconds per minute.

 Cause

 + This is caused by the Sun JVM bug 4500388, which states that "Calling
Thread.sleep() with a small argument affects the system clock". Although
this is reported as fixed in JDK 1.4.0_02, several reports contradict this
(see the bug in
http://bugs.sun.com/bugdatabase/view_bug.do?bug_id=4500388).

 + Also reported by Microsoft as "The system clock may run fast when you
use the ACPI power management timer as a high-resolution counter on Windows
2000-based computers" (See http://support.microsoft.com/?id=821893)
\end{bVerbatim}

You may wish to start the daemon with debug mode on rather than doing it
using bconsole. To do so, edit the following registry key:

\begin{bVerbatim}
HKEY_LOCAL_MACHINE\HARDWARE\SYSTEM\CurrentControlSet\Services\Bacula-dir
\end{bVerbatim}

using regedit, then add \bhighlight{-dnn} after the /service option, where nn represents
the debug level you want.

\label{blb:Compatibility}
\section{Windows Compatibility Considerations}
\index[general]{Windows Compatibility Considerations}
\index[general]{Considerations!Windows Compatibility}

If you are not using the \acs{VSS} (Volume Shadow Copy) option described in the
next section of this chapter, and if any applications are running during
the backup and they have files opened exclusively, \bacula{} will not be able
to backup those files, so be sure you close your applications (or tell your
users to close their applications) before the backup.  Fortunately, most
Microsoft applications do not open files exclusively so that they can be
backed up.  However, you will need to experiment.  In any case, if \bacula{}
cannot open the file, it will print an error message, so you will always
know which files were not backed up.  See the section below on Volume
Shadow Copy Service that permits backing up any file.

During backup, \bacula{} doesn't know about the system registry, so you will
either need to write it out to an \acs{ASCII} file using \textbf{regedit~~/e} or use
a program specifically designed to make a copy or backup the registry.

\bacula{} uses Windows backup \acs{API} calls by default.  Typical of Windows,
programming these special BackupRead and BackupWrite calls is a real
nightmare of complications.  The end result gives some distinct advantages
and some disadvantages.

First, the advantages are that Windows systems, the security and
ownership information is now backed up.  In addition, with the exception of
files in exclusive use by another program, \bacula{} can now access all system
files.  This means that when you restore files, the security and ownership
information will be restored on Windows along with the data.

The disadvantage of the Windows backup \acs{API} calls is that it produces
non-portable backups.  That is files and their data that are backed up on
Windows using the native \acs{API} calls (BackupRead/BackupWrite) cannot be
directly restored on Linux or Unix systems.  \bacula{} should be able to read
non-portable backups on any system and restore the data appropriately.
However, on a system that does not have the BackupRead/BackupWrite calls
(older Windows versions and all Unix/Linux machines), though the file data
can be restored, the Windows security and access control data will not be
restored.  This means that a standard set of access permissions will be set
for such restored files.


As a default, \bacula{} backs up Windows systems using the Windows \acs{API} calls.
If you want to backup data on a Windows system and restore it on a
Unix or Linux system, we have provided a special \textbf{portable} option
that backs up the data in a portable fashion by using portable \acs{API} calls.
See the \bilink{portable option}{blb:portable} on the Include statement in a
FileSet resource in the Director's configuration chapter for the details on
setting this option.  However, using the portable option means you may have
permissions problems accessing files, and none of the security and
ownership information will be backed up or restored.  The file data can,
however, be restored on any system.

You should always be able to restore any file backed up on Unix or
Win95/98/Me to any other system.  On some older Windows systems, you may
have to reset the ownership of such restored files.

Finally, if you specify the \textbf{portable=yes} option on the files you back
up.  \bacula{} will be able to restore them on any other system.  However, any
Windows specific security and ownership information will be lost.

The following matrix will give you an idea of what you can expect. Thanks to
Marc Brueckner for doing the tests:

\LTXtable{\linewidth}{t-main-restore-portability-status}

Note: Non-portable Windows data can be restore to any machine.  In
addition, with recent \bacula{} versions, \bacula{} is able to extract data
part from Windows backup streams when restoring on a non-Windows
machine.

\label{blb:VSS}
\section{Volume Shadow Copy Service}
\index[general]{Volume Shadow Copy Service}
\index[general]{VSS}
Microsoft added \acs{VSS} to Windows XP and Windows 2003. From the perspective of
a backup-solution for Windows, this is an extremely important step. \acs{VSS}
allows \bacula{} to backup open files and even to interact with applications like
RDBMS to produce consistent file copies. \acs{VSS} aware applications are called
\acs{VSS} Writers, they register with the OS so that when \bacula{} wants to do a
Snapshot, the OS will notify the register Writer programs, which may then
create a consistent state in their application, which will be backed up.
Examples for these writers are ``\acs{MSDE}'' (\acl{MSDE}),
  ``Event Log Writer'', ``Registry Writer'' plus 3rd
party-writers.  If you have a non-vss aware application (e.g.
SQL Anywhere or probably MySQL), a shadow copy is still generated
and the open files can be backed up, but there is no guarantee
that the file is consistent.

\bacula{} produces a message from each of the registered writer programs
when it is doing a \acs{VSS} backup so you know which ones are correctly backed
up.

Technically \bacula{} creates a shadow copy as soon as the backup process
starts. It does then backup all files from the shadow copy and destroys the
shadow copy after the backup process. Please have in mind, that \acs{VSS}
creates a snapshot and thus backs up the system at the state it had
when starting the backup. It will disregard file changes which occur during
the backup process.

\acs{VSS} can be turned on by placing an

\index[dir]{Enable VSS}
\index[general]{Enable VSS}

\begin{bVerbatim}
Enable VSS = yes
\end{bVerbatim}

in your FileSet resource.

The \acs{VSS} aware File daemon has the letters \acs{VSS} on the signon line that
it produces when contacted by the console. For example:

\begin{bVerbatim}
Tibs-fd Version: 1.37.32 (22 July 2005) VSS Windows XP MVS NT 5.1.2600
\end{bVerbatim}

the \acs{VSS} is shown in the line above. This only means that the File daemon
is capable of doing \acs{VSS} not that \acs{VSS} is turned on for a particular backup.
There are two ways of telling if \acs{VSS} is actually turned on during a backup.
The first is to look at the status output for a job, e.g.:

\begin{bVerbatim}
Running Jobs:
JobId 1 Job NightlySave.2005-07-23_13.25.45 is running.
    VSS Backup Job started: 23-Jul-05 13:25
    Files=70,113 Bytes=3,987,180,650 Bytes/sec=3,244,247
    Files Examined=75,021
    Processing file: c:/Documents and Settings/kern/My Documents/My Pictures/Misc1/Sans titre - 39.pdd
    SDReadSeqNo=5 fd=352
\end{bVerbatim}

Here, you see under Running Jobs that JobId 1 is ``VSS Backup Job started \ldots{}''
This means that \acs{VSS} is enabled for that job.  If \acs{VSS} is not enabled, it will
simply show ``Backup Job started \ldots{}'' without the letters \acs{VSS}.

The second way to know that the job was backed up with \acs{VSS} is to look at the
Job Report, which will look something like the following:

\begin{bVerbatim}
23-Jul 13:25 rufus-dir: Start Backup JobId 1, Job=NightlySave.2005-07-23_13.25.45
23-Jul 13:26 rufus-sd: Wrote label to prelabeled Volume "TestVolume001" on device "DDS-4" (/dev/nst0)
23-Jul 13:26 rufus-sd: Spooling data ...
23-Jul 13:26 Tibs: Generate VSS snapshots. Driver="VSS WinXP", Drive(s)="C"
23-Jul 13:26 Tibs: VSS Writer: "MSDEWriter", State: 1 (VSS_WS_STABLE)
23-Jul 13:26 Tibs: VSS Writer: "Microsoft Writer (Bootable State)", State: 1 (VSS_WS_STABLE)
23-Jul 13:26 Tibs: VSS Writer: "WMI Writer", State: 1 (VSS_WS_STABLE)
23-Jul 13:26 Tibs: VSS Writer: "Microsoft Writer (Service State)", State: 1 (VSS_WS_STABLE)
\end{bVerbatim}

In the above Job Report listing, you see that the \acs{VSS} snapshot was generated for drive C (if
other drives are backed up, they will be listed on the \textbf{Drive(s)="C"}  You also see the
reports from each of the writer program.  Here they all report VSS\_WS\_STABLE, which means
that you will get a consistent snapshot of the data handled by that writer.

\section{\acs{VSS} Problems}
\index[general]{Problems!VSS}
\index[fd]{Problems!VSS}
\index[general]{VSS Problems}
\index[fd]{VSS Problems}

If you are experiencing problems such as \acs{VSS} hanging on \acs{MSDE}, first try
running \btool{vssadmin} to check for problems, then try running \btool{ntbackup}
which also uses \acs{VSS} to see if it has similar problems. If so, you
know that the problem is in your Windows machine and not with \bacula{}.

The \acs{FD} hang problems were reported with \textbf{MSDEwriter} when:
\begin{bitemize}
\item a local firewall locked local access to the \acs{MSDE} \acs{TCP} port (MSDEwriter
seems to use \acs{TCP}/\acs{IP} and not Named Pipes).
\item \btool{msdtcs} was installed to run under ``localsystem'': try running \btool{msdtcs}
under  networking account (instead of local system) (com+ seems to work
better with this configuration).
\end{bitemize}


\section{Windows Firewalls}
\index[general]{Firewalls!Windows}
\index[general]{Windows Firewalls}

If you turn on the firewalling feature on Windows (default in WinXP SP2),
you are likely to find that the \bacula{} ports are blocked and you cannot
communicate to the other daemons.  This can be deactivated through the
\textbf{Security Notification} dialog, which is apparently somewhere in the
\textbf{Security Center}.  I don't have this on my computer, so I cannot give the
exact details.

The command:

\begin{bVerbatim}
netsh firewall set opmode disable
\end{bVerbatim}

is purported to disable the firewall, but this command is not accepted on my
WinXP Home machine.

\section{Windows Port Usage}
\index[general]{Windows Port Usage}
\index[general]{Usage!Windows Port}

If you want to see if the File daemon has properly opened the port and is
listening, you can enter the following command in a shell window:

\begin{bVerbatim}
netstat -an | findstr 910[123]
\end{bVerbatim}

\btool{TopView} is another program that has been recommend, but it is not a
standard Windows program, so you must find and download it from the Internet.

\section{Windows Disaster Recovery}
\index[general]{Recovery!Windows Disaster}
\index[general]{Windows Disaster Recovery}

\bacula{} Systems provides a special Win\acs{BMR} (Windows Bare Metal Recovery)
program that allows you to perform a bare metal disaster recovery
of your system.

If you do not have the Win\acs{BMR} program, we do not have a good solution for
disaster recovery on Windows.  The main piece lacking, which is available
in the \bacula{} Enterprise Win\acs{BMR}, is a Windows boot floppy or a Windows boot
\acs{CD}. Microsoft releases a Windows Pre-installation Environment (\textbf{WinPE})
that could possibly work, but we have not investigated it.  This means that
until someone figures out the correct procedure, you must restore the \acs{OS}
from the installation disks, then you can load a \bacula{} client and restore
files.  Please don't count on using \btool{bextract} to extract files from
your backup tapes during a disaster recovery unless you have backed up
those files using the \textbf{portable} option.  \btool{bextract} does not run
on Windows, and the normal way \bacula{} saves files using the Windows \acs{API}
prevents the files from being restored on a Unix machine.  Once you have an
operational Windows \acs{OS} loaded, you can run the File daemon and restore your
user files.

\section{Windows FD Restrictions}
\index[general]{Windows FD Restrictions}
In recent versions of Windows, Microsoft has implemented
Volume Mount Points, Encrypted Volumes, and Deduplicated Volumes.
Current versions of the Windows File daemon support Volume Mount
Points much like on Linux systems. That is you must explicitly
add a \bhighlight{File=} line in your FileSet so that it will decend into the 
mount point.  In addition, during the restore of a mount point, the
mount point must already be defined. 

Current \bacula{} File daemons do not support Encrypted Volumes
or Deduplicated Volumes.


\section{Windows Restore Problems}
\index[general]{Problems!Windows Restore}
\index[general]{Windows Restore Problems}
Please see the
\bilink{Restore Chapter}{blb:Windows} of this manual for problems
that you might encounter doing a restore.

section{Windows Backup Problems}
\index[general]{Problems!Windows Backup}
\index[general]{Windows Backup Problems}
If during a Backup, you get the message:
\textbf{ERR=Access is denied} and you are using the portable option,
you should try both adding both the non-portable (backup \acs{API}) and
the Volume Shadow Copy options to your Director's conf file.

In the Options resource:
\begin{bVerbatim}
portable = no
\end{bVerbatim}

In the FileSet resource:
\begin{bVerbatim}
EnableVSS = yes
\end{bVerbatim}

In general, specifying these two options should allow you to backup
any file on a Windows system.  However, in some cases, if users
have allowed to have full control of their folders, even system programs
such a \bacula{} can be locked out.  In this case, you must identify
which folders or files are creating the problem and do the following:

\begin{benumerate}
\item Grant ownership of the file/folder to the Administrators group,
with the option to replace the owner on all child objects.
\item Grant full control permissions to the Administrators group,
and change the user's group to only have Modify permission to
the file/folder and all child objects.
\end{benumerate}

Thanks to Georger Araujo for the above information.

\section{Windows Ownership and Permissions Problems}
\index[general]{Problems!Windows Ownership and Permissions}
\index[general]{Windows Ownership and Permissions Problems}

If you restore files backed up from Windows to an alternate directory,
\bacula{} may need to create some higher level directories that were not saved
(or restored).  In this case, the File daemon will create them under the
SYSTEM account because that is the account that \bacula{} runs under as a
service.  As of version 1.32f-3, \bacula{} creates these files with full
access permission.  However, there may be cases where you have problems
accessing those files even if you run as administrator.  In principle,
Microsoft supplies you with the way to cease the ownership of those files
and thus change the permissions.  However, a much better solution to
working with and changing Windows permissions is the program \btool{SetACL},
which can be found at
\bref{http://setacl.sourceforge.net/}{setacl.sourceforge.net}.

If you have not installed \bacula{} while running as Administrator
and if \bacula{} is not running as a Process with the userid (User Name) SYSTEM,
then it is very unlikely that it will have sufficient permission to
access all your files.

Some users have experienced problems restoring files that participate in
the Active Directory. They also report that changing the userid under which
\bacula{} (\btool{bacula-fd.exe}) runs, from SYSTEM to a Domain Admin userid, resolves
the problem.


\section{Manually resetting the Permissions}
\index[general]{Manually resetting the Permissions}
\index[general]{Permissions!Manually resetting the}

The following solution was provided by Dan Langille \bbracket{dan at langille in the dot org domain}.
The steps are performed using Windows 2000 Server but
they should apply to most Windows platforms. The procedure outlines how to deal
with a problem which arises when a restore creates a top-level new directory.
In this example, ``top-level'' means something like \bdirectoryname{c:\textbackslash{}src},
not \bdirectoryname{c:\textbackslash{}tmp\textbackslash{}src}
where \bdirectoryname{c:\textbackslash{}tmp} already exists. If a restore job specifies \bdirectoryname{/}
as the \textbf{Where:} value, this problem will arise.

The problem appears as a directory which cannot be browsed with Windows
Explorer. The symptoms include the following message when you try to click on
that directory:

\bimageN[0.35\linewidth]{access-is-denied}{Popup on permission issue}{fig:accessdenied}

If you encounter this message, the following steps will change the permissions
to allow full access.

\begin{benumerate}
\item right click on the top level directory (in this example, \bdirectoryname{c:/src})
   and  select \textbf{Properties}.
\item click on the Security tab.
\item If the following message appears, you can ignore it, and click on \textbf{OK}.

\bimageH{view-only}{Message to ignore}{fig:messagetoignore}

You should see something like this:

\bimageH{properties-security}{Properties security}{fig:propertiessecurity}
\item click on Advanced
\item click on the Owner tab
\item Change the owner to something other than the current owner (which is
   \textbf{SYSTEM} in this example as shown below).

\bimageH{properties-security-advanced-owner}{Properties security advanced owner}{fig:propertiessecurityadvancedowner}
\item ensure the ``Replace owner on subcontainers and objects'' box is
   checked
\item click on OK
\item When the message ``You do not have permission to read the contents of
   directory \bdirectoryname{c:\textbackslash{}src} basis. Do you wish to replace
   the directory permissions with permissions granting you Full Control?'', click
on Yes.

\bimageH{confirm}{Confirm granting permissions}{fig:confirmgrantingpermissions}
\item Click on OK to close the Properties tab
   \end{benumerate}

With the above procedure, you should now have full control over your restored
directory.

In addition to the above methods of changing permissions, there is a Microsoft
program named \btool{cacls} that can perform similar functions.

\section{Backing Up the WinNT/XP/2K System State}
\index[general]{State!Backing Up the WinNT/XP/2K System}
\index[general]{Backing Up the WinNT/XP/2K System State}

Note, most of this section applies to the older Windows OSes that
do not have \acs{VSS}. On newer Windows OSes that have \acs{VSS}, all files
including the System State will by default be properly backed
up by \bacula{}.  In addition, if you want special Backup of the
System State (rather than just backup of everything) or backup
of MSSQL or MS Exchange, you should consider the \bacula{} Systems
\acs{VSS} plugin (not to be confused with enabling \acs{VSS} snapshots in the
FileSet).  The \acs{VSS} plugin has explicit knowledge that allow
you to make special backups of Microsoft System components that
have \acs{VSS} writers.  \bacula{} Systems provides specific white papers
on the components that are available for the \acs{VSS} plugin.

If you want to do this on your own, a suggestion by Damian Coutts using
Microsoft's NTBackup utility in conjunction with \bacula{} should permit a
full restore of any damaged system files on Win2K/XP. His suggestion is to
do an NTBackup of the critical system state prior to running a \bacula{}
backup with the following command:

\begin{bVerbatim}
ntbackup backup systemstate /F c:\systemstate.bkf
\end{bVerbatim}

The \textbf{backup} is the command, the \textbf{systemstate} says to backup only
the system state and not all the user files, and the
\textbf{/F c:\textbackslash{}systemstate.bkf} specifies where to write the state file.
this file must then be saved and restored by \bacula{}.

To restore the system state, you first reload a base operating system if
the \acs{OS} is damaged, otherwise, this is not necessary, then you would use
\bacula{} to restore all the damaged or lost user's files and to recover the
\bfilename{c:\textbackslash{}systemstate.bkf} file.  Finally if there are any
damaged or missing system files or registry problems, you run \btool{NTBackup}
and \textbf{catalogue} the system statefile, and then select it for
restore.  The documentation says you can't run a command line restore of
the systemstate.

To the best of my knowledge, this has not yet been tested. If you test it,
please report your results to the \bacula{} email list.

Note, \bacula{} uses \acs{VSS} to backup and restore open files and
system files, but on older Windows machines such as WinNT and
Win2000, \acs{VSS} is not implemented by Microsoft so that you must
use some special techniques to back them up as described
above.  On new Windows machines, \bacula{} will backup and restore
all files including the system state providing you have
\acs{VSS} enabled in your \bacula{} FileSet (default).

\section{Fixing the Windows Boot Record}
\index[general]{Fixing the Windows Boot Record}
\index[general]{Windows!Fixing the Boot Record}
A tip from a user:
An effective way to restore a Windows backup for
those who do not purchase the bare metal restore
capability is to install Windows on a different
hard drive and restore the backup.  Then run the
recovery \acs{CD} and run

\begin{bVerbatim}
diskpart
   select disk 0
   select part 1
   active
   exit

bootrec /rebuldbcd
bootrec /fixboot
bootrec /fixmbr
\end{bVerbatim}


\section{Considerations for Filename Specifications}
\index[general]{Windows!Considerations for Filename Specifications}

Please see the \bilink{Director's Configuration chapter}{blb:win32} of this
manual for important considerations on how to specify Windows paths in
\bacula{} FileSet Include and Exclude directives.

\index[general]{Unicode}
The Window \bacula{} File daemon as well as
\btool{bconsole} and \btool{bwx-console} support Windows
Unicode filenames. There may still be some problems with multiple byte
characters (e.g. Chinese, \ldots{}) where it is a two byte character but the
displayed character is not two characters wide.

\index[general]{Windows Path Length Restriction}
Path/filenames longer than 260 characters (up to 32,000) are supported.

\section{Windows Specific File daemon Command Line}
\index[general]{Client!Windows Specific File daemon Command Line Options}
\index[general]{Windows Specific File daemon Command Line Options}

These options are not normally seen or used by the user, and are documented
here only for information purposes. At the current time, to change the default
options, you must either manually run \textbf{\bacula{}} or you must manually edit
the system registry and modify the appropriate entries.

In order to avoid option clashes between the options necessary for
\textbf{\bacula{}} to run on Windows and the standard \bacula{} options, all Windows
specific options are signaled with a forward slash character (/), while as
usual, the standard \bacula{} options are signaled with a minus (\texttt{-}), or a minus
minus (\texttt{{-}{-}}). All the standard \bacula{} options can be used on the Windows
version. In addition, the following Windows only options are implemented:

\begin{bdescription}

\item [/service ]
   \index[fd]{/service}
   Start \bacula{} as a service

\item [/run ]
   \index[fd]{/run}
   Run the \bacula{} application

\item [/install ]
   \index[fd]{/install}
   Install \bacula{} as a service in the system registry

\item [/remove ]
   \index[fd]{/remove}
   Uninstall \bacula{} from the system registry

\item [/about ]
   \index[fd]{/about}
   Show the \bacula{} about dialogue box

\item [/status ]
   \index[fd]{/status}
   Show the \bacula{} status dialogue box

\item [/events ]
   \index[fd]{/events}
   Show the \bacula{} events dialogue box (not  yet implemented)

\item [/kill ]
   \index[fd]{/kill}
   Stop any running \textbf{\bacula{}}

\item [/help ]
   \index[fd]{/help}
   Show the \bacula{} help dialogue box
\end{bdescription}

It is important to note that under normal circumstances the user should never
need to use these options as they are normally handled by the system
automatically once \bacula{} is installed. However, you may note these options in
some of the \bfilename{.bat} files that have been created for your use.

\section{Shutting down Windows Systems}
\index[general]{Shutting down Windows Systems}
\index[general]{Systems!Shutting down Windows}

Some users like to shutdown their Windows machines after a backup using a
Client Run After Job directive. If you want to do something similar, you might
take the shutdown program from the
\bfootref{http://www.apcupsd.com}{apcupsd project} or one from the
\bfootref{http://technet.microsoft.com/en-us/sysinternals/bb897541.aspx}{Sysinternals project}.
