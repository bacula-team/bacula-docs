\chapter{Volumes}\label{chap:volumes}

Bacula manages all its storage compartments -- Volumes in our terminology -- in Pools. In particular, a Job is not configured to write to any particular Volume, but to a set of Volumes called a Pool.
Volumes of Bacula are always members of only one Pool. There can however be multiple pools.
Accordingly, we present the conceptual overview here starting with the higher-level Pool view.

\section{The ``Pool''}
\subsection{The Concept}
Pools in Bacula are just a way to manage volumes and to manage collections of similar volumes. The following are just examples and you might have either different requirements or even no requirement to separate volumes into several pools:
\begin{bitemize}
\item Pools ``short-term'' and ``long-term'' to handle short and long retention periods.
\item Pools ``big-customer'' and ``usual-customers'' to keep all the backup jobs from your client ``Big Customer'' in a specific pool while putting all ``usual'' customers into another one.
\item Pools ``r-and-d'' and ``accounting'' to keep R\&D backups together and the Accounting ones in another pool, separated from each other.
\end{bitemize}
There can be many reasons to use or not use several pools; the Bacula configuration gives the Backup Administrator the ability to match the defined requirements.

Due to how Bacula manages Volume Retention periods, the jobs put into the same pool should have the same retention periods.

\subsection{Pool Types}
Bacula allows several kinds of pools including\footnote{Listed in the logical order}:
\begin{bitemize}
\item Scratch
\item Backup
\item Recycle
\end{bitemize}
A \emph{Scratch} pool contains volumes that may be with any pool. If no volumes are available for a particular pool Bacula will look in the Scratch pool and move the volume to the pool in question.

A \emph{Backup} pool contains volumes intended to keep data from backups for a defined retention period. It can also include Recycled or Purged volumes, i.\,e. Volumes that are eligible to be overwritten.

A \emph{Recycle} pool contains volumes after the purging process freed them, \textbf{after} they have been used in a \emph{Backup} pool.

The Bacula configuration allows the Administrator to attach a \emph{Scratch} Pool and a \emph{Recycle} Pool to a \emph{Backup} pool. The volume cycle management process is described by the figure~\vref{fig:volume-cycle}.

\bimageH[\linewidth]{bee8-volume-cycling}{Volume Cycling Process}{fig:volume-cycle}
%% \begin{figure}
%%   \begin{center}
%%     \includegraphics[width=\linewidth]{bee8-volume-cycling}
%%     \caption{Volume Cycling Process}\label{fig:volume-cycle}
%%   \end{center}
%% \end{figure}
The following example is a pool definition to manage Volumes for day-to-day incremental backup jobs:
\begin{bVerbatim}
  # Pool to handle day-to-day backup jobs
  Pool {
     Name = PoolFile
     Pool Type = Backup
     Recycle = yes
     AutoPrune = yes
     Volume Retention = 15 days
     Label Format = "file-"
           # Auto-labeling new volumes
     Action on Purge = Truncate
           # Backup to disk. We get back the space ASAP.
     Next Pool = PoolVFull
           # This is the pool where Virtual Full will take place
     Storage = t-disk
     Volume Use Duration = 23h
           # Low duration time, better volume retention period.
     Maximum Volumes = 750
     Maximum Volume Bytes  = 1GB
           # Small volumes. A lot of them. This is for incrementals.
  }
\end{bVerbatim}
In the next example you  will find a pool definition to keep the Virtual Full backups for the previously described incremental backup jobs:
\begin{bVerbatim}
  #
  # Virtual Full in this pool
  Pool {
     Name = PoolVFull
     Pool Type = Backup
     Recycle = yes
     AutoPrune = yes
     Volume Retention = 20 days
     Label Format = "file-vfull-"
          # Auto-labeling new volumes
     Action on Purge = Truncate
          # Backup to disk. We get back the space ASAP.
     Next Pool = PoolRDX
          # This is the pool where Copy jobs will be sent
     Storage = t-migrate
     Volume Use Duration = 23h
          # Low duration time, better volume retention period.
     Maximum Volume Bytes = 10GB
     Maximum Volumes = 20
          # Few volumes, bigger than the incremental ones. For ``Full'' (VF) purpose.
  }
\end{bVerbatim}
And below, we show a pool definition to store the Copy volumes for the previous Full backup jobs:
\begin{bVerbatim}
  #
  # Copy of Fulls in this pool
  Pool {
     Name = PoolRDX
     Pool Type = Backup
     Recycle = yes
     AutoPrune = yes
     Volume Retention = 40 days
     Action on Purge = Truncate
          # Backup to disks. We get back the space ASAP.
     Volume Use Duration = 20h
          # Low duration time, better volume retention period.
     Storage = t-rdx
     Maximum Volume Bytes = 20GB
          # 320GB RDX removable disk. 20GB per RDX volume.
  }
\end{bVerbatim}
%
\section{Media Type}
Even if Media Types may be used for several different reasons with Bacula, you must use them when dealing with several storages or several locations on the same storage.\par
In particular when Bacula is searching for a volume to be used for a backup, it will go through the available volumes. A Bacula job uses a distinct Storage device and the volume selection must be restricted to the actually available volumes. For example, it is quite common to see different disk storage arrays, mounted at different mount points and used by different storage devices, where volumes located in one directory are not available through the other storage device.

More obvious (and less easy to fix!) is a case of mixed media -- for example, a disk storage device will never be able to write to a physical tape. So, all storage devices that cannot share their media must use distinct Media Types.

\section{Storage Capacity Management}
If you want to overwrite a backup you need to overwrite the corresponding volume(s). Additionally, to be able to reuse a Volume, its retention period should first be expired.

\subsection{Volume Retention}\label{par:volume-retention}
The Volume retention period is defined as the time Bacula will not overwrite a Volume after it was last written to.
Therefore, if you keep writing to a volume with a retention period of seven days for six months, your volume will not be overwritten for a total of 187~days -- perhaps even more: remember that Bacula will try to keep your data safe for as long as possible.

\subsection{Limiting the Volume Size}
\subsubsection{Tapes}
On tapes, we usually don't need to explicitly limit the volume size, because the volume is identical to the tape which has a limited and defined maximum capacity.

\subsubsection{Disks}
On disks, on the other hand, as volumes are handled with files, there is a risk of filling the disk with only one volume, so we need to limit the volume size and number of volumes. We explained above how the volume retention periods are handled by Bacula. However, often we need to get back our volumes as soon as possible to be able to reuse the storage space they are using during their retention periods. In the previous case (see chapter~\vref{par:volume-retention}) we should have restricted the use duration of the Volume.

There are several directives available to limit the occupied storage space in a Pool:
\begin{bitemize}
\item Maximum Volumes: the pool will contain at most this number of volumes
\item Maximum Volume Bytes: a volume won't have a size above this value, even if it handles concurrent jobs
\item Maximum Volume Jobs: this is a way to limit a volume usage duration. As soon as the volume contains this number of jobs, Bacula will refuse to use it and will use another one
\item Maximum Use Duration: this is also a way to limit the use duration of a volume.
\end{bitemize}
These limitations can be managed manually (by the Administrator with console commands) or automatically (through scripts or Admin jobs, for example).

\subsection{Space Management}
As we already noted, Bacula will try to keep your data safe as long as possible. This means that you can be in a situation (disk backups only) where your storage space is too small even if you defined correct retention periods. In such cases, you can use the ``Action on Purge'' directive requesting Bacula to actually \texttt{truncate} the disk volumes and thus to get back the corresponding storage space freed by the truncate.
%overwriting backups => overwriting volumes
%same retention on ``similar'' volumes
%how to limit size and number
%tapes: ok by nature
%
%disks volumes: administrator has to prepare the configuration to limit how much data on a volume and how many volumes we want to have
%
%space management
\section{Volumes and Pool Modifications}
Because of its great flexibility, Bacula can be puzzling. Let's say you defined Maximum Volume Bytes to 10\,GB and then realize that you want to move it to 15\,GB. Then you will modify your ``Pool'' resource definition. This is correct and will work for all new volumes created into the Pool. But the existing volumes are not modified by this change. To update the existing volumes already in the Pool with the new values you must update the existing volumes' meta-data:
\begin{bitemize}
\item In \btool{bconsole}, enter the \bcommand{update volume} and answer the subsequent questions Bacula will ask, or
\item use \bcommand{update volume allfrompool=the-pool-name}, or
\item use one of the graphical interfaces.
\end{bitemize}
