\chapter{Autochanger Resource}
\index[sd]{Autochanger Resource}
\index[sd]{Resource!Autochanger}

The Autochanger resource supports single or multiple drive
autochangers by grouping one or more Device resources
into one unit called an autochanger in \bacula{} (often referred to
as a "tape library" by autochanger manufacturers).

If you have an Autochanger, and you want it to function correctly,
you \textbf{must} have an Autochanger resource in your Storage
conf file, and your Director's Storage directives that want to
use an Autochanger \textbf{must} refer to the Autochanger resource name.
In previous versions of \bacula{}, the Director's Storage directives
referred directly to Device resources that were autochangers.
In version 1.38.0 and later, referring directly to Device resources
will not work for Autochangers.

\begin{bdescription}
\bwebdoc{Storage:Autochanger:Name}
\item [Name = \bbracket{Autochanger-Name}]
   \index[sd]{Name}
   Specifies the Name of the Autochanger.  This name is used in the
   Director's Storage definition to refer to the autochanger.  This
   directive is required.

\bwebdoc{Storage:Autochanger:Device}
\item [Device = \bbracket{Device-name1, device-name2, \ldots{}}]
   Specifies the names of the Device resource or resources that correspond
   to the autochanger drive.  If you have a multiple drive autochanger, you
   must specify multiple Device names, each one referring to a separate
   Device resource that contains a Drive Index specification that
   corresponds to the drive number base zero.  You may specify multiple
   device names on a single line separated by commas, and/or you may
   specify multiple Device directives.  This directive is required.

\bwebdoc{Storage:Autochanger:ChangerDevice}
\item [Changer Device = \bbracket{name-string}]
   \index[sd]{Changer Device}
   The specified \textbf{\bbracket{name-string}} gives the system file name of the  autochanger
   device name. If specified in this resource, the Changer Device name
   is not needed in the Device resource. If it is specified in the Device
   resource (see above), it will take precedence over one specified in
   the Autochanger resource.

\bwebdoc{Storage:Autochanger:ChangerCommand}
\item [Changer Command = \bbracket{name-string}]
   \index[sd]{Changer Command}
   The \textbf{\bbracket{name-string}} specifies an external program to be called  that will
   automatically change volumes as required by \textbf{\bacula{}}.  Most frequently,
   you will specify the \bacula{} supplied \mtxchanger{}  script as follows.
   If it is specified here, it need not be specified in the Device
   resource. If it is also specified in the Device resource, it will take
   precedence over the one specified in the Autochanger resource.

\bwebdoc{Storage:Autochanger:End}
\end{bdescription}

The following is an example of a valid Autochanger resource definition:

\begin{bVerbatim}
Autochanger {
  Name = "DDS-4-changer"
  Device = DDS-4-1, DDS-4-2, DDS-4-3
  Changer Device = /dev/sg0
  Changer Command = "/opt/bacula/scripts/mtx-changer %c %o %S %a %d"
}
Device {
  Name = "DDS-4-1"
  Drive Index = 0
  Autochanger = yes
  ...
}
Device {
  Name = "DDS-4-2"
  Drive Index = 1
  Autochanger = yes
  ...
Device {
  Name = "DDS-4-3"
  Drive Index = 2
  Autochanger = yes
  Autoselect = no
  ...
}
\end{bVerbatim}

Please note that it is important to include the \textbf{Autochanger = yes} directive
in each Device definition that belongs to an Autochanger.  A device definition
should not belong to more than one Autochanger resource.  Also, your Device
directive in the Storage resource of the Director's conf file should have
the Autochanger's resource name rather than a name of one of the Devices.

If you have a drive that physically belongs to an Autochanger but you don't want
to have it automatically used when \bacula{} references the Autochanger for backups,
for example, you want to reserve it for restores, you can add the directive:

\begin{bVerbatim}
Autoselect = no
\end{bVerbatim}

to the Device resource for that drive. In that case, \bacula{} will not automatically
select that drive when accessing the Autochanger. You can, still use the drive
by referencing it by the Device name directly rather than the Autochanger name. An example
of such a definition is shown above for the Device DDS-4-3, which will not be
selected when the name DDS-4-changer is used in a Storage definition, but will
be used if DDS-4-3 is used.
