#
#-------------------------------------------------------------------------
SHELL = /bin/sh

AT=@

#
# bacula.env file is a local file to define specific values if any
# bacula.env.dist is a template for it.
BACULAENVFILE=bacula.env
ENVFILE=$(shell [ -e ./$(BACULAENVFILE) ] && echo "OK")
ifeq ($(ENVFILE),OK)
  include $(BACULAENVFILE)
  $(info Found env file $(BACULAENVFILE).)
else
  $(error No environment file "$(BACULAENVFILE)" found. You can copy "$(BACULAENVFILE).dist" to "$(BACULAENVFILE)" and adjust some definitions.)
endif
#
# Where is latex2html tool
BLATEX2HTMLDIR=$(CURDIR)/latex2html-2016
#
# LaTeX directory to build bacula documentation (classes and layout)
LATEX_DIR=$(CURDIR)/latex
#
# latex2html requirements:
## - The latex2html Makefile is required to build latex2html accordingly to our needs
LATEX2HTMLMAKEFILE=$(BLATEX2HTMLDIR)/Makefile
## Destination of the build of latex2html (make install)
BBINDIR=$(CURDIR)/latex2html
## tex files location for latex2html
BTEXDIR=$(LATEX_DIR)/latex2html
## Perl5 libraries to build
PERL5LIB=$(BLATEX2HTMLDIR)
## Files required for latex2html process when compiling tex files.
TRANSLATEIMGS=$(CURDIR)/manuals/translate_images.pl
LATEX2HTMLINIT=$(CURDIR)/manuals/latex2html-init.pl
## latex2html binary
LATEX2HTML=$(BBINDIR)/bin/latex2html
## Tool directory for the post-process
TOOLS_DIR=$(CURDIR)/tools
#
# PDF to latex compiler
ifneq ($(TEX_TO_PDF),)
  $(info Using $(TEX_TO_PDF) compiler.)
else
  $(info PDF compiler $(TEX_TO_PDF) undefined.)
  TEX_TO_PDF=pdflatex
endif
COMPILER=$(shell which $(TEX_TO_PDF))
ifneq ($(COMPILER),)
  $(info Compiler $(COMPILER) found.)
else
# This will *stop* the Makefile
  $(error Unable to find $(TEX_TO_PDF))
endif
#
# For version.tex building process
## bacula source directory
ifeq ($(BACULASRC),)
  BACULASRC=$(HOME)
endif
# Location of the version.h file
VERSION_H=$(shell find $(BACULASRC) -name "version.h" -print)
ifeq ($(VERSION_H),)
  $(error Unable to find version.h in Bacula source tree $(BACULASRC).)
else
  $(info $(VERSION_H) file found into $(dir $(VERSION_H)))
endif
## Bacula source directory (in the version.h point of view)
SOURCES_DIR=$(dir $(VERSION_H))
## Location and name for the version.tex file
VERSION_TEX=$(LATEX_DIR)/version.tex
#
# Directories required
## images 
IMAGES_DIR=$(CURDIR)/images
PNGIMAGES_DIR=$(IMAGES_DIR)/png
JPGIMAGES_DIR=$(IMAGES_DIR)/jpg
SVGIMAGES_DIR=$(IMAGES_DIR)/svg
## covers for manuals
COVERS_DIR=$(CURDIR)/covers
## PDF covers
PDFCOVERS_DIR=$(COVERS_DIR)/pdf
## SVG covers
SVGCOVERS_DIR=$(COVERS_DIR)/svg
## Licence files
LICENCE_DIR=$(MANUALS_DIR)/licences
## Coverpage name. There is only one coverpage used by the .cls LaTeX class file.
BMANUAL_NAME=bcoverpage
#
# Makefiles
## The following Makefile is unique for PDF building and is used whatever the manual is.
GENERAL_MAKEFILE=$(CURDIR)/manuals/Makefile
## The following Makefile is unique for HTML building and is used whatever the manual is.
WEB_MAKEFILE=$(CURDIR)/manuals/web.makefile

export BACULAVERSION
export PERL5LIB
export LATEX_DIR
export IMAGES_DIR
export COVERS_DIR
export PDFCOVERS_DIR
export SVGCOVERS_DIR
export LICENCE_DIR
export BMANUAL_NAME
export COMPILER
export COVERPAGE_NAME
export GENERAL_MAKEFILE
export WEB_MAKEFILE
export LATEX2HTMLINIT
export TRANSLATEIMGS
export LATEX2HTML
export TOOLSDIR
export VERSION_TEX
export TOOLS_DIR
export PNGIMAGES_DIR
export JPGIMAGES_DIR
export SVGIMAGES_DIR
export SVGCOVERS_DIR

externalreferencesfile=$(LATEX_DIR)/external-references.tex

# Installation target directories & other installation stuff
prefix = /usr/local
exec_prefix = ${prefix}
binprefix =
manprefix =
sbindir = ${exec_prefix}/sbin
sysconfdir = ${prefix}/etc
scriptdir = @scriptdir@
mandir = ${datarootdir}/man/man1
manext = 1
datarootdir=${prefix}/share


# Tools & program stuff
MV = /bin/mv
RM = /bin/rm
RMF = /bin/rm -f
CP = /bin/cp
SED = @SED@
AWK = /usr/bin/gawk
ECHO = /bin/echo
CMP = @CMP@
INSTALL = /usr/bin/install -c
# add the -s to the following in PRODUCTION mode
INSTALL_PROGRAM = /usr/bin/install -c -m @SBINPERM@
INSTALL_DATA = /usr/bin/install -c -m 644
INSTALL_SCRIPT = /usr/bin/install -c -m @SBINPERM@
INSTALL_CONFIG = /usr/bin/install -c -m 640


# End of common section of the Makefile
#-------------------------------------------------------------------------

srcdir =	.
.PHONY: latex2html
.PATH:		.

basedir = ..
topdir = ..
thisdir = docs
TRANSLATE=./htmls.sh

#
# Distribution variables
#

de_dirs = manuals/de/console manuals/de/developers manuals/de/main \
  manuals/de/misc manuals/de/problems manuals/de/utility


en_dirs = manuals/en/concept manuals/en/console manuals/en/developers manuals/en/main \
  manuals/en/misc manuals/en/problems manuals/en/utility

es_dirs = manuals/es/console manuals/es/developers manuals/es/main \
  manuals/es/misc manuals/es/problems manuals/es/utility

fr_dirs = manuals/fr/console manuals/fr/developers manuals/fr/main \
  manuals/fr/misc manuals/fr/problems manuals/fr/utility

image_dirs = images/svg
all_dirs = ${de_dirs} ${en_dirs} ${es_dirs} ${fr_dirs} ${image_dirs}

#-------------------------------------------------------------------------

en: all

all:	$(VERSION_TEX) latex2html pdf-manuals html-manuals
	$(AT)echo ""
	$(AT)echo "All manuals built ..."
	$(AT)echo ""
	$(AT)echo ""
	$(AT)echo ""

latex2html: $(BBINDIR)

$(BBINDIR): $(LATEX2HTMLMAKEFILE)
	$(AT)$(MAKE) -C $(BLATEX2HTMLDIR) && $(MAKE) -C $(BLATEX2HTMLDIR) install

$(LATEX2HTMLMAKEFILE):
	$(AT)echo "Building latex2html Makefile"
	$(AT)(cd ${BLATEX2HTMLDIR} ; ./configure --prefix=${BBINDIR} --with-texpath=${BTEXDIR})
	$(AT)echo "Makefile installed."

pdf-manuals: external-references
	$(AT)echo " "
	$(AT)echo "Generating PDF manuals..."
	$(AT)for I in ${en_dirs}; \
	  do $(MAKE) -C $$I pdflatex || (echo ""; echo ""; echo "  ====== Error in `pwd` ======"; echo ""; echo ""; exit 1;); done

#(cd $$I; echo "==>Entering directory `pwd`"; \
	      	done
	$(AT)echo ""
	$(AT)echo "All PDF manuals built ..."
	$(AT)echo ""
	$(AT)echo ""
	$(AT)echo ""

html-manuals:
	$(AT)echo " "
	$(AT)echo "Creating Web documentation..."
	$(AT)for I in ${en_dirs}; \
	  do (cd $$I; echo "==>Entering directory `pwd`"; \
	      $(MAKE) web || (echo ""; echo ""; echo "	====== Error in `pwd` ======"; \
			    echo ""; echo ""; exit 1;)); \
	done
	$(AT)echo "Handling external references"
	$(AT)for I in ${en_dirs} ;\
	  do (cd $$I ; $(MAKE) references ||  (echo ""; echo ""; echo "	====== Error in `pwd` ======"; \
			    echo ""; echo ""; exit 1;)); \
	done
	$(AT)echo "Remove links ..."
	$(AT)find . -type l -exec rm {} \;
	$(AT)echo ""
	$(AT)echo "All HTML manuals built ..."
	$(AT)echo ""
	$(AT)echo ""
	$(AT)echo ""


templates:
	$(AT)echo " "
	$(AT)echo -n "Linking templates.."
	$(AT)mkdir -p ~/texmf/tex/latex/bsys/
	$(AT)if [ ! -e  ~/texmf/tex/latex/bsys/bsysmanual ] || [ -h ~/texmf/tex/latex/bsys/bsysmanual ]; \
	   then\
		ln -sf `pwd`/bsysmanual ~/texmf/tex/latex/bsys/;\
	fi
	mktexlsr ~/texmf
	$(AT)echo "Done"

external-references:
	$(AT)echo " "
	$(AT)echo -n "Building external references file"
	$(AT)find ${en_dirs} -mindepth 0 -maxdepth 1 -name "*tex" -exec grep -q '\label' {} \; -print| awk -F/ '{ print "\\externaldocument[" $$3 "-]{../"$$3"/"$$4"}"}'|sed -e 's/.tex//g' > ${externalreferencesfile}
	$(AT)for I in ${en_dirs}; \
	  do $(MAKE) -C $$I $@ || (echo ""; echo ""; echo "  ====== Error in `pwd` ======"; echo ""; echo ""; exit 1;); done
	$(AT)echo "All $@ built ..."
	$(AT)echo "External references for all manuals built."

bacula-web:
	$(AT)(if [ -x bacula-web ]; then cd bacula-web; make; fi)

fr:	french

french:
#	$(AT)for I in ${fr_dirs}; do (if [ -x $$I ]; then cd $$I; cp -f ../../version.tex .; fi); done
	$(AT)for I in ${fr_dirs}; \
	  do (cd $$I; echo "==>Entering directory `pwd`"; \
	      $(MAKE) all || (echo ""; echo ""; echo "	====== Error in `pwd` ======"; \
			    echo ""; echo ""; exit 1;)); \
	done

de:	german

german:
#	$(AT)for I in ${de_dirs}; do (if [ -x $$I ]; then cd $$I; cp -f ../../version.tex .; fi); done
	$(AT)for I in ${de_dirs}; \
	  do (cd $$I; echo "==>Entering directory `pwd`"; \
	      $(MAKE) all || (echo ""; echo ""; echo "	====== Error in `pwd` ======"; \
			    echo ""; echo ""; exit 1;)); \
	done

es:	spanish

spanish:
#	$(AT)for I in ${es_dirs}; do (if [ -x $$I ]; then cd $$I; cp -f ../../version.tex .; fi); done
	$(AT)for I in ${es_dirs}; \
	  do (cd $$I; echo "==>Entering directory `pwd`"; \
	      $(MAKE) all || (echo ""; echo ""; echo "	====== Error in `pwd` ======"; \
			    echo ""; echo ""; exit 1;)); \
	done


$(basedir)/$(VERNAME).lsm: LSM.in $(srcdir)/../autoconf/Make.common.in $(srcdir)/../src/version.h
	$(SED) $(SEDREPLACE) < $(srcdir)/LSM.in > $@

clean:
	$(AT)$(RMF) *~ 1 2 3 bacula-doc*.tar.gz
	$(AT)(if [ -x bacula-web ] ; then cd bacula-web; make clean; fi)
	$(AT)for I in ${all_dirs}; \
	  do (if [ -x $$I ]; then cd $$I; echo "==>Entering directory `pwd`"; ${MAKE} $@ || exit 1; fi); done


realclean: clean

distclean: $(LATEX2HTMLMAKEFILE) clean
	$(AT)(cd $(BLATEX2HTMLDIR) ; make $@)
	$(AT)rm -rf $(BBINDIR)
	$(AT)rm -rf $(BTEXDIR)
	$(AT)$(RMF) -r CVS html-manual/CVS home-page/CVS techlogs/CVS
	$(AT)$(RMF) -rf autom4te.cache bacula-doc-* config.log config.out
	$(AT)$(RMF) -f config.status kernsconfig
	$(AT)(if [ -x bacula-web ] ; then cd bacula-web; make clean; fi)
	$(AT)for I in ${all_dirs}; \
	  do (if [ -x $$I ]; then ${MAKE} -C $$I $@ || exit 1; fi); done
	$(AT)rm -f ${VERSION_TEX}
	$(AT)rm -f ${externalreferencesfile}



devclean:

depend:

install:
#	$(INSTALL_DATA) bacula.1 $(DESTDIR)$(mandir)/$(manprefix)bacula.$(manext)

uninstall:
#	-cd $(mandir); $(RMF) $(manprefix)bacula.$(manext)


version: $(VERSION_TEX)

$(VERSION_TEX): $(VERSION_H)
	$(info Building version.tex)
	$(AT)bacula=$(abspath $(SOURCES_DIR)../..) versionh=$(VERSION_H) versiontex=$(VERSION_TEX) ./update_version


verbose:
	$(AT)echo "version.h file: $(VERSION_H)\n"
	$(AT)echo "version.h directory: $(SOURCES_DIR)\n"
	$(AT)echo "version.tex file: $(VERSION_TEX)"
	$(AT)echo "LATEX2HTML_OK: $(LATEX2HTML_OK)"
	$(AT)echo "TEX_TO_PDF: >$(TEX_TO_PDF)<"
	$(AT)echo "TEX_TO_PDF_OK: $(TEX_TO_PDF_OK)"
