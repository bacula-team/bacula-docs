s\chapter{Automatic Volume Recycling}
\label{blb:RecyclingChapter}
\index[general]{Recycling!Automatic Volume }
\index[general]{Automatic Volume Recycling }

By default, once \bacula{} starts writing a Volume, it may append to the
volume, but it will not overwrite the existing data thus destroying it.
However when \bacula{} \textbf{recycles} a Volume, the Volume becomes available
for being reused, and \bacula{} can at some later time overwrite the previous
contents of that Volume.  At that point all previous data on that Volume
will be lost.  If the Volume is a tape, the tape will be rewritten from the
beginning.  If the Volume is a disk file, the file will be truncated before
being rewritten.

You may not want Bacula to automatically recycle (reuse) Volumes.  Doing so
may require a large number of Volumes though.  However, it is also possible
to manually recycle Volumes so that they may be reused.  For more on manual
recycling, see the section entitled \bilink{Manually Recycling Volumes}{blb:manualrecycling} below in this chapter.

Most people prefer to have a Pool of Volumes that are used for daily backups and
recycled once a week, another Pool of Volumes that are used for Full backups
once a week and recycled monthly, and finally a Pool of Volumes that are used
once a month and recycled after a year or two. With a scheme like this, the
number of Volumes in your pool or pools remains constant.

By properly defining your Volume Pools with appropriate Retention periods,
\bacula{} can manage the recycling (such as defined above) automatically. 

Automatic recycling of Volumes is controlled by four records in the \bresourcename{Pool}
resource definition in the Director's configuration file. These four
records are:

\begin{bitemize}
\item AutoPrune = yes
\item VolumeRetention = \bbracket{time}
\item Recycle = yes
\item RecyclePool = \bbracket{APool}
\item ScratchPool = \bbracket{APool}
\end{bitemize}

The above first three directives are all you need assuming that you fill
each of your Volumes then wait the Volume Retention period before
reusing them, providing there is some non-pruned Jobs or Files on the
Volume. \bdirectivename{Recycle Pool} and \bdirectivename{Scratch Pool} directives are not required. If not
defined, \bacula{} will recycle Volumes and keep them in the current Pool.

If you want \bacula{} to stop using a Volume and recycle
it before it is full, you will need to use one or more additional 
directives such as:

\begin{bitemize}
\item \bvalueddirective{Use Volume Once}{yes}
\item \bvalueddirective{Volume Use Duration}{ttt}
\item \bvalueddirective{Maximum Volume Jobs}{nnn}
\item \bvalueddirective{Maximum Volume Bytes}{mmm}
\end{bitemize}

Please see below and
the \bilink{Basic Volume Management}{blb:DiskChapter} chapter
of this manual for more complete examples.

Automatic recycling of Volumes is performed by \bacula{} only when it wants a
new Volume and no appendable Volumes are available in the Pool. It will then
search the Pool for any Volumes with the \bstatus{Recycle} flag set and the
Volume Status is \bstatus{Purged}. At that point, it will choose the oldest
purged volume and recycle it.

If there are no volumes with status \bstatus{Purged}, then the recycling occurs
in two steps:
\smallskip

The first is that the Catalog for a Volume must be pruned of all Jobs (i.e.
Purged) and Files contained on that Volume.
\smallskip

The second step is the actual recycling of the Volume.  Only Volumes marked
\bstatus{Full} or \bstatus{Used} will be considered for pruning.  The Volume will be
purged, all Jobs and Files associated to this Volume are pruned from Catalog,
if the \bdirectivename{VolumeRetention} period has expired.  When a Volume is marked
as \bstatus{Purged}, it means that no Catalog records for Jobs and Files reference
that Volume, and the Volume can be recycled and reused.  Please note a Volume
can be reused even though the \bdirectivename{Volume Retention} period has not expired if the
Jobs and Files associated to the Volume had been already pruned.  Until
recycling actually occurs, the Volume data remains intact.  If no Volumes
can be found for recycling for any of the reasons stated above, \bacula{} will
request operator intervention (i.e.  it will ask you to label a new volume).


A key point mentioned above, that can be a source of frustration, is that
\bacula{} will only recycle purged Volumes if there is no other appendable
Volume available, otherwise, it will always write to an appendable Volume
before recycling even if there are Volumes marked as \bstatus{Purged}.  This preserves
your data as long as possible.  So, if you wish to ``force'' \bacula{} to use a
purged Volume, you must first ensure that no other Volume in the Pool is
marked \bstatus{Append}.  If necessary, you can manually set a volume to \bstatus{Full}.
The reason for this is that \bacula{} wants to preserve the data on
your old Volumes (even though purged from the catalog) as long as absolutely
possible before overwriting it.  There are also a number of directives such
as \bdirectivename{Volume Use Duration} that will automatically mark a volume as \bstatus{Used}
and thus no longer appendable.

\label{blb:AutoPruning}
\section{Automatic Pruning}
\index[general]{Automatic Pruning}
\index[general]{Pruning!Automatic}

As \bacula{} writes files to a Volume, it keeps a list of files, jobs, and volumes
in a database called the catalog.  Among other things, the database helps
\bacula{} to decide which files to back up in an incremental or differential
backup, and helps you locate files on past backups when you want to restore
something.  However, the catalog will grow larger and larger as time goes
on, and eventually it can become unacceptably large.

\bacula{}'s process for removing entries from the catalog is called Pruning.
The default is Automatic Pruning, which means that once a Job record
reaches a certain age (e.g.  30 days old) and a pruning occurs, it will be
removed from the catalog.  Note that Job records that are required for
current restore won't be removed automatically, and File records are needed
for VirtualFull and Accurate backups.  Once a job has been pruned, you can
still restore it from the backup Volume, provided that the Volume has not
been recycled, but one additional step is required: scanning the volume
with \bcommandname{bscan}.  The alternative to Automatic Pruning is Manual Pruning, in
which you explicitly tell \bacula{} to erase the catalog entries for a volume.
You'd usually do this when you want to reuse a \bacula{} volume, because
there's no point in keeping a list of files that USED TO BE on a volume.
Or, if the catalog is starting to get too big, you could prune the oldest
jobs to save space.  Manual pruning is done with the
\bxlink{prune}{blb:ManualPruning}{console}{command} in the \bconsoleman{}
 (thanks to Bryce Denney for the above explanation).

\section{Pruning Directives}
\index[general]{Pruning Directives }
\index[general]{Directives!Pruning }
There are three pruning durations.  All apply to catalog database Jobs
and Files records and not to the actual data in a Volume.  The pruning
(or retention) durations are for: Volumes (Jobs and Files records in the
Volume), Jobs (Job records), and Files (File records).  The durations
inter-depend a bit because if \bacula{} prunes a Volume, it automatically
removes all the Job records, and, consequently, all the File records.
Also when a Job record is pruned, all the File records for that
Job are also pruned (deleted) from the catalog.

Having the File records in the database means that you can examine all the
files backed up for a particular Job. They take the most space in the catalog
(probably 90-95\% of the total). When the File records are pruned, the Job
records can remain, and you can still examine what Jobs ran, but not the
details of the Files backed up. In addition, without the File records, you
cannot use the Console \bcommandname{restore} command to restore specific files.

When a Job record is pruned, the Volume (Media record) for that Job can still
remain in the database, and if you do a \bcommandname{list volumes}, you will see the
volume information, but the Job records (and its File records) will no longer
be available.

In each case, pruning removes information about where older files are, but it
also prevents the catalog from growing to be too large. You choose the
retention periods in function of how many files you are backing up and the
time periods you want to keep those records online, and the size of the
database. You can always re-insert the records (with 98\% of the original data)
by using \btool{bscan} to scan in a whole Volume or any part of the volume that
you want.

By setting \bdirectivename{AutoPrune} to \bvalue{yes} you will permit \bacula{} to
automatically prune Volumes in the Pool when a Job needs a Volume.
Volume pruning means removing Jobs and Files records from the catalog.
It does not shrink the size of the Volume or affect the Volume data until
the Volume gets overwritten. When a Job requests a volume and there are
no Volumes with Volume Status \bstatus{Append}, \bstatus{Recycle} or \bstatus{Purged}
available, \bacula{} will begin volume pruning. This means that all Jobs
that are older than the \bdirectivename{VolumeRetention} period will be pruned from
every Volume that has Volume Status \bstatus{Full} or \bstatus{Used} and has
Recycle set to \bvalue{yes}. Pruning consists of deleting the corresponding
Job, File, and JobMedia records from the catalog database. No
change to the physical data on the Volume occurs during the pruning process.
When all files are pruned from a Volume (i.e. no records in the catalog), the
Volume will be marked as \bstatus{Purged} implying that no Jobs remain on the
volume. The Pool records that control the pruning are described below.

\begin{bdescription}

\item [AutoPrune = \byesorno{}]
   \index[console]{AutoPrune  }
   If AutoPrune is set to  \bdefaultvalue{yes} (default), \bacula{}
   will  automatically apply the Volume retention period when running a Job  and
   it needs a new Volume but no appendable volumes are available.  At that point,
   \bacula{} will prune all Volumes that can be pruned  (i.e. AutoPrune set) in an
   attempt to find a usable volume. If  during the autoprune, all files are
   pruned from the Volume, it  will be marked with VolStatus \bstatus{Purged}.  The
   default is \bdefaultvalue{yes}. Note, that although the File and Job records may be
   pruned from the catalog, a Volume will be marked Purged (and hence
   ready for recycling) if the Volume status is Append, Full, Used, or Error.
   If the Volume has another status, such as Archive, Read-Only, Disabled,
   Busy, or Cleaning, the Volume status will not be changed to Purged.

\item [Volume Retention = \bbracket{time-period-specification}]
   \index[console]{Volume Retention}
   The Volume Retention record defines the length of time that \bacula{} will
   guarantee that the Volume is not reused (i.e. recycled). As long as
   a Volume has the status {\bf Append}, it will not be recycled even
   though the retention period may have expired, until the status
   is changed to some other status such as Full, Used, Purged, ...

\smallskip
   The retention period when applied starts at the time of the last 
   write to the Volume. For a Volume to recycled, the volume must not
   have a status of {\bf Append} and the retention period must have
   expired.  Even in that case, the Volume will not be recycled if 
   any other appendable Volume is available for the Job to complete.
   As noted above, the Volume status will be marked {\bf Purged} by
   Bacula prior to it being recycled.

   Note, when all the Job records that are on the Volume have been removed,
   the Volume will be marked \bstatus{Purged} (i.e. it has no more valid
   Jobs stored on it), and the Volume may be recycled even if the \bdirectivename{Volume Retention}
   period has not expired.

   When this time period expires, and if \bdirectivename{AutoPrune} is set to \bvalue{yes},
   and a new Volume is needed, but no appendable Volume is available,
   \bacula{} will prune (remove) Job records that are older than the specified
   Volume Retention period even if the Job or File retention has not been
   reached. Normally this should not happen since the Volume Retention 
   period should always be set greater than the Job Retention period, 
   which should be greater than the File Retention period.

   The Volume Retention period takes precedence over any Job Retention
   period you have specified in the Client resource.  It should also be
   noted, that the Volume Retention period is obtained by reading the
   Catalog Database Media record rather than the Pool resource record.
   This means that if you change the VolumeRetention in the Pool resource
   record (in bacula-dir.conf), you must ensure that the corresponding
   change is made in the catalog by using the \bcommandname{update pool} command.
   Doing so will insure that any new Volumes will be created with the
   changed Volume Retention period.  Any existing Volumes will have their
   own copy of the Volume Retention period that can only be changed on a
   Volume by Volume basis using the \bcommandname{update volume} command.

   When all Job catalog entries are removed from the volume,  its VolStatus is
   set to \bstatus{Purged}. The files remain physically  on the Volume until the
   volume is overwritten.

   Retention periods are specified in seconds,  minutes, hours, days, weeks,
   months,  quarters, or years on the record. See the
   \bilink{Configuration chapter}{blb:Time} of this  manual for
   additional details of time specification.

   The default Volume Retention period is 1 year. 

% TODO: if that is the format, should it be in quotes? decide on a style

\item [Recycle = \byesorno{}]
   \index[fd]{Recycle  }
   This statement tells \bacula{} whether or not the particular Volume can be
   recycled (i.e.  rewritten).  If Recycle is set to \bdefaultvalue{no} (the
   default), then even if \bacula{} prunes all the Jobs on the volume and it
   is marked \bstatus{Purged}, it will not consider the volume for recycling.  If
   Recycle is set to \bvalue{yes} and all Jobs have been pruned, the volume
   status will be set to \bstatus{Purged} and the volume may then be reused
   when another volume is needed.  If the volume is reused, it is relabeled
   with the same Volume Name, however all previous data will be lost.
   \end{bdescription}

   It is also possible to ``force'' pruning of all Volumes in the Pool
   associated with a Job by adding \bvalueddirective{Prune Files}{yes} to the Job resource.

\label{blb:Recycling}
\label{blb:RecyclingAlgorithm}
\section{Recycling Algorithm}
\index[general]{Algorithm!Recycling }
\index[general]{Recycling Algorithm }

When a Job needs a Volume and no appendable, recycled or purged is available,
\bacula{} starts the recycling algorithm pruning Jobs and Files for the oldest
Volume in the pool used by the job. After the Volume is pruned, and if
the \bdirectivename{Recycle} flag is on (\bvalueddirective{Recycle}{yes}) for that Volume, \bacula{} will
relabel it and write new data on it.

As mentioned above, there are two key points for getting a Volume
to be recycled. First, the Volume must no longer be marked Append (there
are a number of directives to automatically make this change), and second
since the last write on the Volume, one or more of the Retention periods
must have expired so that there are no more catalog backup job records
that reference that Volume.  Once both those conditions are satisfied,
the volume can be marked Purged and hence recycled.

The full algorithm that \bacula{} uses when it needs a new Volume is:
\index[general]{New Volume Algorithm}
\index[general]{Algorithm!New Volume}

The algorithm described below assumes that AutoPrune is enabled,
that Recycling is turned on, and that you have defined
appropriate Retention periods, or used the defaults for all these
items.

\begin{bitemize}
\item If the request is for an Autochanger device, look only
   for Volumes in the Autochanger (i.e. with InChanger set and that have
   the correct Storage device).
\item Search the Pool for a Volume with VolStatus=Append (if there is more
   than one, the Volume with the oldest date last written is chosen.  If
   two have the same date then the one with the lowest MediaId is chosen).
\item Search the Pool for a Volume with VolStatus=Recycle and the InChanger
   flag is set true (if there is more than one, the Volume with the oldest
   date last written is chosen.  If two have the same date then the one
   with the lowest MediaId is chosen).
\item Search the Pool for a Volume with VolStatus=Purged and try recycling any purged Volumes.
\item Prune volumes applying Volume retention period (Volumes with VolStatus
  \bstatus{Full} or \bstatus{Used} are pruned). Note, when all the File and Job
   records are pruned from a Volume, the Volume will be marked \bstatus{Purged} and
   this may be prior to the expiration of the Volume retention period.
 \item Prune the oldest Volume if \bvalueddirective{RecycleOldestVolume}{yes} (the Volume with the
   oldest LastWritten date and VolStatus equal to  \bstatus{Full} or \bstatus{Used} is chosen).
   This record ensures that all retention periods are properly respected.
\item Purge the oldest Volume if PurgeOldestVolume=yes (the Volume  with the
   oldest LastWritten date and VolStatus equal to  \bstatus{Full} or \bstatus{Used} is chosen).
   We strongly  recommend against the use of \bdirectivename{PurgeOldestVolume} as
   it can quite easily lead to loss of current backup data.
\item Search for a Volume in the Scratch Pool (the default Scratch pool
   or another Scratch Pool defined for the pool) and if found
   move it to the current Pool for the Job and use it. Note, when
   the Scratch Volume is moved into the current Pool, the basic
   Pool defaults are applied as if it is a newly labeled Volume
   (equivalent to an \bcommandname{update volume from pool} command).
\item If we were looking for Volumes in the Autochanger, go back to step 2 above, but this time, look for any Volume whether or not it is in the Autochanger.
\item Attempt to create a new Volume if automatic labeling is enabled and the maximum number of Volumes is not reached.
\item Give up and ask operator.
\end{bitemize}

The above occurs when \bacula{} has finished writing a Volume or when no Volume
is present in the drive.

On the other hand, if you have inserted a different Volume after the last
job, and \bacula{} recognizes the Volume as valid, the Storage daemon will
request authorization from the Director to use this Volume.  In this case,
if you have set \bvalueddirective{Recycle Current Volume = yes} and the Volume is marked
as Used or Full, Bacula will prune the volume and if all jobs were removed
during the pruning (respecting the retention periods), the Volume will be
recycled and re-used.

\smallskip

If you want to strictly observe the \bdirectivename{Volume Retention Period} under all
circumstances -- i.e. you have a Volume with critical data, you must set
\bvalueddirective{Recycle}{no}. However, in doing so, you must manually do the
recycling of the Volume for it to be used again.

The recycling algorithm in this case is:
\begin{bitemize}
\item If the VolStatus is \bstatus{Append} or \bstatus{Recycle}
   is set, the volume  will be used.
 \item If \bdirectivename{Recycle Current Volume} is set and the  volume is marked \bstatus{Full}
   or \bstatus{Used}, \bacula{}  will prune the volume (applying the retention
   period).  If all Jobs are pruned from the volume, it will be  recycled.
\end{bitemize}

This permits users to manually change the Volume every day and load volumes in
an order different from what is in the catalog, and if the volume does not
contain a current copy of your backup data, it will be used.

A few points to keep in mind:

\begin{benumerate}
\item If a pool doesn't have maximum volumes defined then \bacula{} will prefer to
  demand new volumes over forcibly purging older volumes.

\item If volumes become free through pruning, then they get marked as
  \bstatus{Purged} and are immediately available for recycling -- these will
  be used in preference to creating new volumes.

\item If the Job, File, and Volume retention periods are different, then
  it's common to see a volume with no files or jobs listed in the database,
  but which is still not marked as \bstatus{Purged}, if \bdirectivename{Autoprune} is set to \bvalue{no}
  in the pool resource.
\end{benumerate}


\section{Recycle Status}
\index[general]{Status!Recycle }
\index[general]{Recycle Status }

Each Volume inherits the Recycle status (yes or no) from the Pool resource
record when the Media record is created (normally when the Volume is
labeled).  This Recycle status is stored in the Media record of the
Catalog.  Using the Console program, you may subsequently change the
Recycle status for each Volume.
For example in the following output from \bcommandname{list volumes}: 

\begin{bVerbatim}
+----------+-------+--------+---------+------------+--------+-----+
| VolumeNa | Media | VolSta | VolByte | LastWritte | VolRet | Rec |
+----------+-------+--------+---------+------------+--------+-----+
| File0001 | File  | Full   | 4190055 | 2002-05-25 | 14400  | 1   |
| File0002 | File  | Full   | 1896460 | 2002-05-26 | 14400  | 1   |
| File0003 | File  | Full   | 1896460 | 2002-05-26 | 14400  | 1   |
| File0004 | File  | Full   | 1896460 | 2002-05-26 | 14400  | 1   |
| File0005 | File  | Full   | 1896460 | 2002-05-26 | 14400  | 1   |
| File0006 | File  | Full   | 1896460 | 2002-05-26 | 14400  | 1   |
| File0007 | File  | Purged | 1896466 | 2002-05-26 | 14400  | 1   |
+----------+-------+--------+---------+------------+--------+-----+
\end{bVerbatim}

all the volumes are marked as recyclable, and the last Volume,
\bvalue{File0007} has been purged, so it may be immediately recycled.  The other
volumes are all marked recyclable and when their Volume Retention period
(14400 seconds or four hours) expires, they will be eligible for pruning,
and possibly recycling.  Even though Volume \bvalue{File0007} has been purged,
all the data on the Volume is still recoverable.  A purged Volume simply
means that there are no entries in the Catalog.  Even if the Volume Status
is changed to \bstatus{Recycle}, the data on the Volume will be recoverable.
The data is lost only when the Volume is re-labeled and re-written.

To modify Volume \bvalue{File0001} so that it cannot be recycled, you use the
\bcommandname{update volume pool=File} command in the console program, or simply
\bcommandname{update} and \bacula{} will prompt you for the information. 

\begin{bVerbatim}
+----------+------+-------+---------+-------------+-------+-----+
| VolumeNa | Media| VolSta| VolByte | LastWritten | VolRet| Rec |
+----------+------+-------+---------+-------------+-------+-----+
| File0001 | File | Full  | 4190055 | 2002-05-25  | 14400 | 0   |
| File0002 | File | Full  | 1897236 | 2002-05-26  | 14400 | 1   |
| File0003 | File | Full  | 1896460 | 2002-05-26  | 14400 | 1   |
| File0004 | File | Full  | 1896460 | 2002-05-26  | 14400 | 1   |
| File0005 | File | Full  | 1896460 | 2002-05-26  | 14400 | 1   |
| File0006 | File | Full  | 1896460 | 2002-05-26  | 14400 | 1   |
| File0007 | File | Purged| 1896466 | 2002-05-26  | 14400 | 1   |
+----------+------+-------+---------+-------------+-------+-----+
\end{bVerbatim}

In this case, \textbf{File0001} will never be automatically recycled. The same
effect can be achieved by setting the Volume Status to Read-Only.

As you have noted, the Volume Status (VolStatus) column in the
catalog database contains the current status of the Volume, which
is normally maintained automatically by \bacula{}. To give you an
idea of some of the values it can take during the life cycle of
a Volume, here is a picture of the process.

\begin{bVerbatim}
A typical volume life cycle is like this:

              because job count or size limit exceeded
      Append  ------------------------------------>  Used/Full
        ^                                                  |
        | First Job writes to        Retention time passed |
        | the volume                   and recycling takes |
        |                                            place |
        |                                                  v
      Recycled <-------------------------------------- Purged
                     Volume is selected for reuse
\end{bVerbatim}


\section{Making \bacula{} Use a Single Tape}
\label{blb:singletape}
\index[general]{Tape!Making \bacula{} Use a Single}
\index[general]{Making \bacula{} Use a Single Tape}

Most people will want \bacula{} to fill a tape and when it is full, a new tape
will be mounted, and so on.  However, as an extreme example, it is possible
for \bacula{} to write on a single tape, and every night to rewrite it.  To
get this to work, you must do two things: first, set the \bdirectivename{VolumeRetention} to
less than your save period (one day), and the second item is to make \bacula{}
mark the tape as full after using it once.  This is done using
\bvalueddirective{UseVolumeOnce}{yes}.  If this latter record is not used and the tape is
not full after the first time it is written, \bacula{} will simply append to
the tape and eventually request another volume.  Using the tape only once,
forces the tape to be marked
\bstatus{Full} after each use, and the next time \bacula{} runs, it will
recycle the tape.

An example Pool resource that does this is:

\begin{bVerbatim}
Pool {
  Name = DDS-4
  Use Volume Once = yes
  Pool Type = Backup
  AutoPrune = yes
  VolumeRetention = 12h # expire after 12 hours
  Recycle = yes
}
\end{bVerbatim}

\section{Daily, Weekly, Monthly Tape Usage Example}
\label{blb:usageexample}
\index[general]{Daily, Weekly, Monthly Tape Usage Example }
\index[general]{Example!Daily Weekly Monthly Tape Usage }

This example is meant to show you how one could define a fixed set of volumes
that \bacula{} will rotate through on a regular schedule. There are an infinite
number of such schemes, all of which have various advantages and
disadvantages. Note: these volumes may either be tape volumes or disk
volumes.

We start with the following assumptions:

\begin{bitemize}
\item A single tape has more than enough capacity to do  a full save.
\item There are ten tapes that are used on a daily basis  for incremental
   backups. They are prelabeled Daily1 ...  Daily10.
\item There are four tapes that are used on a weekly basis  for full backups.
   They are labeled Week1 ... Week4.
\item There are 12 tapes that are used on a monthly basis  for full backups.
   They are numbered Month1 ... Month12
\item A full backup is done every Saturday evening (tape inserted  Friday
   evening before leaving work).
\item No backups are done over the weekend (this is easy to  change).
\item The first Friday of each month, a Monthly tape is used for  the Full
   backup.
\item Incremental backups are done Monday - Friday (actually  Tue-Fri
   mornings).
% TODO: why this ``actually''? does this need to be explained?
   \end{bitemize}

We start the system by doing a Full save to one of the weekly volumes or one
of the monthly volumes. The next morning, we remove the tape and insert a
Daily tape. Friday evening, we remove the Daily tape and insert the next tape
in the Weekly series. Monday, we remove the Weekly tape and re-insert the
Daily tape. On the first Friday of the next month, we insert the next Monthly
tape in the series rather than a Weekly tape, then continue. When a Daily tape
finally fills up, \bacula{} will request the next one in the series, and
the next day when you notice the email message, you will mount it and
\bacula{} will finish the unfinished incremental backup.

What does this give? Well, at any point, you will have the last complete
Full save plus several Incremental saves. For any given file you want to
recover (or your whole system), you will have a copy of that file every day
for at least the last 14 days. For older versions, you will have at least three
and probably four Friday full saves of that file, and going back further, you
will have a copy of that file made on the beginning of the month for at least
a year.

So you have copies of any file (or your whole system) for at least a year, but
as you go back in time, the time between copies increases from daily to weekly
to monthly.

What would the \bacula{} configuration look like to implement such a scheme?

\begin{bVerbatim}
Schedule {
  Name = "NightlySave"
  Run = Level=Full Pool=Monthly 1st sat at 03:05
  Run = Level=Full Pool=Weekly 2nd-5th sat at 03:05
  Run = Level=Incremental Pool=Daily tue-fri at 03:05
}
Job {
  Name = "NightlySave"
  Type = Backup
  Level = Full
  Client = LocalMachine
  FileSet = "File Set"
  Messages = Standard
  Storage = DDS-4
  Pool = Daily
  Schedule = "NightlySave"
}
# Definition of file storage device
Storage {
  Name = DDS-4
  Address = localhost
  SDPort = 9103
  Password = XXXXXXXXXXXXX
  Device = FileStorage
  Media Type = 8mm
}
FileSet {
  Name = "File Set"
  Include {
    Options { signature=MD5 }
    File = fffffffffffffffff
  }
  Exclude  { File=*.o }
}
Pool {
  Name = Daily
  Pool Type = Backup
  AutoPrune = yes
  VolumeRetention = 10d   # recycle in 10 days
  Maximum Volumes = 10
  Recycle = yes
}
Pool {
  Name = Weekly
  Use Volume Once = yes
  Pool Type = Backup
  AutoPrune = yes
  VolumeRetention = 30d  # recycle in 30 days (default)
  Recycle = yes
}
Pool {
  Name = Monthly
  Use Volume Once = yes
  Pool Type = Backup
  AutoPrune = yes
  VolumeRetention = 365d  # recycle in 1 year
  Recycle = yes
}
\end{bVerbatim}

\section{ Automatic Pruning and Recycling Example}
\label{blb:PruningExample}
\index[general]{Automatic Pruning and Recycling Example }
\index[general]{Example!Automatic Pruning and Recycling }

Perhaps the best way to understand the various resource records that come
into play during automatic pruning and recycling is to run a Job that goes
through the whole cycle.  If you add the following resources to your
Director's configuration file:

\begin{bVerbatim}
Schedule {
  Name = "30 minute cycle"
  Run = Level=Full Pool=File Messages=Standard Storage=File
         hourly at 0:05
  Run = Level=Full Pool=File Messages=Standard Storage=File
         hourly at 0:35
}
Job {
  Name = "Filetest"
  Type = Backup
  Level = Full
  Client=XXXXXXXXXX
  FileSet="Test Files"
  Messages = Standard
  Storage = File
  Pool = File
  Schedule = "30 minute cycle"
}
# Definition of file storage device
Storage {
  Name = File
  Address = XXXXXXXXXXX
  SDPort = 9103
  Password = XXXXXXXXXXXXX
  Device = FileStorage
  Media Type = File
}
FileSet {
  Name = "File Set"
  Include {
    Options { signature=MD5 }
    File = fffffffffffffffff
  }
  Exclude  { File=*.o }
}
Pool {
  Name = File
  Use Volume Once = yes
  Pool Type = Backup
  LabelFormat = "File"
  AutoPrune = yes
  VolumeRetention = 4h
  Maximum Volumes = 12
  Recycle = yes
}
\end{bVerbatim}

Where you will need to replace the \textbf{ffffffffff}'s by the appropriate files
to be saved for your configuration. For the FileSet Include, choose a
directory that has one or two megabytes maximum since there will probably be
approximately eight copies of the directory that \bacula{} will cycle through.

In addition, you will need to add the following to your Storage daemon's
configuration file:

\begin{bVerbatim}
Device {
  Name = FileStorage
  Media Type = File
  Archive Device = /tmp
  LabelMedia = yes;
  Random Access = Yes;
  AutomaticMount = yes;
  RemovableMedia = no;
  AlwaysOpen = no;
}
\end{bVerbatim}

With the above resources, \bacula{} will start a Job every half hour that saves a
copy of the directory you chose to /tmp/File0001 ... /tmp/File0012. After 4
hours, \bacula{} will start recycling the backup Volumes (/tmp/File0001 ...). You
should see this happening in the output produced. \bacula{} will automatically
create the Volumes (Files) the first time it uses them.

To turn it off, either delete all the resources you've added, or simply
comment out the \bresourcename{Schedule} record in the \bresourcename{Job} resource.

\section{Manually Recycling Volumes}
\label{blb:manualrecycling}
\index[general]{Volumes!Manually Recycling }
\index[general]{Manually Recycling Volumes }

Although automatic recycling of Volumes is implemented in version 1.20 and
later (see the
\bilink{Automatic Recycling of Volumes}{blb:RecyclingChapter} chapter of
this manual), you may want to manually force reuse (recycling) of a Volume.

Assuming that you want to keep the Volume name, but you simply want to write
new data on the tape, the steps to take are:

\begin{bitemize}
\item Use the \bcommandname{update volume} command in the Console to  ensure that the
   \bstatus{Recycle} field is set to \bvalue{1}  
\item Use the \bcommandname{purge jobs volume} command in the Console  to mark the
   Volume as \bstatus{Purged}. Check by using  \bcommandname{list volumes}. 
\end{bitemize}

Once the Volume is marked Purged, it will be recycled the next time a Volume
is needed.

If you wish to reuse the volume by giving it a new name, follow the following
steps:

\begin{bitemize}
\item Use the \bcommandname{purge jobs volume} command in the Console  to mark the
   Volume as \bstatus{Purged}. Check by using  \bcommandname{list volumes}.  
\item Use the Console \bcommandname{relabel}
   command to relabel the Volume. 
\end{bitemize}

Please note that the \bcommandname{relabel} command applies only to tape Volumes.

For \bacula{} versions prior to 1.30 or to manually relabel the Volume, use the
instructions below:

\begin{bitemize}
\item Use the \bcommandname{delete volume} command in the Console  to delete the Volume
   from the Catalog.  
\item If a different tape is mounted, use the \bcommandname{unmount}  command,
   remove the tape, and insert the tape to be  renamed.  
\item Write an EOF mark in the tape using the following  commands: 

\begin{bVerbatim}
mt -f /dev/nst0 rewind
mt -f /dev/nst0 weof
\end{bVerbatim}

where you replace \bfilename{/dev/nst0} with the appropriate  device name on your
system.  
\item Use the \bcommandname{label} command to write a new label to  the tape and to
   enter it in the catalog. 
\end{bitemize}

Please be aware that the \bcommandname{delete} command can be dangerous. Once it is
done, to recover the File records, you must either restore your database as it
was before the \bcommandname{delete} command, or use the \btool{bscan} utility program to
scan the tape and recreate the database entries. 
