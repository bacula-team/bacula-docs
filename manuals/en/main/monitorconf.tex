\chapter{Monitor Configuration}
\label{blb:MonitorChapter}
\index[general]{Monitor Configuration}
\index[general]{Configuration!Monitor}

The Monitor configuration file is a stripped down version of the Director
configuration file, mixed with a Console configuration file. It simply
contains the information necessary to contact Directors, Clients, and Storage
daemons you want to monitor.

For a general discussion of configuration file and resources including the
data types recognized by \textbf{\bacula{}}, please see the
\bilink{Configuration}{blb:ConfigureChapter} chapter of this manual.

The following Monitor Resource definition must be defined:

\begin{bitemize}
\item
   \bilink{Monitor}{blb:MonitorResource} -- to  define the Monitor's
   name used to connect to all the daemons and  the password used to connect to
the Directors. Note, you must not  define more than one Monitor resource in
the  Monitor configuration file.
\item At least one
   \bilink{Client}{blb:ClientResource1},
   \bilink{Storage}{blb:StorageResource1} or
\bilink{Director}{blb:DirectorResource2} resource, to define the
daemons to monitor.
\end{bitemize}

\section{The Monitor Resource}
\label{blb:MonitorResource}
\index[general]{Monitor Resource}
\index[general]{Resource!Monitor}

The Monitor resource defines the attributes of the Monitor running on the
network. The parameters you define here must be configured as a Director
resource in Clients and Storages configuration files, and as a Console
resource in Directors configuration files.

\begin{bdescription}

\item [Monitor]
   \index[fd]{Monitor}
   Start of the Monitor records.

\item [Name = \bbracket{name}]
   \index[fd]{Name}
   Specify the Director name used to connect  to Client and Storage, and the
Console name used to connect to Director.  This record is required.

\item [DisplayAdvancedOptions = \bbracket{boolean}]
  Display advanced options in the tray monitor (for backup and restore operations)

\item [CommandDirectory = \bbracket{directory}]
  Directory where the tray monitor will look at a regular interval to find
  commands to execute.
  
\item [Refresh Interval = \bbracket{time}]
   \index[fd]{Refresh Interval}
   Specifies the time to wait  between status requests to each daemon. It can't
be set to less than  1 second, or more than 10 minutes, and the default value
is \bdefaultvalue{5 seconds}.
% TODO: what is format of the time?
% TODO: should the digits in this  definition be spelled out? should
% TODO: this say "time-period-specification" above??)
\end{bdescription}

\section{The Director Resource}
\label{blb:DirectorResource2}
\index[general]{Director Resource}
\index[general]{Resource!Director}

The Director resource defines the attributes of the Directors that are
monitored by this Monitor.

As you are not permitted to define a Password in this resource, to avoid
obtaining full Director privileges, you must create a Console resource in the
\bilink{Director's configuration}{blb:DirectorChapter} file, using the
Console Name and Password defined in the Monitor resource. To avoid security
problems, you should configure this Console resource to allow access to no
other daemons, and permit the use of only two commands: \bcommandname{status} and
\bcommandname{.status} (see below for an example).

You may have multiple Director resource specifications in a single Monitor
configuration file.

\begin{bdescription}

\item [Director]
   \index[fd]{Director}
   Start of the Director records.

\item [Name = \bbracket{name}]
   \index[fd]{Name}
   The Director name used to identify  the Director in the list of monitored
daemons. It is not required  to be the same as the one defined in the Director's
configuration file.  This record is required.

\item [Port = \bbracket{port-number}]
   \index[fd]{Port}
   Specify the port to use to connect  to the Director. This value will most
   likely already be set to the value  you specified on the \textbf{{-}{-}:with-baseport}
   option of the  \btool{./configure} command. This port must be
identical to the  \textbf{DIRport} specified in the \textbf{Director} resource of
the
\bilink{Director's configuration}{blb:DirectorChapter} file.  The
default is \bdefaultvalue{9101} so this record is not normally specified.

\item [Address = \bbracket{address}]
   \index[fd]{Address}
   Where the address is a host name,  a fully qualified domain name, or a network
address used to connect  to the Director.  This record is required.
\end{bdescription}

\section{The Client Resource}
\label{blb:ClientResource1}
\index[general]{Resource!Client}
\index[general]{Client Resource}

The Client resource defines the attributes of the Clients that are monitored
by this Monitor.

You must create a Director resource in the
\bilink{Client's configuration}{blb:FiledConfChapter} file, using the
Director Name defined in the Monitor resource. To avoid security problems, you
should set the \textbf{Monitor} directive to \textbf{Yes} in this Director resource.


You may have multiple Director resource specifications in a single Monitor
configuration file.

\begin{bdescription}

\item [Client (or FileDaemon)]
   \index[fd]{Client (or FileDaemon)}
   Start of the Client records.

\item [Name = \bbracket{name}]
   \index[fd]{Name}
   The Client name used to identify  the Director in the list of monitored
daemons. It is not required  to be the same as the one defined in the Client's
configuration file.  This record is required.

\item [Address = \bbracket{address}]
   \index[fd]{Address}
   Where the address is a host  name, a fully qualified domain name, or a network
address in  dotted quad notation for a \bacula{} File daemon.  This record is
required.

\item [Port = \bbracket{port-number}]
   \index[fd]{Port}
   Where the port is a port  number at which the \bacula{} File daemon can be
contacted.  The default is \bdefaultvalue{9102}.

\item [Password = \bbracket{password}]
   \index[fd]{Password}
   This is the password to be  used when establishing a connection with the File
services, so  the Client configuration file on the machine to be backed up
must  have the same password defined for this Director. This record is
required.
\end{bdescription}

\section{The Storage Resource}
\label{blb:StorageResource1}
\index[general]{Resource!Storage}
\index[general]{Storage Resource}

The Storage resource defines the attributes of the Storages that are monitored
by this Monitor.

You must create a Director resource in the
\bilink{Storage's configuration}{blb:StoredConfChapter} file, using the
Director Name defined in the Monitor resource. To avoid security problems, you
should set the \textbf{Monitor} directive to \textbf{Yes} in this Director resource.


You may have multiple Director resource specifications in a single Monitor
configuration file.

\begin{bdescription}

\item [Storage]
   \index[fd]{Storage}
   Start of the Storage records.

\item [Name = \bbracket{name}]
   \index[fd]{Name}
   The Storage name used to identify  the Director in the list of monitored
daemons. It is not required  to be the same as the one defined in the Storage's
configuration file.  This record is required.

\item [Address = \bbracket{address}]
   \index[fd]{Address}
   Where the address is a host  name, a fully qualified domain name, or a network
address in  dotted quad notation for a \bacula{} Storage daemon.  This record is
required.

\item [Port = \bbracket{port}]
   \index[fd]{Port}
   Where port is the port to use to  contact the storage daemon for information
and to start jobs.  This same port number must appear in the Storage resource
of the  Storage daemon's configuration file. The default is \bdefaultvalue{9103}.

\item [Password = \bbracket{password}]
   \index[sd]{Password}
   This is the password to be used  when establishing a connection with the
Storage services. This  same password also must appear in the Director
resource of the Storage  daemon's configuration file. This record is required.

\end{bdescription}

\section{Tray Monitor Security}
\index[general]{Tray Monitor Security}

There is no security problem in relaxing the permissions on
\bfilename{tray-monitor.conf} as long as FD, SD and DIR are configured properly, so
the passwords contained in this file only gives access to the status of
the daemons. It could be a security problem if you consider the status
information as potentially dangerous (I don't think it is the case).

Concerning Director's configuration: \\
In \bfilename{tray-monitor.conf}, the password in the Monitor resource must point to
a restricted console in \bfilename{bacula-dir.conf} (see the documentation). So, if
you use this password with bconsole, you'll only have access to the
status of the director (commands \bcommandname{status} and \bcommandname{.status}).
It could be a security problem if there is a bug in the \acs{ACL} code of the
director.

Concerning File and Storage Daemons' configuration:\\
In \bfilename{tray-monitor.conf}, the Name in the Monitor resource must point to a
Director resource in \bfilename{bacula-fd}/\bfilename{sd.conf}, with the Monitor directive set
to \bvalue{yes} (once again, see the documentation).
It could be a security problem if there is a bug in the code which check
if a command is valid for a Monitor (this is very unlikely as the code
is pretty simple).


\section{Sample Tray Monitor configuration}
\label{blb:SampleConfiguration1}
\index[general]{Sample Tray Monitor configuration}

An example Tray Monitor configuration file might be the following:

\begin{bVerbatim}
#
# Bacula Tray Monitor Configuration File
#
Monitor {
  Name = rufus-mon        # password for Directors
  RefreshInterval = 10 seconds
}

Client {
  Name = rufus-fd
  Address = rufus
  Port = 9102           # password for FileDaemon
  Password = "FYpq4yyI1y562EMS35bA0J0QC0M2L3t5cZObxT3XQxgxppTn"
}
Storage {
  Name = rufus-sd
  Address = rufus
  Port = 9103           # password for StorageDaemon
  Password = "9usxgc307dMbe7jbD16v0PXlhD64UVasIDD0DH2WAujcDsc6"
}
Director {
  Name = rufus-dir
  port = 9101
  address = rufus
}
\end{bVerbatim}

\subsection{Sample File daemon's Director record.}
\index[general]{Sample File daemon's Director record.}
\index[general]{Record!Sample File daemon's Director}

Click
\bilink{here to see the full example.}{blb:SampleClientConfiguration}


\begin{bVerbatim}
#
# Restricted Director, used by tray-monitor to get the
#   status of the file daemon
#
Director {
  Name = rufus-mon
  Password = "FYpq4yyI1y562EMS35bA0J0QC0M2L3t5cZObxT3XQxgxppTn"
  Monitor = yes
}
\end{bVerbatim}

\subsection{Sample Storage daemon's Director record.}
\index[general]{Record!Sample Storage daemon's Director}
\index[general]{Sample Storage daemon's Director record.}

Click
\bilink{here to see the full example.}{blb:SampleConfiguration}

\begin{bVerbatim}
#
# Restricted Director, used by tray-monitor to get the
#   status of the storage daemon
#
Director {
  Name = rufus-mon
  Password = "9usxgc307dMbe7jbD16v0PXlhD64UVasIDD0DH2WAujcDsc6"
  Monitor = yes
}
\end{bVerbatim}

\subsection{Sample Director's Console record.}
\index[general]{Record!Sample Director's Console}
\index[general]{Sample Director's Console record.}

Click
\bilink{here to see the full example.}{blb:SampleDirectorConfiguration}

\begin{bVerbatim}
#
# Restricted console used by tray-monitor to get the status of the director
#
Console {
  Name = Monitor
  Password = "GN0uRo7PTUmlMbqrJ2Gr1p0fk0HQJTxwnFyE4WSST3MWZseR"
  CommandACL = status, .status
}
\end{bVerbatim}
