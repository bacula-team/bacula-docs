#!/bin/bash
#
# Philippe Chauvat - BaculaSystems
# Last modification 04-dec-2012
#
# This script fetch all HTML 'manual' directories
# to apply a conversion script (translatedoc.pl)
# and then produce the final HTML version of the manuals
#
# args: none
# 
# vars
# SOURCEDIRHEAD:        Where are located the original directories relatively to the
#                       current script
#                       Default: ../manuals/en
#
# TRANSLATOR:           The script to apply to each HTML original file
#                       Default: ./translatedoc.pl
#
# DEBUG:                You want some ? Specify -d there
#                       Default: ""
#
# FROMFILEMENUEXTRACT:  We want to extract ako menu. This var give the name of the file
#                       from which to extract it
#                       Default: index.html
#
# DIRSTOCREATE:         Specify here which directory(ies) must be created before running the conversion
#                       Default: "css js images"
#
# LIST                  The list of dirs containing the manuals to translate
#                       Default: `find ${SOURCEDIRHEAD} -mindepth 1 -maxdepth 1 -name "www-*" -type d`
#
# ROOTDIR               All HTML files rely on CSS, JS and IMAGES. ROOTDIR specify the relative path to those.
#                       Default: "../.."
#
# CSSDIR                The CSS directory used by HTMLs
#                       Default: ${ROOTDIR}/css
#
# JSDIR                 The JS (javascript) directory used by HTMLs
#                       Default: ${ROOTDIR}/js
#
# IMAGEDIR              The images directory used by HTMLs
#                       Default: ${ROOTDIR}/images
#
# DOSUBFILES            Do we want to generate all subfiles (not only the index.html). Debug and tests purpose
#                       Default: yes
#
SOURCEDIRHEAD="../manuals/en"
TRANSLATOR='./translatedoc.pl'
DEBUG="" # change to -d if you want some debug there
FROMFILEMENUEXTRACT="index.html"
DIRSTOCREATE="css js images"
LIST=`find ${SOURCEDIRHEAD} -mindepth 1 -maxdepth 1 -name "www-*" -type d`
#LIST=${SOURCEDIRHEAD}/www-main
ROOTDIR="../.."
CSSDIR=${ROOTDIR}/css
JSDIR=${ROOTDIR}/js
IMAGEDIR=${ROOTDIR}/images
SOURCEIMAGEDIR=../images
DESTIMAGEDIR=${SOURCEDIRHEAD}/images
#
# Do we want to generate HTML files for all
# files
DOSUBFILES=yes
#
# For each manual
for M in ${LIST}
do
    #
    # Extract the directory name: console, developers, main, etc.
    thedirname=`echo $M | sed -e 's/.*www-\(.*\)/\1/g'`
    #
    # Message to indicate what we are building
    echo ""
    echo ""
    echo "$thedirname Manual"
    #
    # Where to find HTML files
    readdir=$M/$thedirname
    #
    # Where to store the result
    DESTINATION_DIR=$SOURCEDIRHEAD/$thedirname/$thedirname
    #
    # Create the desitnation directory if needed
    mkdir -p $DESTINATION_DIR
    #
    # Create otherdirs if needed
    for D in ${DIRSTOCREATE}
    do
	mkdir -p $SOURCEDIRHEAD/$D
    done
    #
    # Building the menu must be done without any existing file
    rm -f wholemenu_${thedirname}.html
    echo -n "Building navigation menu from ${readdir}/${FROMFILEMENUEXTRACT} to ${DESTINATION_DIR}/${FROMFILEMENUEXTRACT}..."
    ./translatedoc.pl ${DEBUG} -i ${readdir}/${FROMFILEMENUEXTRACT} -e -o ${DESTINATION_DIR}/${FROMFILEMENUEXTRACT} -j ${JSDIR} -c ${CSSDIR} -p ${IMAGEDIR} -n ${thedirname} -r ${SOURCEDIRHEAD}
    echo "Done."
    if [ ${DOSUBFILES} == "yes" ]
    then
	for L in `ls ${readdir}|egrep html$`
	do
	    echo -n "Translating $L..."
	    ./translatedoc.pl ${DEBUG} -i ${readdir}/${L} -o ${DESTINATION_DIR}/${L} -j ${JSDIR} -c ${CSSDIR} -p ${IMAGEDIR} -n ${thedirname}
	    echo "Done."
	done
    fi
done
#
# Anchor management
readdir=""
for M in ${LIST}
do
    #
    # Extract the directory name: console, developers, main, etc.
    thedirname=`echo $M | sed -e 's/.*www-\(.*\)/\1/g'`
    #
    # Where to find HTML files
    readdir="${readdir} $SOURCEDIRHEAD/$thedirname/$thedirname"
done
echo "./handle-xr-references.pl -m \"${readdir}\" -i list-of-anchors -l en"
./handle-xr-references.pl -m "${readdir}" -i list-of-anchors -l en
#
# Copy images
cp -v ${SOURCEIMAGEDIR}/png/*.png ${DESTIMAGEDIR}/
cp -v ${SOURCEIMAGEDIR}/*.png ${DESTIMAGEDIR}/
