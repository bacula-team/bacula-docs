\chapter{What To Do When \bacula{} Crashes (Kaboom)}
\label{blb:KaboomChapter}
\index[general]{Kaboom!What To Do When \bacula{} Crashes }
\index[general]{What To Do When \bacula{} Crashes (Kaboom) }

If you are running on a Linux system, and you have a set of working
configuration files, it is very unlikely that \textbf{\bacula{}} will crash. As with
all software, however, it is inevitable that someday, it may crash,
particularly if you are running on another operating system or using a new or
unusual feature.

This chapter explains what you should do if one of the three \textbf{\bacula{}}
daemons (Director, File, Storage) crashes.  When we speak of crashing, we
mean that the daemon terminates abnormally because of an error.  There are
many cases where \bacula{} detects errors (such as PIPE errors) and will fail
a job. These are not considered crashes.  In addition, under certain
conditions, \bacula{} will detect a fatal in the configuration, such as
lack of permission to read/write the working directory. In that case,
\bacula{} will force itself to crash with a SEGFAULT. However, before
crashing, \bacula{} will normally display a message indicating why.
For more details, please read on.


\section{Traceback}
\index[general]{Traceback}

Each of the three \bacula{} daemons has a built-in exception handler which, in
case of an error, will attempt to produce a traceback. If successful the
traceback will be emailed to you.

For this to work, you need to ensure that a few things are setup correctly on
your system:

\begin{benumerate}
\item You must have a version of \bacula{} built with debug information turned
   on and not stripped of debugging symbols.

\item You must have an installed copy of \btool{gdb} (the \acs{GNU} debugger),  and it
  must be on \textbf{\bacula{}'s} path. On some systems such as Solaris,
  \btool{gdb} may be replaced by \btool{dbx}.

\item The \bacula{} installed script file \btool{btraceback} must  be in the same
   directory as the daemon which dies, and it must  be marked as executable.

\item The script file \bfilename{btraceback.gdb} must  have the correct  path to it
   specified in the \btool{btraceback} file.

\item You must have a \btool{mail} program which is on \textbf{\bacula{}'s}  path.
   By default, this \btool{mail} program is set to \btool{bsmtp}, so it must
   be correctly configured.

\item If you run either the Director or Storage daemon under a non-root
   userid, you will most likely need to modify the \btool{btraceback} file
   to do something like \btool{sudo} (raise to root priority) for the
   call to \btool{gdb} so that it has the proper permissions to debug
   \bacula{}.
\end{benumerate}

If all the above conditions are met, the daemon that crashes will produce a
traceback report and email it to you. If the above conditions are not true,
you can either run the debugger by hand as described below, or you may be able
to correct the problems by editing the \btool{btraceback} file. I recommend not
spending too much time on trying to get the traceback to work as it can be
very difficult.

The changes that might be needed are to add a correct path to the \btool{gdb}
program, correct the path to the \bfilename{btraceback.gdb} file, change the \btool{mail}
program or its path, or change your email address. The key line in the
\btool{btraceback} file is:

\begin{bVerbatim}
gdb -quiet -batch -x /home/kern/bacula/bin/btraceback.gdb \
     $1 $2 2>\&1 | bsmtp -s "Bacula traceback" your-address@xxx.com
\end{bVerbatim}

Since each daemon has the same traceback code, a single btraceback file is
sufficient if you are running more than one daemon on a machine.

\section{Testing The Traceback}
\index[general]{Traceback!Testing The }
\index[general]{Testing The Traceback }

To ``manually'' test the traceback feature, you simply start \textbf{\bacula{}} then
obtain the \textbf{PID} of the main daemon thread (there are multiple threads).
The output produced here will look different depending on what OS and what
version of the kernel you are running.
Unfortunately, the output had to be split to fit on this page:

\begin{bVerbatim}
[kern@rufus kern]$ ps fax --columns 132 | grep bacula-dir
 2103 ?        S      0:00 /home/kern/bacula/k/src/dird/bacula-dir -c
                                       /home/kern/bacula/k/src/dird/dird.conf
 2104 ?        S      0:00  \_ /home/kern/bacula/k/src/dird/bacula-dir -c
                                       /home/kern/bacula/k/src/dird/dird.conf
 2106 ?        S      0:00      \_ /home/kern/bacula/k/src/dird/bacula-dir -c
                                       /home/kern/bacula/k/src/dird/dird.conf
 2105 ?        S      0:00      \_ /home/kern/bacula/k/src/dird/bacula-dir -c
                                       /home/kern/bacula/k/src/dird/dird.conf
\end{bVerbatim}

which in this case is 2103. Then while \bacula{} is running, you call the program
giving it the path to the \bacula{} executable and the \textbf{PID}. In this case,
it is:

\begin{bVerbatim}
./btraceback /home/kern/bacula/k/src/dird 2103
\end{bVerbatim}

It should produce an email showing you the current state of the daemon (in
this case the Director), and then exit leaving \textbf{\bacula{}} running as if
nothing happened. If this is not the case, you will need to correct the
problem by modifying the \btool{btraceback} script.

Typical problems might be that \btool{gdb} or \btool{dbx} for Solaris is not on
the default path.  Fix this by specifying the full path to it in the
\btool{btraceback} file.  Another common problem is that you haven't modified the
script so that the \btool{bsmtp} program has an appropriate smtp server or
the proper syntax for your \acs{SMTP} server.  If you use the \btool{mail} program
and it is not on the default path, it will also fail.  On some systems, it
is preferable to use \btool{Mail} rather than \btool{mail}.

\section{Getting A Traceback On Other Systems}
\index[general]{Getting A Traceback On Other Systems}
\index[general]{Systems!Getting A Traceback On Other}

It should be possible to produce a similar traceback on systems other than
Linux, either using \btool{gdb} or some other debugger. Solaris with \btool{dbx}
loaded works quite fine. On other systems, you will need to modify the
\btool{btraceback} program to invoke the correct debugger, and possibly correct the
\bfilename{btraceback.gdb} script to have appropriate commands for your debugger. If
anyone succeeds in making this work with another debugger, please send us a
copy of what you modified. Please keep in mind that for any debugger to
work, it will most likely need to run as root, so you may need to modify
the \btool{btraceback} script accordingly.

\label{blb:ManuallyDebugging}
\section{Manually Running \bacula{} Under The Debugger}
\index[general]{Manually Running \bacula{} Under The Debugger}
\index[general]{Debugger!Manually Running \bacula{} Under The}

If for some reason you cannot get the automatic traceback, or if you want to
interactively examine the variable contents after a crash, you can run \bacula{}
under the debugger. Assuming you want to run the Storage daemon under the
debugger (the technique is the same for the other daemons, only the name
changes), you would do the following:

\begin{benumerate}
\item Start the Director and the File daemon. If the  Storage daemon also
  starts, you will need to find its PID  as shown above (\btool{ps fax | grep   bacula-sd})
  and kill it  with a command like the following:

\begin{bVerbatim}
kill -15 PID
\end{bVerbatim}

where you replace \textbf{PID} by the actual value.

\item At this point, the Director and the File daemon should  be running but
   the Storage daemon should not.

\item \btool{cd} to the directory containing the Storage daemon

\item Start the Storage daemon under the debugger:

\begin{bVerbatim}
gdb ./bacula-sd
\end{bVerbatim}

\item Run the Storage daemon:

\begin{bVerbatim}
run -s -f -c ./bacula-sd.conf
\end{bVerbatim}

You may replace the \bfilename{./bacula-sd.conf} with the full path  to the Storage
daemon's configuration file.

\item At this point, \bacula{} will be fully operational.

\item In another shell command window, start the Console program  and do what
   is necessary to cause \bacula{} to die.

\item When \bacula{} crashes, the \btool{gdb} shell window will  become active and
   \btool{gdb} will show you the error that  occurred.

\item To get a general traceback of all threads, issue the following  command:

\begin{bVerbatim}
thread apply all bt
\end{bVerbatim}

After that you can issue any debugging command.
\end{benumerate}

\section{Getting Debug Output from \bacula{}}
\index[general]{Getting Debug Output from \bacula{} }
Each of the daemons normally has debug compiled into the program, but
disabled. There are two ways to enable the debug output. One is to add the
\textbf{-d nnn} option on the command line when starting the debugger.
The \textbf{nnn} is the debug level, and generally anything between 50 and 200 is
reasonable. The higher the number, the more output is produced. The output is
written to standard output.

The second way of getting debug output is to dynamically turn it on using the
Console using the \bcommandname{setdebug} command. The full syntax of the command is:

\begin{bVerbatim}
setdebug level=nnn client=client-name storage=storage-name dir
\end{bVerbatim}

If none of the options are given, the command will prompt you. You can
selectively turn on/off debugging in any or all the daemons (i.e. it is not
necessary to specify all the components of the above command).
