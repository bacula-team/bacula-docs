\chapter{Using \bacula{} \acl{CDP}}
\label{blb:cdp}
\index[general]{CDP}
\index[general]{Continuous Data Protection}

\ac{CDP} also called continuous backup or real-time
backup, refers to backup of Client data by automatically saving a copy of
every change made to that data, essentially capturing every version of the data
that the user saves. It allows the user or administrator to restore data to any
point in time.

\bimageH%[0.9\linewidth]
{cdp}{\acs{CDP} Example}{fig:cdp}

The \baculaenterprise{} \acs{CDP} feature is composed of two components. A
application (\btool{cdp-client} or \btool{tray-monitor}) that will monitor a
set of directories configured by the user and a \bacula{} FileDaemon plugin
responsible to secure the data on a \bacula{} infrastructure.

The user application (\btool{cdp-client} or \btool{tray-monitor}) is
responsible to monitor files and directories. When a modification is detected,
a copy of the new data is done into a \bdirectoryname{spool} directory. At a regular
interval, a \bacula{} backup job will contact the FileDaemon and will save all
the files archived by the \btool{cdp-client}. The local data can be restored
at any time without a network connection to the Director.

\section{Configuration}

\subsection{FileDaemon Configuration}

As with all \bacula{} plugins, the \bdirectivename{Plugin Directory} directive in the
\bdirectivename{FileDaemon} resource of the \bfilename{bacula-fd.conf} file needs to be
set:
\begin{bVerbatim}
FileDaemon {
  Name = test-fd
  ...
  Plugin Directory = /opt/bacula/plugins
}
\end{bVerbatim}

\subsection{Plugin Parameters}

In order to configure the CDP Plugin, one of the following parameters must be set:

The following parameters are used in the \acs{CDP} job.

\begin{bitemize}
\item \texttt{user=<string>} specifies a user in the system. The CDP Plugin will guess the location of the journal file for this user. \bhighlight{Not available on Windows.} 
\item \texttt{group=<string>} specifies a group in the system. The CDP Plugin will guess the location of the journal files of the users from this group. \bhighlight{Not available on Windows.} 
\item \texttt{userHome=<string>} specifies the path to the \texttt{cdp} User Home. This is where the CDP Plugin will look for the journal file. \bhighlight{On Windows, this is the User AppData/Roaming folder, for example: "C:/Users/Auser/AppData/Roaming".}
\end{bitemize}

\subsection{Director Configuration}

\begin{bVerbatim}
FileSet {
  Name = cdp
  Include {
    Options {
      compression = LZO
    }

    Plugin = "cdp: userHome=/home/user1"
  }
}

Job {
  Name = CDP
  Client = myclient-fd
  Type = Backup
  Pool = Default
  Schedule = Hourly      # Choose a schedule
  Messages = Standard
  FileSet = cdp
}
\end{bVerbatim}

\subsection{CDP Client Configuration}

\begin{bdescription}
\item [-f \bbracket{dir}] watches the directory \bbracket{dir} for changes
\item [-j \bbracket{dir}] sets journal directory to  \bbracket{dir}. Default value is \bdefaultvalue{\bbracket{HOME}}
\item [-s \bbracket{dir}] sets spool directory to \bbracket{dir}. Default value is \bdefaultvalue{\bbracket{HOME}/.cdp-sdir}
\item [-d \bbracket{nn}] set debug level to \bbracket{nn}
\item [-h] print help
\end{bdescription}

\subsection{Tray Monitor Configuration}

You can setup the folders you wish to watch for changes by using the Tray Monitor.

Open the Tray Monitor options and click in the option \bhighlight{Watch\ldots{}}:

\bimageH
{cdp-image1}{Open \acs{CDP} Client}{blb:fig:cdp-image1.png}

You should see a window displaying the watched folders. Click in the button \bhighlight{Add}:

\bimageH
{cdp-image2}{CDP Client Window}{blb:fig:cdp-image2.png}

Select the folder you wish to watch:

\bimageH
{cdp-image3}{Watching a Folder}{blb:fig:cdp-image3.png}

You should see the watched folder listed, as in the image below:

\bimageH
{cdp-image4}{Result}{blb:fig:cdp-image4.png}

\subsection{The Spool Directory and Scheduled Backups}

The Spool Directory contains all files from the watched folders
that were created and / or changed. It keeps a copy of every version of
the files.

An example of this directory can be seen below: 

\bimageH
{cdp-image7}{Result}{blb:fig:cdp-image7.png}

You must schedule backup jobs with a proper Files in order to backup those files with Bacula.
Following this example, the job configuration would be:

\begin{bVerbatim}
FileSet {
  Name = cdp
  Include {
    Options {
      compression = LZO
    }
    Plugin = "cdp: userHome=/home/user1"
  }
}

Job {
  Name = CDP
  Client = myclient-fd
  Type = Backup
  Pool = Default
  Schedule = Hourly      # Choose a schedule
  Messages = Standard
  FileSet = cdp
}
\end{bVerbatim} 

The Job reads the journal file to select which files from the Spool Directory
should be backed up. After that, the files will be available to be restored at any time:

\bimageH
{cdp-image8}{Job Files}{blb:fig:cdp-image8.png}

\subsection{Important}

It's necessary to keep either the \btool{cdp-client} or the \btool{tray-monitor} running in order to 
protect the directories being watched.

\subsection{Limitations}

\begin{bitemize}
\item There is a limit in the number of folders that can be watched by default, which varies
depending on your Operating System. You can increase that number by checking the correct
procedure for your Operating System.
\item On Linux OSes, the CDP client implementation relies on the \bhighlight{inotify API}, therefore being subject
to it's limitations (for example, it doesn't work with NFS). On Windows, the client relies on the
\bhighlight{ReadDirectoryChangesW API}.
\end{bitemize}
