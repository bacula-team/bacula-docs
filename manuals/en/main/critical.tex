\chapter{Critical Items to Implement Before Production}
\label{blb:CriticalChapter}
\index[general]{Production!Critical Items to Implement Before}
\index[general]{Critical Items to Implement Before Production}

We recommend you take your time before implementing a production a \bacula{}
backup system since \bacula{} is a rather complex program, and if you make a
mistake, you may suddenly find that you cannot restore your files in case
of a disaster.  This is especially true if you have not previously used a
major backup product.

If you follow the instructions in this chapter, you will have covered most of
the major problems that can occur. It goes without saying that if you ever
find that we have left out an important point, please inform us, so
that we can document it to the benefit of everyone.


\section{Critical Items}
\label{blb:Critical}
\index[general]{Critical Items}
\index[general]{Items!Critical}

The following assumes that you have installed \bacula{}, you more or less
understand it, you have at least worked through the tutorial or have
equivalent experience, and that you have set up a basic production
configuration. If you haven't done the above, please do so and then come back
here. The following is a sort of checklist that points with perhaps a brief
explanation of why you should do it.  In most cases, you will find the
details elsewhere in the manual.  The order is more or less the order you
would use in setting up a production system (if you already are in
production, use the checklist anyway).

\begin{bitemize}
\item Test your tape drive for compatibility with \bacula{} by using the \bcommandname{test}
   command in the See the \bxlink{btape}{blb:btape}{utility}{section} of the \butilityman{}.
\item Better than doing the above is to walk through the nine steps in the
   \bxlink{Tape Testing}{blb:TapeTestingChapter}{problems}{chapter} of the \bproblemman{}. It
   may take you a bit of time, but it will eliminate surprises.
\item Test the end of tape handling of your tape drive by using the
   \bcommandname{fill} command in the \bxlink{btape program}{blb:btape}{utility}{section} (Part of the \butilityman{})
\item If you are using a Linux 2.4 kernel, make sure that \btool{/lib/tls} is disabled. \bacula{}
   does not work with this library. See the second point under
   \bilink{Supported Operating Systems.}{blb:SupportedOSes}
\item Do at least one restore of files. If you backup multiple \acs{OS} types
   (Linux, Solaris, HP, MacOS, FreeBSD, Win32, \ldots{}),
   restore files from each system type. The
   \bilink{Restoring Files}{blb:RestoreChapter} chapter shows you how.
\item Write a bootstrap file to a separate system for each backup job.  The
   \bdirectivename{Write Bootstrap} directive is described in the
   \bilink{Director Configuration}{blb:writebootstrap}  chapter of the
   manual, and more details are available in the
   \bilink{Bootstrap File}{blb:BootstrapChapter} chapter. Also, the default
   \bfilename{bacula-dir.conf} comes with a \bdirectivename{Write Bootstrap} directive defined. This  allows
   you to recover the state of your system as of the last backup.
\item Backup your catalog. An example of this is found in the default
   \bfilename{bacula-dir.conf} file. The backup script is installed by default and
   should handle any database, though you may want to make your own local
   modifications.  See also \bilink{Backing Up Your \bacula{} Database --- Security Considerations}{blb:BackingUpBaculaSecurityConsiderations} for more
   information.
\item Write a bootstrap file for the catalog. An example of this is found in
   the default \bfilename{bacula-dir.conf} file. This will allow you to quickly restore your
   catalog in the event it is wiped out -- otherwise it  is many excruciating
   hours of work.
\item Make a copy of the \bfilename{bacula-dir.conf}, \bfilename{bacula-sd.conf}, and
   \bfilename{bacula-fd.conf} files that you are using on your server. Put it in a safe
   place (on another machine) as these files can be difficult to
   reconstruct if your server dies.
\item Make a \bacula{} Rescue CDROM! See the
  \bilink{Disaster Recovery Using a \bacula{} Rescue CDROM}{blb:RescueChapter} chapter.
  It is trivial to  make such a CDROM,    and it can make system recovery in the event of
  a lost hard disk infinitely easier.
\item \bacula{} assumes all filenames are in \acs{UTF}-8 format. This is important
   when saving the filenames to the catalog. For Win32 machine, \bacula{} will
   automatically convert from Unicode to UTF-8, but on Unix, Linux, *BSD,
   and MacOS X machines, you must explicitly ensure that your locale is set
   properly. Typically this means that the \textbf{LANG} environment variable
   must end in \textbf{.UTF-8}. A full example is \textbf{en\_US.UTF-8}. The
   exact syntax may vary a bit from OS to OS, and exactly how you define it
   will also vary.

   On most modern Win32 machines, you can edit the configuration files with \textbf{notepad}
   and choose output encoding UTF-8.
\end{bitemize}

\section{Recommended Items}
\index[general]{Items!Recommended}
\index[general]{Recommended Items}

Although these items may not be critical, they are recommended and will help
you avoid problems.

\begin{bitemize}
\item Read the \bilink{Quick Start Guide to \bacula{}}{blb:QuickStartChapter}
\item After installing and experimenting with \bacula{}, read and work carefully
   through the examples in the
   \bilink{Tutorial}{blb:TutorialChapter} chapter  of this manual.
\item Learn what each of the \bxdlink{\bacula{} Utility Programs}{blb:TheUtilityChapter}{utility}{chapter}
   does.
\item Set up reasonable retention periods so that your catalog does not  grow
   to be too big. See the following three chapters:
   \begin{bitemize}
     \item \bilink{Recycling your Volumes}{blb:RecyclingChapter},
     \item \bilink{Basic Volume Management}{blb:DiskChapter},
     \item \bilink{Using Pools to Manage Volumes}{blb:PoolsChapter}.
   \end{bitemize}
\item Perform a bare metal recovery using the \bacula{} Rescue CDROM.  See the
   \bilink{Disaster Recovery Using a \bacula{} Rescue CDROM}{blb:RescueChapter}
    chapter.
\end{bitemize}

If you absolutely must implement a system where you write a different
tape each night and take it offsite in the morning. We recommend that you do
several things:
\begin{bitemize}
\item Write a bootstrap file of your backed up data and a bootstrap file
   of your catalog backup to a usb disk or a \acs{CDROM} and take that with
   the tape.  If this is not possible, try to write those files to another
   computer or offsite computer, or send them as email to a friend. If none
   of that is possible, at least print the bootstrap files and take that
   offsite with the tape.  Having the bootstrap files will make recovery
   much easier.
\item It is better not to force \bacula{} to load a particular tape each day.
   Instead, let \bacula{} choose the tape.  If you need to know what tape to
   mount, you can print a list of recycled and appendable tapes daily, and
   select any tape from that list.  \bacula{} may propose a particular tape
   for use that it considers optimal, but it will accept any valid tape
   from the correct pool.
\end{bitemize}
