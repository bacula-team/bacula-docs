#
# Makefile for converting svg files
# to something else: png, pdf, eps
#
# There would be absolutely no reason, at all, to produce EPS neither PNG of this covers. Anyway you can do it it you want to.
#
# 22-oct-2020 - Philippe Chauvat / Bacula Systems
#		Better handling process for SVG files
#		Inkscape does not recognize .template files as inkscape when opening/closing/saving them
#		Source files are now: .svg then will be moved to .tpl and compiled as .pdf
#		Template files are therefore intermediate and can be deleted if needed.
# 27-jul-2017 - Philippe Chauvat / Bacula Systems : Improvements with date and version management
#               Work is based on svg files named *.template which contain __BVERSION__ and __BDATE__
#		These "variables" are going to be seded to the current version and the current date.
# 10-Oct-2012 - Philippe Chauvat / Bacula Systems
# 
INKSCAPE=inkscape
VERSION=$(shell $(INKSCAPE) -V 2>/dev/null|cut -d ' ' -f2|cut -d'.' -f1)
VERBOSE=FALSE
ifeq ($(VERBOSE),TRUE)
	HIDE=
else
	HIDE=@
endif

#
# Traitement de la compilation à partir de la version 1 d'Inkscape
ifeq ($(VERSION),1)
	INKSCAPE_FLAGS=-o
	SVG_TO_PDF=--export-type=pdf -C --export-pdf-version=1.5 -T $(INKSCAPE_FLAGS)
	SVG_TO_EPS=--export-type=eps -C -T $(INKSCAPE_FLAGS)
	SVG_TO_PNG=--export-type=png -C -T --export-dpi=120 $(INKSCAPE_FLAGS)
#
# Version 0.x
else
	INKSCAPE_FLAGS=-z -T -C
	SVG_TO_PDF=$(INKSCAPE_FLAGS) -A
	SVG_TO_EPS=$(INKSCAPE_FLAGS) -E
	SVG_TO_PNG=$(INKSCAPE_FLAGS) -e
endif
PDFDIR=../pdf
EPSDIR=../eps

TPLSVGS=$(shell grep -l '__' *.svg)
SVGS=$(shell grep -L '__' *.svg)
TPLS=$(TPLSVGS:.svg=.tpl)
PDFS=$(addprefix $(PDFDIR)/,$(SVGS:.svg=.pdf)) $(addprefix $(PDFDIR)/,$(TPLS:.tpl=.pdf))
EPSS=$(addprefix $(EPSDIR)/,$(SVGS:.svg=.eps)) $(addprefix $(EPSDIR)/,$(TPLS:.tpl=.eps))

BDATE := $(shell date +%Y-%m-%d)

all: tpl pdf

.SUFFIXES:
.PHONY:
.DONTCARE:

check:
	$(HIDE)echo "Template SVG files: $(TPLSVGS)"
	$(HIDE)echo "Regular SVG files: $(SVGS)"
	$(HIDE)echo "TPL files to be generated: $(TPLS)"
	$(HIDE)echo "PDFDIR: $(PDFDIR)"
	$(HIDE)echo "PDFs $(PDFS)"
	$(HIDE)echo "EPSDIR: $(EPSDIR)"

help:
	$(HIDE)echo "This Makefile handles .svg file containing parameters to .pdf format while replacing the parameters with their values."
	$(HIDE)echo "    Parameter should be __PARAMETER_NAME__ at least in one place"
	$(HIDE)echo "    This is the way the Makefile identifies the SVG file to convert. Other SVG files will be directly exported into PDF directly"
	$(HIDE)echo "The process is in two steps:"
	$(HIDE)echo "  a) SVG to TPL file which will replace the parameters with their respective value"
	$(HIDE)echo "  b) TPL to PDF. As inkscape is complaining about .tpl not being an expected file extension"
	$(HIDE)echo "     both STDERR and STDOUT are sent to their .svg_error file"
#
# PDF images creation
pdf: tpl $(TEMPLATES) $(PDFS)

tpl: $(TPLS)

$(TPLS): $(VERSION_TEX)

%.tpl: %.svg
	$(HIDE)echo "$< :   Handling version number and date"
	$(HIDE)sed "s/__BVERSION__/`awk '{print $$1}' $(VERSION_TEX)`/g" $< > $@
	$(HIDE)sed -i 's/__BEDITION__/$(BACULAVERSION)/g' $@
	$(HIDE)sed -i 's/__BDATE__/$(BDATE)/g' $@


$(PDFS): | $(PDFDIR)
$(PDFDIR):
	$(HIDE)echo "Creating PDF images directory..."
	$(HIDE)mkdir $(PDFDIR)
	$(HIDE)echo "Done"

$(PDFDIR)/%.pdf: %.tpl
	$(HIDE)echo "Converting $< to $@"
	$(HIDE)$(INKSCAPE) $(SVG_TO_PDF) $@ $< 1>$<.svg_error 2>$<.svg_error

$(PDFDIR)/%.pdf: %.svg
	$(HIDE)echo "$< :   Converting to PDF"
	$(HIDE)$(INKSCAPE) $(SVG_TO_PDF) $@ $<
#
# EPS images creation
eps: $(EPSS)
$(EPSS): | $(EPSDIR)
$(EPSDIR):
	$(HIDE)echo "Creating EPS images directory..."
	$(HIDE)mkdir $(EPSDIR)
	$(HIDE)echo "Done"

$(EPSDIR)/%.pdf: %.tpl
	$(HIDE)echo "Converting $< to $@"
	$(HIDE)$(INKSCAPE) $(SVG_TO_EPS) $@ $< 1>$<.svg_error 2>$<.svg_error

$(EPSDIR)/%.eps: %.svg
	$(HIDE)${INKSCAPE} ${INKSCAPE_FLAGS} ${SVG_TO_EPS} $(EPSDIR)/$@ $<

#
# PNG images creation
png: $(PNGS)
$(PNGS): | $(PNGDIR)
$(PNGDIR):
	$(HIDE)echo "Creating PNG images directory..."
	$(HIDE)mkdir $(PNGDIR)
	$(HIDE)echo "Done"

$(PNGDIR)/%.pdf: %.tpl
	$(HIDE)echo "Converting $< to $@"
	$(HIDE)$(INKSCAPE) $(SVG_TO_PNG) $@ $< 1>$<.svg_error 2>$<.svg_error

$(PNGDIR)/%.png: %.svg
	$(HIDE)${INKSCAPE} ${INKSCAPE_FLAGS} ${SVG_TO_PNG} $(PNGDIR)/$@ $<

clean:
	$(HIDE)rm -f $(TPLS)
	$(HIDE)(if [ -e $(PDFDIR) ]; then cd $(PDFDIR) ; rm -f $(PDFS); fi)
	$(HIDE)(if [ -e $(EPSDIR) ]; then cd $(EPSDIR) ; rm -f $(EPSS); fi)
	$(HIDE)(if [ -e $(PNGDIR) ]; then cd $(PNGDIR) ; rm -f $(PNGS); fi)
	$(HIDE)rm -f *~ *.svg_error

distclean: clean
