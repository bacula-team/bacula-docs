\chapter{Data Spooling}
\label{blb:SpoolingChapter}
\index[general]{Data Spooling }
\index[general]{Spooling!Data }

\bacula{} allows you to specify that you want the Storage daemon to initially
write your data to disk and then subsequently to tape. This serves several
important purposes.

\begin{bitemize}
\item It takes a long time for data to come in from the File daemon during
   an Incremental backup.  If it is directly written to tape, the tape will
   start and stop or shoe-shine as it is often called causing tape wear.
   By first writing the data to disk, then writing it to tape, the tape can
   be kept in continual motion.
\item While the spooled data is being written to the tape, the despooling
   process has exclusive use of the tape.  This means that you can spool
   multiple simultaneous jobs to disk, then have them very efficiently
   despooled one at a time without having the data blocks from several jobs
   intermingled, thus substantially improving the time needed to restore
   files. While despooling, all jobs spooling continue running.
\item Writing to a tape can be slow.  By first spooling your data to disk,
   you can often reduce the time the File daemon is running on a system,
   thus reducing downtime, and/or interference with users.  Of course, if
   your spool device is not large enough to hold all the data from your
   File daemon, you may actually slow down the overall backup.
\end{bitemize}

Data spooling is exactly that ``spooling''.  It is not a way to first write a
``backup'' to a disk file and then to a tape.  When the backup has only been
spooled to disk, it is not complete yet and cannot be restored until it is
written to tape.

\bacula{} version 1.39.x and later supports writing a backup
to disk then later \textbf{Migrating} or moving it to a tape (or any
other medium). For
details on this, please see the \bilink{Migration}{blb:MigrationChapter} chapter
of this manual for more details.

The remainder of this chapter explains the various directives that you can use
in the spooling process.

\label{blb:directives}
\section{Data Spooling Directives}
\index[general]{Directives!Data Spooling }
\index[general]{Data Spooling Directives }

The following directives can be used to control data spooling.

\begin{bitemize}
\item To turn data spooling on/off at the Job level in  the Job resource in
   the Director's conf file (default  \bdefaultvalue{no}).

\textbf{SpoolData = \byesorno{}}

\item To override the Job specification in a Schedule Run  directive in the
   Director's conf file.

\textbf{SpoolData = \byesorno{}}

\item To override the Job specification in a bconsole session using the \bcommandname{run}
   command. Please note that this does \textbf{not} refer to a configuration
   statement, but to an argument for the \bcommandname{run} command.

\textbf{SpoolData= \byesorno{}}

\item To limit the the maximum spool file size for a particular job in the Job
  resource

\textbf{Spool Size = size}
   Where size is a the maximum spool size for this job  specified in \bhighlight{bytes}.

\item To limit the maximum total size of the spooled data  for a particular
   device. Specified in the Device  resource of the Storage daemon's conf file
   (default  \bdefaultvalue{unlimited}).

\textbf{Maximum Spool Size = size}
   Where size is a the maximum spool size for all jobs  specified in \bhighlight{bytes}.

\item To limit the maximum total size of the spooled data  for a particular
   device for a single job. Specified  in the Device Resource of the Storage
   daemon's configuration file (default \bdefaultvalue{unlimited}).

\textbf{Maximum Job Spool Size = size}
   Where size is the maximum spool file size for a single  job specified in
   \bhighlight{bytes}.

\item To specify the spool directory for a particular device.  Specified in
  the Device Resource of the Storage daemon's configuration  file (default,
  the \bdefaultvalue{working directory}).

\textbf{Spool Directory = directory}
\end{bitemize}

\label{blb:warning}

% TODO: fix this section name
\section{FileSet consideration}%%% Philippe's idea for renaming this section. !!! MAJOR WARNING !!!}
\index[general]{Spooling! Fileset} %%WARNING! MAJOR}
\index[general]{Fileset and spooling}%%MAJOR WARNING}

Please be very careful to \bhighlight{exclude} the spool directory from any backup,
otherwise, your job will write enormous amounts of data to the Volume, and
most probably terminate in error. This is because in attempting to backup the
spool file, the backup data will be written a second time to the spool file,
and so on \emph{ad infinitum}.

Another advice is to always specify the maximum spool size so that your disk
doesn't completely fill up. In principle, data spooling will properly detect a
full disk, and despool data allowing the job to continue. However, attribute
spooling is not so kind to the user. If the disk on which attributes are being
spooled fills, the job will be canceled. In addition, if your working
directory is on the same partition as the spool directory, then \bacula{} jobs
will fail possibly in bizarre ways when the spool fills.

\label{blb:points}
\section{Other Points}
\index[general]{Points!Other }
\index[general]{Other Points }

\begin{bitemize}
\item When data spooling is enabled, \bacula{} automatically  turns on attribute
   spooling. In other words, it also  spools the catalog entries to disk. This is
   done so  that in case the job fails, there will be no catalog  entries
   pointing to non-existent tape backups.
\item Attribute despooling occurs near the end of a job.  The Storage daemon
   accumulates file attributes during the backup and  sends them to the
   Director at the end of the job.  The Director then inserts the file
   attributes into the catalog.  During this insertion, the tape drive may
   be inactive.  When the file attribute insertion is completed, the job
   terminates.
\item Attribute spool files are always placed in the  working directory of
   the Storage daemon.
\item When \bacula{} begins despooling data spooled to disk, it  takes exclusive
   use of the tape. This has the major  advantage that in running multiple
   simultaneous jobs at  the same time, the blocks of several jobs will not be
   intermingled.
\item It probably does not make a lot of sense to enable data  spooling if you
   are writing to disk files.
\item It is probably best to provide as large a spool file as  possible to
   avoid repeatedly spooling/despooling. Also,  while a job is despooling to
   tape, the File daemon must wait  (i.e. spooling stops for the job while it is
   despooling).
\item If you are running multiple simultaneous jobs, \bacula{}  will continue
   spooling other jobs while one is despooling  to tape, provided there is
   sufficient spool file space.
\end{bitemize}
