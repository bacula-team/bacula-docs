function log(msg) {
    setTimeout(function() {
        throw new Error(msg);
    }, 0);
}
function basename(path) {
    var forwin = path.replace(/\\/g,'/') ;
    var fornux = forwin.replace(/.*\//, '' );
    return fornux ;
}
function menuonload(self) {
    var myatag = "";
    var mytag = "" ;
    var x = 0 ;
    var atag = document.getElementsByTagName("a") ;
    var filename = basename(document.location.href) ;
    for (x=0;x<atag.length;x=x+1) {
        if (basename(atag[x].href) == filename) {
	    myatag = atag[x] ;
	    break ;
        }
    }
    openmenus(self,myatag) ;
}

function openmenus(self,myatag) {
    x = 0 ;
    var breadcrumbstext = new Array() ;
    var breadcrumbsrefs = new Array() ;
    breadcrumbsrefs[x] = myatag.href ;
    breadcrumbstext[x] = myatag.innerHTML ;
    mytag = myatag.parentNode.parentNode ;
    while (mytag.tagName != "DIV") {
	x++ ;
	//
	// Change arrow from "right" to "down"
	myatag = mytag.parentNode.getElementsByTagName("a")[0] ;
	//
	// Keep all needed informations
	breadcrumbsrefs[x] = myatag.href ;
	breadcrumbstext[x] = myatag.innerHTML ;
	//
	// Find upper level
        mytag = mytag.parentNode.parentNode ;
    }
    //
    // Find the breadcrumbs part
    mytag = document.getElementById("b_breadcrumbscontentid") ;
    //
    // Build breadcrumbs content
    var thebreadcrumbs = "<ul>" ;
    for (y=x-1;y>=0;y--) {
	thebreadcrumbs = thebreadcrumbs + "<li><a href='" + breadcrumbsrefs[y] + "' onclick=\"menuonclick(this,'');\">" + breadcrumbstext[y] + "</a></li>" ;
    }
    thebreadcrumbs = thebreadcrumbs + "</ul>"
    mytag.innerHTML = thebreadcrumbs ;
}
function menuonclick(self) {
    //
    // find the *other* <A> tag which contains the same HREF
    var myatag = "";
    var x ;
    var atags = document.getElementsByTagName("a") ;
    var filename = basename(self.href) ;
    var firstone = 0 ;
    for (x=0;x<atags.length;x=x+1) {
        if ((basename(atags[x].href) == filename) && (atags[x].name != "")) {
	    myatag = atags[x] ;
	    break ;
        }
    }
    openmenus(self,myatag) ;
}
function click_expandingMenuHeader(obj,uniqid)
{
    var x=document.getElementById(uniqid).className;
    x=x.replace("expandingMenuNotSelected","expandingMenuSelected");
    obj.style.listStyleImage = "url(\'../../images/down.png\')" ;
    document.getElementById(uniqid).className=x;
    document.getElementById(uniqid).style.display="block";
    linode = obj.parentNode ;
    linode.onmouseout="" ;
}
function over_expandingMenuHeader(obj,uniqid)
{
    var x=document.getElementById(uniqid).className;
    if (x.indexOf("expandingMenuNotSelected")>-1)
    {
	x=x.replace("expandingMenuNotSelected","expandingMenuSelected");
        obj.style.listStyleImage = "url(\'../../images/down.png\')" ;
	document.getElementById(uniqid).className=x;
	document.getElementById(uniqid).style.display="block";
    }
}
function out_expandingMenuHeader(obj,uniqid)
{
    var x=document.getElementById(uniqid).className;
    if (x.indexOf("expandingMenuSelected")>-1)
    {
	x=x.replace("expandingMenuSelected","expandingMenuNotSelected");
	document.getElementById(uniqid).className=x;
        obj.style.listStyleImage = "url(\'../../images/right.png\')" ;
	document.getElementById(uniqid).style.display="none";
    }
}
